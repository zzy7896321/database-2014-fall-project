ALTER DATABASE acmdb02
	CHARACTER SET utf8
	COLLATE utf8_general_ci;

CREATE TABLE Books (
	ISBN		CHAR(13) PRIMARY KEY,
	Title		VARCHAR(255) NOT NULL,
	Publisher	VARCHAR(255) NOT NULL DEFAULT '',
	Year_of_publication	INTEGER NOT NULL,
	Num_copies	INTEGER UNSIGNED NOT NULL,
	Price		INTEGER UNSIGNED NOT NULL,	/* in cents */
	Format	 	TINYINT NOT NULL,	/* 1: hardcover, 0: softcover */
	Keywords	VARCHAR(255) NOT NULL DEFAULT '',
	Subject		VARCHAR(64) NOT NULL DEFAULT ''
);

CREATE TABLE Authors (
	Author_id SERIAL PRIMARY KEY,
	Author_name	VARCHAR(100) NOT NULL
);

CREATE TABLE Written_by (
	ISBN		CHAR(13),
	Author_id   BIGINT UNSIGNED,
	PRIMARY KEY	(ISBN, Author_id),
	FOREIGN KEY (ISBN) REFERENCES Books (ISBN),
	FOREIGN KEY (Author_id) REFERENCES Authors (Author_id)
);

CREATE TABLE Users (
	Login		VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin PRIMARY KEY,
	Passwd		CHAR(32) NOT NULL,	/* MD5 */
	Phone		VARCHAR(16) NOT NULL DEFAULT '',
	Name		VARCHAR(100) NOT NULL DEFAULT '',
	Address		VARCHAR(255) NOT NULL DEFAULT '',
	UserType	TINYINT NOT NULL DEFAULT 1	/* 0 for admin, 1 for ordinary user */
);

CREATE TABLE Comments (
	ISBN		CHAR(13) REFERENCES Books (ISBN),
	Login		VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin REFERENCES Users (Login),
	Score		TINYINT NOT NULL,
	Date		TIMESTAMP NOT NULL DEFAULT NOW(),
	Description	VARCHAR(255) NOT NULL DEFAULT '',
	PRIMARY KEY (ISBN, Login),
	FOREIGN KEY (ISBN) REFERENCES Books (ISBN),
	FOREIGN KEY (Login) REFERENCES Users (Login)
);

CREATE TABLE Rates (
	ISBN		CHAR(13),
	Commenter_Login	VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin,	/* the user who posts the comment */
	Rater_Login		VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin,	/* the user who rates the comment */
	Usefulness		TINYINT NOT NULL, /* 0 for useless, 1 for useful, 2 for very useful */
	PRIMARY KEY (ISBN, Commenter_Login, Rater_Login),
	FOREIGN KEY (ISBN, Commenter_Login) REFERENCES Comments (ISBN, Login),
	FOREIGN KEY (Rater_Login) REFERENCES Users (Login)
);

CREATE TABLE Trusts (
	Commenter_Login	VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin,	/* the user who declares the trust comment */
	Commented_Login VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin, 	/* the user who receives the trust/untrust comment */
	Trust			TINYINT NOT NULL,	/* 1: trusted, -1: untrusted */
	PRIMARY KEY (Commenter_Login, Commented_Login),
	FOREIGN KEY (Commenter_Login) REFERENCES Users (Login),
	FOREIGN KEY (Commented_Login) REFERENCES Users (Login)
);

CREATE TABLE Orders (
	Oid		SERIAL PRIMARY KEY,
	Login	VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
	Time	TIMESTAMP DEFAULT NOW(),
	FOREIGN KEY (Login) REFERENCES Users (Login)
);

CREATE TABLE Includes (
	Oid		BIGINT UNSIGNED,
	ISBN	CHAR(13),
	Quantity	INTEGER	UNSIGNED NOT NULL,
	PRIMARY KEY (Oid, ISBN),
	FOREIGN KEY (Oid) REFERENCES Orders (Oid),
	FOREIGN KEY (ISBN) REFERENCES Books (ISBN)
);

