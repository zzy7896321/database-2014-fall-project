-- MySQL dump 10.13  Distrib 5.5.39, for CYGWIN (x86_64)
--
-- Host: 192.168.142.3    Database: acmdb02
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Authors`
--

DROP TABLE IF EXISTS `Authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Authors` (
  `Author_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Author_name` varchar(100) NOT NULL,
  PRIMARY KEY (`Author_id`),
  UNIQUE KEY `Author_id` (`Author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Authors`
--

LOCK TABLES `Authors` WRITE;
/*!40000 ALTER TABLE `Authors` DISABLE KEYS */;
/*!40000 ALTER TABLE `Authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Books`
--

DROP TABLE IF EXISTS `Books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Books` (
  `ISBN` char(13) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Publisher` varchar(255) NOT NULL DEFAULT '',
  `Year_of_publication` int(11) NOT NULL,
  `Num_copies` int(10) unsigned NOT NULL,
  `Price` int(10) unsigned NOT NULL,
  `Format` tinyint(4) NOT NULL,
  `Keywords` varchar(255) NOT NULL DEFAULT '',
  `Subject` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Books`
--

LOCK TABLES `Books` WRITE;
/*!40000 ALTER TABLE `Books` DISABLE KEYS */;
/*!40000 ALTER TABLE `Books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Comments`
--

DROP TABLE IF EXISTS `Comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comments` (
  `ISBN` char(13) NOT NULL DEFAULT '',
  `Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `Score` tinyint(4) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ISBN`,`Login`),
  KEY `Login` (`Login`),
  CONSTRAINT `Comments_ibfk_1` FOREIGN KEY (`ISBN`) REFERENCES `Books` (`ISBN`),
  CONSTRAINT `Comments_ibfk_2` FOREIGN KEY (`Login`) REFERENCES `Users` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comments`
--

LOCK TABLES `Comments` WRITE;
/*!40000 ALTER TABLE `Comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `Comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Includes`
--

DROP TABLE IF EXISTS `Includes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Includes` (
  `Oid` bigint(20) unsigned NOT NULL DEFAULT '0',
  `ISBN` char(13) NOT NULL DEFAULT '',
  `Quantity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Oid`,`ISBN`),
  KEY `ISBN` (`ISBN`),
  CONSTRAINT `Includes_ibfk_1` FOREIGN KEY (`Oid`) REFERENCES `Orders` (`Oid`),
  CONSTRAINT `Includes_ibfk_2` FOREIGN KEY (`ISBN`) REFERENCES `Books` (`ISBN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Includes`
--

LOCK TABLES `Includes` WRITE;
/*!40000 ALTER TABLE `Includes` DISABLE KEYS */;
/*!40000 ALTER TABLE `Includes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Orders` (
  `Oid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Oid`),
  UNIQUE KEY `Oid` (`Oid`),
  KEY `Login` (`Login`),
  CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`Login`) REFERENCES `Users` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rates`
--

DROP TABLE IF EXISTS `Rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rates` (
  `ISBN` char(13) NOT NULL DEFAULT '',
  `Commenter_Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `Rater_Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `Usefulness` tinyint(4) NOT NULL,
  PRIMARY KEY (`ISBN`,`Commenter_Login`,`Rater_Login`),
  KEY `Rater_Login` (`Rater_Login`),
  CONSTRAINT `Rates_ibfk_1` FOREIGN KEY (`ISBN`, `Commenter_Login`) REFERENCES `Comments` (`ISBN`, `Login`),
  CONSTRAINT `Rates_ibfk_2` FOREIGN KEY (`Rater_Login`) REFERENCES `Users` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rates`
--

LOCK TABLES `Rates` WRITE;
/*!40000 ALTER TABLE `Rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Trusts`
--

DROP TABLE IF EXISTS `Trusts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Trusts` (
  `Commenter_Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `Commented_Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `Trust` tinyint(4) NOT NULL,
  PRIMARY KEY (`Commenter_Login`,`Commented_Login`),
  KEY `Commented_Login` (`Commented_Login`),
  CONSTRAINT `Trusts_ibfk_1` FOREIGN KEY (`Commenter_Login`) REFERENCES `Users` (`Login`),
  CONSTRAINT `Trusts_ibfk_2` FOREIGN KEY (`Commented_Login`) REFERENCES `Users` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Trusts`
--

LOCK TABLES `Trusts` WRITE;
/*!40000 ALTER TABLE `Trusts` DISABLE KEYS */;
/*!40000 ALTER TABLE `Trusts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `Login` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Passwd` char(32) NOT NULL,
  `Phone` varchar(16) NOT NULL DEFAULT '',
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Address` varchar(255) NOT NULL DEFAULT '',
  `UserType` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES ('admin','21232f297a57a5a743894a0e4a801fc3','N/A','admin','N/A',0);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Written_by`
--

DROP TABLE IF EXISTS `Written_by`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Written_by` (
  `ISBN` char(13) NOT NULL DEFAULT '',
  `Author_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ISBN`,`Author_id`),
  KEY `Author_id` (`Author_id`),
  CONSTRAINT `Written_by_ibfk_1` FOREIGN KEY (`ISBN`) REFERENCES `Books` (`ISBN`),
  CONSTRAINT `Written_by_ibfk_2` FOREIGN KEY (`Author_id`) REFERENCES `Authors` (`Author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Written_by`
--

LOCK TABLES `Written_by` WRITE;
/*!40000 ALTER TABLE `Written_by` DISABLE KEYS */;
/*!40000 ALTER TABLE `Written_by` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-08 23:52:51
