Database 2014 Fall Course Project
==============================

A bookstore database management system by Peng Yanqing and Zhao Zhuoyue.

## Build and Run

The project can be built with ant and JAVA 8.

### Build with makefile
To build:
	
	ant
    
To clean, type:

    ant clean
    
## Build with Eclipse
The eclipse project file is also supplied. Before use eclipse to build or run the program, you need to compile the jflex and cup specifications. Type:
	
	ant parser lex
    
## Run
On linux:

	ant run
    
On windows: 

    run.cmd

Or use a command line:

	java -cp bin/:lib/java-cup-11b-runtime.jar acmdb.DBSystem default.xml
    
With eclipse or cygwin ported bash, the password cannot be hidden when login/register.

## Phase 1. Command-line interface system

See doc/doc.pdf

## Phase 2. Web-based interface

JSP. Tested on Tomcat 6.

### Additional build target
Type:
	
	ant web

The classes are converted to java 7 compatible byte code by retrolambda. Then classes and libraries are copied to public_html/WEB-INF/classes and public_html/WEB-INF/lib.

