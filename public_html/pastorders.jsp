<%@ page import="acmdb.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Past orders</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}

	Order order = (Order) session.getAttribute("cart");
	String popn = request.getParameter("popn");
	int n = WebUtil.parseInt(popn, -1);
	if (popn == null || popn.equals(""))
		n = 10;
	boolean invalid = (n < 0);
	
%>

<style>
#bookqueryresults div {
	padding: 0px;
}

#bookqueryresults table {
	width: 100%;
}

#bookqueryresults table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}

#bookqueryresults th, td {
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.index {
	width: 5%;
}

th.isbn {
	width: 10%;
}

th.title {
	width: 45%;	
}

th.year {
	width: 5%;
}

th.publisher {
	width: 25%;
}

th.price {
	width: 5%;
}

th.sales {
	width: 5%;
}

#bookqueryresults ul {
	list-style-type: none;
	display: block;
	padding: 0px;
	margin: 0px;
}

#bookqueryresults ul, li {
	padding-left: 15px;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

}

</style>
<body>

<h2>Past orders</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>

<div id="pop">
<form id="pop_form" name="pop_form" action="pastorders.jsp" autocomplete="off" method="post">
	<p>
	<input type="text" id="pop_text" name="popn" size=30 value="<%=invalid?popn:""%>" placeholder="past n (0 for unlimited, empty for 10)"/>
	

	<input type="submit" value="submit"/>
	<span id="prompt_id" class="error_prompt"><%=invalid ? "Invalid number" : ""%></span>

</form>
</div>
		
<%
	if (n>=0) {
		%>
	<div id="bookqueryresults">
	<h3>Query results</h3>
	<%
			SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		ArrayList<Long> oids = new ArrayList<Long>();
		int result = sql.queryOrderIds(user.login, n, oids);
		if (result != SQL.SUCCESS) { %>
			Query failed
		<%
		}
		else if (oids == null || oids.isEmpty()) {
		%>No such orders found.<%	
	} else {

		final String[] titles = {
			"ID",
			"Time",
			"#Items",
			"Total Price",
			"",
		};
		final String[] cssclass = {
			"index",
			"time",
			"num",
			"price",
			"detail",
		};
		
		%>
			<table id="bqtable" >
				<tr>
				<%
					for (int cno = 0; cno < titles.length; ++cno) {
						%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
					}
				%>
				</tr>
				<%
					for (int rid = 0, tar = oids.size(); rid < tar; ++rid) {
						Long oid = oids.get(rid);
						Pointer<PastOrder> po = new Pointer<PastOrder>();
						result = sql.queryOrderById(oid, po);
						if (result != SQL.SUCCESS || po == null || po.value == null) {
							%>Query failed<%
							break;
						}
					%>
						<tr>
							<td><%=oid%></td>
							<td><%=po.value.time%></td>
							<td><%=po.value.getNum()%></td>
							<td><%=WebUtil.formatPrice(po.value.getTotalPrice())%></td>
							<td><a href="showorder.jsp?oid=<%=oid%>">Details</a></td>
						</tr>
					<%
					}
				%>
			</table>
			
			
		<%
	}	/* if (books != null) */
		} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	}
	%>

	</div>

</body>

</html>
