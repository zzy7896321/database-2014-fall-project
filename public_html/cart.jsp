<%@ page import="acmdb.*,java.util.HashMap" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - my cart</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

<style>
#items div {
	padding: 0px;
}

table {
	width: 100%;
}

table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}

th, td {
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.isbn {
	width: 10%;
}

th.title {
	width: 35%;	
}

th.year {
	width: 5%;
}

th.publisher {
	width: 20%;
}

th.price {
	width: 5%;
}

th.number {
	width: 5%;
}

th.sum {
	width: 5%;
}

th.action {
	width: 15%;
}

</style>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	Order order = (Order) session.getAttribute("cart");
	if (order == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	
	HashMap<String, Book> books = null;
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		books = WebUtil.queryBooksInOrder(sql, user.login, order);
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
%>

<body>

<h2>My cart</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>

<% 
if (books == null) {
	%>Error occured when loading.<%
} else {
%>
<div id="items">
<%
	int nitems = 0;
	int sumprice = 0;
	
	Order.Item[] items = order.items.values().toArray(new Order.Item[0]);
	for (Order.Item item : items) {
		Book book = books.get(item.isbn);
		if (book == null) {
			order.items.remove(item.isbn);
		}
		else {
			nitems += item.num;
			sumprice += book.price * item.num;		
		}
	}
	items = order.items.values().toArray(new Order.Item[0]);

	final String[] titles = {
		"ISBN",
		"Title",
		"Year",
		"Publisher",
		"Price",
		"Number",
		"Sum",
		"",
	};

	final String[] cssclass = {
		"isbn",
		"title",
		"year",
		"publisher",
		"price",
		"number",
		"sum",
		"action",
	};
%>

<div id="summary">
<span style="float: left">
	You have <%=nitems%> items. Total price is <%=WebUtil.formatPrice(sumprice)%>.
</span>
<span style="float: right; margin-right: 20px;">
	<a href="checkout.jsp">Checkout</a>
	<span style="margin-right:20px;"></span>
	<a href="clearcart.jsp">Clear</a>
</span>
</div>

		<script type="text/javascript">
			function addToCart(isbn, num) {
				var f = document.getElementById("addtocartform");
				f.isbn.value = isbn;
				f.num.value = num;
				f.submit();
			}
		</script>

		<form id="addtocartform" style="display: none" action="additem.jsp" method="post"/>
			<input type="hidden" name="isbn" />
			<input type="hidden" name="num" />
		</form>
	
	<table id="ordertable">
		<tr>
		<%
			for (int cno = 0; cno < titles.length; ++cno) {
				%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
			}
		%>
		</tr>
		<%
			for (int id = 0; id < items.length; ++id) {
				Order.Item item = items[id];
				Book book = books.get(item.isbn);
			%>
			<tr>
				<td><%=book.isbn%></td>
				<td><%=book.title%></td>
				<td><%=book.year%></td>
				<td><%=book.publisher%></td>
				<td><%=WebUtil.formatPrice(book.price)%></td>
				<td><%=item.num%></td>
				<td><%=WebUtil.formatPrice(book.price * item.num)%></td>
				<td>
					<input type="text" size="1" value="1" id="<%="num" + id%>"/>
					<span>
						<a href='javascript:addToCart("<%=book.isbn%>", document.getElementById("<%="num" + id%>").value)'>Add</a>
					</span>
					<span style="padding-left:5%">
						<a href='javascript:addToCart("<%=book.isbn%>", "-" + document.getElementById("<%="num" + id%>").value)'>Drop</a>
					</span>
					<span style="padding-left:5%">
						<a href="<%="showdetails.jsp?isbn=" + book.isbn%>" target="_blank">Details</a>
					</span>
				</td>
			</tr>
			<%
			}
		%>

	</table>
</div>

<% } /* books != null */ %>

</body>

</html>
