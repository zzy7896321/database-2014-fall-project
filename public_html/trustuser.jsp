<%@ page import="acmdb.*" %>


<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}
	
	String strust = request.getParameter("trust");
	int trust = WebUtil.parseInt(strust, -2);
	if (trust < -1 || trust > 1) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String clogin = request.getParameter("clogin");
	if (clogin == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String returnpage = null;

	String isbn = request.getParameter("isbn");
	String back = request.getParameter("back");
	String qn = request.getParameter("qn");
	String pn = request.getParameter("pn");
	
	if (isbn != null) {
		returnpage = "showdetails.jsp?isbn=" + isbn;
		if (back != null) {
			returnpage += "&back="  + back;
		}
		if (qn != null) {
			returnpage += "&qn=" + qn;
		}
		if (pn != null) {
			returnpage += "&pn=" + pn;
		}
	}
	else {
		returnpage = "index.jsp";
	}
	
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		/* results ignored */
		sql.updateTrustState(user.login, clogin, trust);
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
	response.sendRedirect(returnpage);
%>

