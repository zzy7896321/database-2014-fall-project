<%@ page import="acmdb.*,java.util.ArrayList" %>

<!DOCTYPE html>
<%
	String ulogin = request.getParameter("login");
	String upasswd = request.getParameter("passwd");
	
	String redirPage = request.getParameter("from");
	if (redirPage == null) {
		redirPage = "index.jsp";
	}

	if (ulogin != null) {
		SQL sql = WebUtil.getSQL();
		User user = null;
		if (sql != null) try {
			user = new User();
			if (sql.userLogin(ulogin, upasswd, user) != SQL.SUCCESS) {
				user = null;
			}
		}
		finally {
			WebUtil.closeSQL(sql);
			sql = null;
		}
		
		if (user != null) {
			session.setAttribute("user", user);
			session.setAttribute("cart", new Order(user.login));
			
			session.setAttribute("lowest_qn", 0);
			session.setAttribute("qn_first_in_queue", 0);
			session.setAttribute("qn_length", 0);
		
			int queue_size = WebUtil.bookQueryQueueSize();
			session.setAttribute("book_query_results", new ArrayList[queue_size]);
			session.setAttribute("book_query_type", new String[queue_size]);
			session.setAttribute("book_query", new BookQuery[queue_size]);

			response.sendRedirect(redirPage);
			return ;
		}
	}

%>

<html>
<head>

<title>Bookstore</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>

<style>

#login {
	width: 350px;
	margin: 0px;
}

#login label, input {
	display: inline-block;
}

#login label {
	width: 80px;
	text-align: right;
}

#login label + input {
	margin: 0 10px 0 0;
}

#login input + label {
	margin: 4px 0 0 0;
}

#login p {
	margin: 0 0 0 0;
	padding: 8px 0 0 0;
}

#reg input[type="submit"] {
	margin: 0 0 0 60px;
}

</style>

</head>

<body>
<h1>Welcome to the bookstore.</h1>

<form id="login" name="login" action="<%="login.jsp?from=" + redirPage%>" method="post">
	<label for="login_login" >user:</label>
	<input type="text" id="login_login" name="login" 
	value="<%=WebUtil.getString(ulogin)%>"
	autocomplete="off" size=30/>

	<p>
	<label for="login_passwd">password:</label>
	<input type="password" id="login_passwd" name="passwd" autocomplete="off" size=30/>
	
	<p>
	<input type="submit" value="login"/>
	<% if (ulogin != null) { %>
	<span style="margin-left:20px;color:red;">Invalid login or password.</span>
	<% } %>
</form>

<p>
<a href="register.jsp">register</a>

<br />
Our username and password of administration account are same as those in phase 1.

</body>

</html>
