<%@ page import="acmdb.*" %>


<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	Order order = (Order) session.getAttribute("cart");
	order.items.clear();
	
	response.sendRedirect("cart.jsp");
%>

