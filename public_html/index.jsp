<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>


</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp");
		return ;
	}
%>

<body>
<h1>Welcome to the bookstore, <%=user.login%>.</h1>

<div id="navbar">
	<ul>
		<li><a href="index.jsp">Home</a>
		<li><a href="logout.jsp">Logout</a>
		<li><a href="cart.jsp">My cart</a>
	</ul>
</div>

<div id="menu">
	<ul>
		<li><a href="userinfo.jsp">My personal info</a>
		<li><a href="searchbooks.jsp">Search books</a>
		<li><a href="recommend.jsp">Recommend books</a>
		<li><a href="distance.jsp">Author distance</a>
		<li><a href="pastorders.jsp">Past orders</a>
	</ul>
</div>

<% if (user.usertype == SQL.UTYPE_ADMIN) { %>

<div id="man_menu">
	<ul>
		<li><a href="management.jsp">Management</a>
	</ul>
</div>

<% } /* endif user.usertype */ %>

</body>

</html>
