<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<%
	session.removeAttribute("user");
	session.removeAttribute("cart");
	
	session.removeAttribute("lowest_qn");
	session.removeAttribute("qn_first_in_queue");
	session.removeAttribute("qn_length");

	session.removeAttribute("book_query_results");
	session.removeAttribute("book_query_type");
	session.removeAttribute("book_query");

	response.sendRedirect("login.jsp");
%>

<html>
<head>

<title>Bookstore</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>

</head>

<body>
Logout succeeded.
If forwarding doesn't work, <a href="index.jsp">click me</a>.
</body>

</html>
