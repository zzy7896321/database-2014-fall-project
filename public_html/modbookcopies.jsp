<%@ page import="acmdb.*,java.util.ArrayList" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - add new copies</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}
%>

<style>
#bookqueryresults div {
	padding: 0px;
}

#bookqueryresults table {
	width: 100%;
}

#bookqueryresults table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}

#bookqueryresults th, td {
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.isbn {
	width: 10%;
}

th.title {
	width: 35%;	
}

th.year {
	width: 5%;
}

th.publisher {
	width: 20%;
}

th.price {
	width: 5%;
}

th.number {
	width: 5%;
}

th.action {
	width: 20%;
}

#bookqueryresults ul {
	list-style-type: none;
	display: block;
	padding: 0px;
	margin: 0px;
}

#bookqueryresults ul, li {
	padding-left: 15px;
}

</style>

<body>

<h2>Add new copies</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<jsp:include page="searchforms.jsp"/>

<%
	String s_qn = request.getParameter("qn");
if (s_qn != null) {
	int qn = WebUtil.parseInt(s_qn, -1);
	
	int lowest_qn = WebUtil.extractInteger(session.getAttribute("lowest_qn"), -1);
	int qn_first = WebUtil.extractInteger(session.getAttribute("qn_first_in_queue"), -1);
	int qn_length = WebUtil.extractInteger(session.getAttribute("qn_length"), -1);
	if (lowest_qn == -1 || qn_first == -1 || qn_length == -1) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	
	ArrayList<Book>[] book_query_results = (ArrayList<Book>[]) session.getAttribute("book_query_results");
	if (book_query_results == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	int queue_size = WebUtil.bookQueryQueueSize();

	if (qn < 0 || !WebUtil.inRange(qn, lowest_qn, qn_length)) {
		%><p><span style="color:red">Invalid/expired query number.</span><%	
	} else {
%>
	<div id="bookqueryresults">
	<h3>Query results</h3>
		
<%
	int offset = (qn_first + WebUtil.offset(qn, lowest_qn)) % queue_size;
	ArrayList<Book> books = book_query_results[offset];
	if (books == null || books.isEmpty()) {
		%>No such books found.<%	
	} else {
		String s_pn = request.getParameter("pn");
		int pn = WebUtil.parseInt(s_pn, 0);

		final int entriesPerPage = 16;
		final int nPages = (books.size() + entriesPerPage - 1) / entriesPerPage;
		final String[] titles = {
			"ISBN",
			"Title",
			"Year",
			"Publisher",
			"Price",
			"#copies",
			"",
		};
		final String[] cssclass = {
			"isbn",
			"title",
			"year",
			"publisher",
			"price",
			"number",
			"action",
		};

		if (pn < 0) pn = 0;
		if (pn >= nPages) pn = nPages - 1;
		
		%>
		<script type="text/javascript">
			function addcopies(rid, num, isbn) {
				var f = document.getElementById("addcopiesform");
				f.rid.value = rid;
				f.num.value = num;
				f.isbn.value = isbn;
				f.submit();
			}
		</script>

		<form id="addcopiesform" style="display: none" action="addcopies.jsp" method="post"/>
			<input type="hidden" name="qn" value="<%=qn%>"/>
			<input type="hidden" name="pn" value="<%=pn%>"/>
			<input type="hidden" name="isbn"/>
			<input type="hidden" name="rid" />
			<input type="hidden" name="num" />
		</form>

			<table id="<%="bqtable_" + pn%>" >
				<tr>
				<%
					for (int cno = 0; cno < titles.length; ++cno) {
						%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
					}
				%>
				</tr>
				<%
					for (int rid = pn * entriesPerPage, tar = Math.min(books.size(), rid + entriesPerPage); rid < tar; ++rid) {
						Book book = books.get(rid);
					%>
						<tr>
							<td><%=book.isbn%></td>
							<td><%=book.title%></td>
							<td><%=book.year%></td>
							<td><%=book.publisher%></td>
							<td><%=WebUtil.formatPrice(book.price)%></td>
							<td><%=book.n_copies%></td>
							<td>
								<input type="text" size="1" value="1" id="<%="num" + rid%>" />
								<span><a href='javascript:addcopies("<%=rid%>", document.getElementById("<%="num" + rid%>").value, "<%=book.isbn%>")' >Add</a></span>
								<span><a href='javascript:addcopies("<%=rid%>", "-" + document.getElementById("<%="num" + rid%>").value, "<%=book.isbn%>")' >Drop</a></span>
								<span style="padding-left: 5%;"><a href="<%=
										String.format("showdetails.jsp?isbn=%s&back=modbookcopies.jsp&qn=%d&pn=%d",
											book.isbn, qn, pn)
								%>">Details</a></span>
							</td>
						</tr>
					<%
					}
				%>
			</table>
			
			
			<%
				final int npageIndices = 10;
				int leftmostpn = Math.max(pn - npageIndices / 2 + 1, 0); /* inclusive */
				int rightmostpn = Math.min(leftmostpn + npageIndices, nPages); /* exclusive */
				leftmostpn = Math.max(rightmostpn - npageIndices, 0);
				
			%>
			<div id="pageindex">
				<ul>
					<%
						if (leftmostpn > 0) {
						%><li><a href="<%=String.format("modbookcopies.jsp?qn=%d&pn=0", qn)%>" >First</a><%
						}

						if (pn > 0) {
						%><li><a href="<%=String.format("modbookcopies.jsp?qn=%d&pn=%d", qn, pn - 1)%>" >&lt;&lt;</a><%
						}
						
						for (int i = leftmostpn; i < rightmostpn; ++i) {
						if (i == pn) {
						%><li><%=i + 1%><%
						}
						else {
						%><li><a href="<%=String.format("modbookcopies.jsp?qn=%d&pn=%d", qn, i)%>"><%=i+1%></a><%	
						}
						}

						if (pn < nPages - 1) {
						%><li><a href="<%=String.format("modbookcopies.jsp?qn=%d&pn=%d", qn, pn + 1)%>">&gt;&gt;</a><%
						}

						if (rightmostpn < nPages) {
						%><li><a href="<%=String.format("modbookcopies.jsp?qn=%d&pn=%d", qn, nPages - 1)%>">Last</a><%
						}
					%>
				</ul>
			</div>
		<%
	}	/* if (books != null) */
	%>

	</div>
<% } /* qn is in range */  

} /* s_qn != null */ %>

</body>

</html>
