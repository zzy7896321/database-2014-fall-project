<%@ page import="acmdb.*,java.util.ArrayList" %>


<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}
	
	String isbn = request.getParameter("isbn");
	if (isbn == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String srid = request.getParameter("rid");
	if (srid == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	int rid = WebUtil.parseInt(srid, -1);

	String sqn = request.getParameter("qn");
	if (sqn == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	int qn = WebUtil.parseInt(sqn, -1);

	String pn = request.getParameter("pn");

	String returnpage = "modbookcopies.jsp?qn=" + sqn;
	if (pn != null) {
		returnpage += "&pn=" + pn;
	}

	int lowest_qn = WebUtil.extractInteger(session.getAttribute("lowest_qn"), -1);
	int qn_first = WebUtil.extractInteger(session.getAttribute("qn_first_in_queue"), -1);
	int qn_length = WebUtil.extractInteger(session.getAttribute("qn_length"), -1);
	if (lowest_qn == -1 || qn_first == -1 || qn_length == -1) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	
	ArrayList<Book>[] book_query_results = (ArrayList<Book>[]) session.getAttribute("book_query_results");
	if (book_query_results == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	int queue_size = WebUtil.bookQueryQueueSize();
	
	if (qn < 0 || !WebUtil.inRange(qn, lowest_qn, qn_length)) {
		%>
		<jsp:forward page="prompt.jsp">
		<jsp:param name="msg" value="Invalid query number"/>
		<jsp:param name="pagename" value="Add copies"/>
		<jsp:param name="redirpage" value="modbookcopies.jsp"/>
		</jsp:forward>
		<%
	}

	int offset = (qn_first + WebUtil.offset(qn, lowest_qn)) % queue_size;
	ArrayList<Book> books = book_query_results[offset];
	if (rid < 0 || rid >= books.size()) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	Book book = books.get(rid);
	if (!isbn.equals(book.isbn)) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String snum = request.getParameter("num");
	int num = WebUtil.parseInt(snum, 0);
	if (num == 0) {
		response.sendRedirect(returnpage);
		return ;
	}
	
	
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		if (sql.addNumCopies(isbn, num) == SQL.SUCCESS) {
			book.n_copies = (book.n_copies < -num) ? 0 : (book.n_copies + num);
		}
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
	response.sendRedirect(returnpage);
%>

