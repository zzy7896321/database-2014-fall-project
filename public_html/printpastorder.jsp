<%@ page import="acmdb.*" %>

<%
	PastOrder order = null;
	{
		Object o = request.getAttribute("pastorder");
		if (o != null && o instanceof PastOrder) {
			order = (PastOrder) o;
		}
		else {
			%>invalid parameter<%
			return ;
		}
	}
%>

<div>
	<table class="orderinfo">
		<tr>
			<td class="orderinfocaption">Order id</td>
			<td class="orderinfocontent"><%=order.oid%></td>
		</tr>
		<tr>
			<td class="orderinfocaption">Time</td>
			<td class="orderinfocontent"><%=order.time%></td>
		</tr>
		<tr>
			<td class="orderinfocaption">User</td>
			<td class="orderinfocontent"><%=order.login%></td>
		</tr>
		<tr>
			<td class="orderinfocaption">#Items</td>
			<td class="orderinfocontent"><%=order.getNum()%></td>
		</tr>
		<tr>
			<td class="orderinfocaption">Total price</td>
			<td class="orderinfocontent"><%=WebUtil.formatPrice(order.getTotalPrice())%></td>
		</tr>
	</table>
</div>

<%
	final String[] titles = {
		"ISBN",
		"Title",
		"Year",
		"Publisher",
		"Price",
		"Number",
		"Sum",
		"",
	};

	final String[] cssclass = {
		"pastorderisbn",
		"pastordertitle",
		"pastorderyear",
		"pastorderpublisher",
		"pastorderprice",
		"pastordernumber",
		"pastordersum",
		"pastorderaction",
	};
%>

<div>
	<table class="pastorder">
	<tr>
	<%
		for (int cno = 0; cno < titles.length; ++cno) {
			%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
		}
	%>
	</tr>

	<%
		int n = order.nums.size();
		for (int id = 0; id < n; ++id) {
			Book book = order.books.get(id);
			int num = order.nums.get(id);
		%>
		<tr>
			<td class="pastorder"><%=book.isbn%></td>
			<td class="pastorder"><%=book.title%></td>
			<td class="pastorder"><%=book.year%></td>
			<td class="pastorder"><%=book.publisher%></td>
			<td class="pastorder"><%=WebUtil.formatPrice(book.price)%></td>
			<td class="pastorder"><%=num%></td>
			<td class="pastorder"><%=WebUtil.formatPrice(book.price * num)%></td>
			<td class="pastorder">
				<a href="<%="showdetails.jsp?isbn=" + book.isbn%>" target="_blank">View book</a>	
			</td>
		</tr>
		<%
		}
	%>

	</table>
</div>
