<!DOCTYPE html>
<html>
<head>

<title>Bookstore</title>

<%
	String redirpage = request.getParameter("redirpage");
	String pagename = null;
	if (redirpage == null) {
		redirpage = "index.jsp";
		pagename = "home page";
	}
	else {
		pagename = request.getParameter("pagename");
		if (pagename == null) {
			pagename = redirpage;
		}
	}
%>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>

<meta http-equiv="refresh" content="5,<%=redirpage%>"/>

</head>

<body>
<%
	String msg = request.getParameter("msg");
	if (msg == null) {
		%>Oops. Something got wrong.<%
	}
	else {
		%><%=msg%><%
	}

%>
<p>
You will be redirected back to <a href="<%=redirpage%>"><%=pagename%></a> in 5 seconds.

</body>

</html>
