<%@ page import="acmdb.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Pop publishers</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="publisher" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}

	Order order = (Order) session.getAttribute("cart");
	String popn = request.getParameter("popn");
	int n = WebUtil.parseInt(popn, -1);
	if (popn == null || popn.equals(""))
		n = 10;
	boolean invalid = (n < 0);
	
%>

<style>
#publisherqueryresults div {
	padding: 0px;
}

#publisherqueryresults table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}

#publisherqueryresults th, td {
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.index {
	width: 20%;	
}

th.sales {
	width: 20%;	
}

th.name {
	width: 60%;	
}


#publisherqueryresults ul {
	list-style-type: none;
	display: block;
	padding: 0px;
	margin: 0px;
}

#publisherqueryresults ul, li {
	padding-left: 15px;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

}

</style>
<body>

<h2>Pop publishers</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<div id="pop">
<form id="pop_form" name="pop_form" action="poppublishers.jsp" autocomplete="off" method="post">
	<p>
	<input type="text" id="pop_text" name="popn" size=50 value="<%=invalid?popn:""%>" placeholder="top n popular publishers(0 for unlimited, empty for ten)"/>
	

	<input type="submit" value="submit"/>
	<span id="prompt_id" class="error_prompt"><%=invalid ? "Invalid number" : ""%></span>

</form>
</div>
		
<%
	if (n>=0) {
		%>
	<div id="publisherqueryresults">
	<h3>Query results</h3>
	<%
			SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		ArrayList<Pair<String, Integer>> pubs = new ArrayList<Pair<String, Integer>>();
		int result = sql.queryMostPopularPublishers(pubs, n);
		if (result != SQL.SUCCESS) { %>
			Query failed;
		<%
		}
		else if (pubs == null || pubs.isEmpty()) {
		%>No such publishers found.<%	
	} else {

		final String[] titles = {
			"Rank",
			"Sales",
			"Publisher",
		};
		final String[] cssclass = {
			"index",
			"sales",
			"names",
		};
		
		%>
			<table id="bqtable" >
				<tr>
				<%
					for (int cno = 0; cno < titles.length; ++cno) {
						%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
					}
				%>
				</tr>
				<%
					for (int rid = 0, tar = pubs.size(); rid < tar; ++rid) {
						Pair<String, Integer> pub = pubs.get(rid);
					%>
						<tr>
							<td><%=rid+1%></td>
							<td><%=pub.second%></td>
							<td><%=pub.first%></td>
						</tr>
					<%
					}
				%>
			</table>
			
			
		<%
	}	
		} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	}
	%>

	</div>

</body>

</html>
