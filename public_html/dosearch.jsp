<%@ page import="acmdb.*,java.util.ArrayList" isThreadSafe="true" %>

<%
	User user = (User) session.getAttribute("user");	
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	String submitPage = request.getParameter("submitpage");
	if (submitPage == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	
	String searchType = request.getParameter("searchtype");
	if (searchType == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	if (!searchType.equals("b") && !searchType.equals("a")) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	SQL sql = WebUtil.getSQL();
	ArrayList<Book> books = null;
	BookQuery query = null;
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
				
		if (searchType.equals("b")) {
			String queryString = request.getParameter("basic_search_input");
			String orderby = request.getParameter("orderby");
			query = WebUtil.getBasicBookQuery(queryString, orderby);
		}
	
		else if (searchType.equals("a")) {

			String title = request.getParameter("title");
			String isbn = request.getParameter("ISBN");
			String author = request.getParameter("author");
			String publisher = request.getParameter("publisher");
			String subject = request.getParameter("subject");
			String keywords = request.getParameter("keywords");
			String orderby = request.getParameter("orderby");

			query = WebUtil.getAdvancedBookQuery(title, isbn, author, publisher, subject, keywords, orderby);
		}

		if (query != null) {
			books = WebUtil.queryBooks(sql, user.login, query);
		}
		

	} finally {
		sql.close();
		sql = null;
	}

	int lowest_qn = WebUtil.extractInteger(session.getAttribute("lowest_qn"), -1);
	int qn_first = WebUtil.extractInteger(session.getAttribute("qn_first_in_queue"), -1);
	int qn_length = WebUtil.extractInteger(session.getAttribute("qn_length"), -1);
	if (lowest_qn == -1 || qn_first == -1 || qn_length == -1) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	ArrayList<Book>[] book_query_results = (ArrayList<Book>[]) session.getAttribute("book_query_results");
	String[] book_query_type = (String[]) session.getAttribute("book_query_type");
	BookQuery[] book_query = (BookQuery[]) session.getAttribute("book_query");
	if (book_query_results == null || book_query_type == null || book_query == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	
	int queue_size = WebUtil.bookQueryQueueSize();
	int target = 0;
	if (qn_length >= queue_size) {
		if (++lowest_qn < 0) lowest_qn = 0;
		target = qn_first;
		qn_first = (qn_first + 1) % queue_size;
	}
	else {
		qn_length += 1;
		target = (qn_first + qn_length - 1) % queue_size;
	}
	int qn = WebUtil.addWrapAround(lowest_qn, qn_length - 1);
	
	book_query_results[target] = books;
	book_query_type[target] = searchType;
	book_query[target] = query;

	session.setAttribute("lowest_qn", lowest_qn);
	session.setAttribute("qn_first_in_queue", qn_first);
	session.setAttribute("qn_length", qn_length);

	response.sendRedirect(submitPage + "?qn=" + qn);
%>

