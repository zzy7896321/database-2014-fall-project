<%@ page import="acmdb.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Most useful users</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="user" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}

	Order order = (Order) session.getAttribute("cart");
	String popn = request.getParameter("popn");
	int n = WebUtil.parseInt(popn, -1);
	if (popn == null || popn.equals(""))
		n = 10;
	boolean invalid = (n < 0);
	
%>

<style>
#userqueryresults div {
	padding: 0px;
}

#userqueryresults table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}

#userqueryresults th, td {
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.index {
	width: 5%;	
}

th.score {
	width: 5%;	
}

th.login {
	width: 20%;	
}

th.phone {
	width: 10%;	
}

th.name {
	width: 25%;	
}

th.address {
	width: 30%;	
}

th.usertype {
	width: 5%;
}

#userqueryresults ul {
	list-style-type: none;
	display: block;
	padding: 0px;
	margin: 0px;
}

#userqueryresults ul, li {
	padding-left: 15px;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

}

</style>
<body>

<h2>Most useful users</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<div id="pop">
<form id="pop_form" name="pop_form" action="mostuseful.jsp" autocomplete="off" method="post">
	<p>
	<input type="text" id="pop_text" name="popn" size=50 value="<%=invalid?popn:""%>" placeholder="top n useful users(0 for unlimited, empty for ten)"/>
	

	<input type="submit" value="submit"/>
	<span id="prompt_id" class="error_prompt"><%=invalid ? "Invalid number" : ""%></span>

</form>
</div>
		
<%
	if (n>=0) {
		%>
	<div id="userqueryresults">
	<h3>Query results</h3>
	<%
			SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		ArrayList<User> users = new ArrayList<User>();
		int result = sql.queryMostUsefulUsers(users, n);
		if (result != SQL.SUCCESS) { %>
			Query failed;
		<%
		}
		else if (users == null || users.isEmpty()) {
		%>No such users found.<%	
	} else {

		final String[] titles = {
			"Rank",
			"Score",
			"Login",
			"Phone",
			"Name",
			"Address",
			"Usertype", 
		};
		final String[] cssclass = {
			"index",
			"score",
			"login",
			"phone",
			"name",
			"address",
			"usertype",
		};
		
		%>
			<table id="bqtable" >
				<tr>
				<%
					for (int cno = 0; cno < titles.length; ++cno) {
						%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
					}
				%>
				</tr>
				<%
					for (int rid = 0, tar = users.size(); rid < tar; ++rid) {
						User u = users.get(rid);
					%>
						<tr>
							<td><%=rid+1%></td>
							<td><%=(Double)u.a_field%></td>
							<td><%=u.login%></td>
							<td><%=u.phone%></td>
							<td><%=u.name%></td>
							<td><%=u.address%></td>
							<td><%=u.usertype==SQL.UTYPE_ADMIN ? "admin" : "user"%></td>
						</tr>
					<%
					}
				%>
			</table>
			
			
		<%
	}	
		} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	}
	%>

	</div>

</body>

</html>
