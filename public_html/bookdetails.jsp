<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - template</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	Order order = (Order) session.getAttribute("cart");
	
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {

	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
%>

<body>

<h2>Template</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>

<!-- edit here -->

</body>

</html>
