<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Create new administrator account</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}

	String login = request.getParameter("login");
	String passwd = request.getParameter("passwd");
	String phone = request.getParameter("phone");
	String name = request.getParameter("name");
	String addr = request.getParameter("addr");
	int userregResult = SQL.SUCCESS;
	boolean success = false;
	if (login != null) {

		SQL sql = WebUtil.getSQL(); 
		if (sql == null) {
			%><jsp:forward page="prompt.jsp"/><%
		} else try {
			userregResult = sql.adminReg(login, passwd, phone, name, addr);

			if (userregResult == SQL.SUCCESS) {
				success = true;
				login = passwd = phone = name = addr = null;
			}
		} /* endof sql try */
		finally {
			sql.close();
			sql = null;
		}
	}

%>

<style>
#reg form {
	width: 1200px;
	margin: 20px 0 0 0;
}

#reg label, input {
	display: inline-block;
	margin: 0;
}

#reg label {
	width: 120px;
	text-align: right;
}

#reg input + label {
	text-align: left;
	margin: 0 0 0 20px;
}

#reg input[type="submit"] {
	margin: 0 0 0 60px;
}

#reg p {
	margin: 0 0 0 0;
	padding: 8px 0 0 0;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

span.field_info {
	margin-left: 20px;
}

</style>

<script type="text/javascript" language="javascript">

function checkRegistrationForm() {
	document.getElementById("prompt").innerHTML="";
	var ret = true;
	var login = document.getElementById("reg_login").value;
	if (login.length == 0 || login.length > 30) {
		document.getElementById("invalid_login_prompt").innerHTML = "Invalid username";
		ret = false;
	}
	else {
		document.getElementById("invalid_login_prompt").innerHTML = "";
	}
	
	var passwd = document.getElementById("reg_passwd").value;
	if (passwd.length < 1 || passwd.length > 30) {
		document.getElementById("invalid_passwd_prompt").innerHTML = "Invalid password";
		document.getElementById("passwd_mismatch_prompt").innerHTML = "";
		ret = false;
	}
	else {
		document.getElementById("invalid_passwd_prompt").innerHTML = "";
	
		var confirm_passwd = document.getElementById("reg_passwd_confirm").value;
		if (passwd != confirm_passwd) {
			document.getElementById("passwd_mismatch_prompt").innerHTML = "Password mismatch";
			ret = false;
		}
		else {
			document.getElementById("passwd_mismatch_prompt").innerHTML = "";
		}
	}

	var phone = document.getElementById("reg_phone").value;
	if (phone.length > 16) {
		document.getElementById("too_long_phone").innerHTML = "Too long";
		ret = false;
	}
	else {
		document.getElementById("too_long_phone").innerHTML = "";
	}

	var name = document.getElementById("reg_name").value;
	if (name.length > 100) {
		document.getElementById("too_long_name").innerHTML = "Too long";
		ret = false;
	}
	else {
		document.getElementById("too_long_name").innerHTML = "";
	}

	var addr = document.getElementById("reg_addr").value;
	if (addr.length > 255) {
		document.getElementById("too_long_addr").innerHTML = "Too long";
		ret = false;
	}
	else {
		document.getElementById("too_long_addr").innerHTML = "";
	}

	return ret;	
}

</script>

<body>

<h2>Create new administrator account</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<div id="reg">
<form id="reg_form" name="reg_form" action="addadmin.jsp" autocomplete="off" 
	onsubmit="return checkRegistrationForm()" method="post">
	<label for="reg_login"><span style="color:red">*</span>user</label>
	<input type="text" id="reg_login" name="login" autofocus size=30
		value="<%=(login == null) ? "" : login%>"	/>
	<span class="field_info">1 ~ 30 characters.</span>
	<span id="invalid_login_prompt" class="error_prompt"><%=(userregResult == SQL.EDUPLOGIN) ? "The username is already used." : ""%></span>

	<p>
	<label for="reg_passwd"><span style="color:red">*</span>password</label>
	<input type="password" id="reg_passwd" name="passwd" size=30 />
	<span class="field_info">1 ~ 30 characters.</span>
	<span id="invalid_passwd_prompt" class="error_prompt"></span>

	<p>
	<label for="reg_passwd_confirm"><span style="color:red">*</span>confirm password</label>
	<input type="password" id="reg_passwd_confirm" size=30 />
	<span id="passwd_mismatch_prompt" class="error_prompt"></span>
	
	<p>
	<label for="reg_phone">phone</label>
	<input type="text" id="reg_phone" name="phone" size=30 
		value="<%=(phone == null) ? "" : phone%>" />
	<span class="field_info">0 ~ 16 characters.</span>
	<span id = "too_long_phone" class="error_prompt"></span>

	<p>
	<label for="reg_name">real name</label>
	<input type="text" id="reg_name" name="name" size=30 
		value="<%=(name == null) ? "" : name%>" />
	<span class="field_info">no more than 100 characters.</span>
	<span id = "too_long_name" class="error_prompt"></span>

	<p>
	<label for="reg_addr">address</label>
	<input type="text" id="reg_addr" name="addr" size=60 
		value="<%=(addr == null) ? "" : addr%>" />
	<span class="field_info">no more than 255 characters.</span>
	<span id = "too_long_addr" class="error_prompt"></span>

	<p>
	<input type="submit" value="submit"/>

</form>
</div>

<p>

Items with <span style="color:red">*</span> are required.

<p>
<label id="prompt">
<% if (success) { %>
<b>Create succeed</b>
<%} else if (userregResult != SQL.SUCCESS) { %>
<b>Unknown error</b>
<% } %>
</label>

</body>

</html>
