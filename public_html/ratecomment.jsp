<%@ page import="acmdb.*" %>


<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}
	
	String srate = request.getParameter("rate");
	int rate = WebUtil.parseInt(srate, -1);
	if (rate < 0 || rate > 2) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String clogin = request.getParameter("clogin");
	if (clogin == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String isbn = request.getParameter("isbn");
	if (isbn == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String back = request.getParameter("back");
	String qn = request.getParameter("qn");
	String pn = request.getParameter("pn");

	String returnpage = "showdetails.jsp?isbn=" + isbn;
	if (back != null) {
		returnpage += "&back="  + back;
	}
	if (qn != null) {
		returnpage += "&qn=" + qn;
	}
	if (pn != null) {
		returnpage += "&pn=" + pn;
	}
	
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		/* results ignored */
		SQL.strerror(sql.postRating(isbn, clogin, user.login, rate));
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
	response.sendRedirect(returnpage);
%>

