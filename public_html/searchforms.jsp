<%@ page import="acmdb.*" %>

<%
	String basic_search_input = "";
	String title = "";
	String ISBN = "";
	String author = "";
	String publisher = "";
	String subject = "";
	String keywords = "";
	
	int orderbyIndex = 0;
	
	String searchtype = "b";
	String s_qn = request.getParameter("qn");
	int qn = WebUtil.parseInt(s_qn, -1);
	if (qn >= 0) {
		int lowest_qn = WebUtil.extractInteger(session.getAttribute("lowest_qn"), -1);
		int qn_first = WebUtil.extractInteger(session.getAttribute("qn_first_in_queue"), -1);
		int qn_length = WebUtil.extractInteger(session.getAttribute("qn_length"), -1);
		if (lowest_qn == -1 || qn_first == -1 || qn_length == -1) {
			%><jsp:forward page="prompt.jsp"/><%
		}
	
		String[] book_query_type = (String[]) session.getAttribute("book_query_type");
		BookQuery[] book_query = (BookQuery[]) session.getAttribute("book_query");
		if (book_query_type == null || book_query == null) {
			%><jsp:forward page="prompt.jsp"/><%
		}

		int queue_size = WebUtil.bookQueryQueueSize();
		if (WebUtil.inRange(qn, lowest_qn, qn_length)) {
			
			int i = (qn_first + WebUtil.offset(qn, lowest_qn)) % queue_size;
			searchtype = book_query_type[i];
			if (searchtype == null) searchtype = "b";
			BookQuery query = book_query[i];
	
			if (query != null) {
				if (searchtype.equals("b")) {
					if (query.isbn != null) {
						basic_search_input = query.isbn;	
					}
					else {
						basic_search_input = WebUtil.getString(query.title);
					}
					orderbyIndex = query.orderby;
				}
				else {
					title = WebUtil.getString(query.title);
					ISBN = WebUtil.getString(query.isbn);
					author = WebUtil.getString(query.author);
					publisher = WebUtil.getString(query.publisher);
					subject = WebUtil.getString(query.subject);
					keywords = WebUtil.getString(query.keywords);
					orderbyIndex = query.orderby;
				}
			}
		}
	}

	String submitPage = request.getParameter("submitpage");
	if (submitPage == null) {
		submitPage = WebUtil.basename(request.getRequestURI());
	}
%>

<script type="text/javascript">
function showAdvancedSearch() {
	document.getElementById("basic_search").style.display="none";
	document.getElementById("advanced_search").style.display="block";
}

function showBasicSearch() {
	document.getElementById("basic_search").style.display="block";
	document.getElementById("advanced_search").style.display="none";
}

function setOrderby(form) {
	form.orderby.value = document.getElementById("orderby").selectedIndex;
	return true;
}

function resetOrderby() {
	document.getElementById("orderby").selectedIndex =<%=orderbyIndex %>;
}

function resetBasicSearchForm() {
	resetOrderby();
	var form = document.getElementById("basic_search_form");
	form.basic_search_input.value = "<%=basic_search_input%>";
}

function resetAdvancedSearchForm() {
	resetOrderby();
	var form = document.getElementById("advanced_search_form");
	form.title.value = "<%=title%>";
	form.ISBN.value = "<%=ISBN%>";
	form.author.value = "<%=author%>";
	form.publisher.value = "<%=publisher%>";
	form.subject.value = "<%=subject%>";
	form.keywords.value = "<%=keywords%>";
}

</script>

<label for="orderby">OrderBy</label>
<select id="orderby">
	<option>unspecified</option>
	<option>year of publication DESC</option>
	<option>year of publication ASC</option>
	<option>avg. score DESC</option>
	<option>avg. score ASC</option>
	<option>avg. score from the trusted DESC</option>
	<option>avg. score from the trusted ASC</option>
</select>

<script type="text/javascript">
	document.getElementById("orderby").selectedIndex = <%=orderbyIndex%>;
</script>


<div id="basic_search" class="searchform">
<form id="basic_search_form" method="post" action="dosearch.jsp" 
	autocomplete="false" onsubmit="return setOrderby(this)">
	
	<input type="text" id="basic_search_input" name="basic_search_input" size=70
		value="<%=basic_search_input%>" placeholder="ISBN/Book title"/>
	
	<input type="hidden" name="searchtype" value="b"/>
	<input type="hidden" name="orderby"/>
	<input type="hidden" name="submitpage" value="<%=submitPage%>"/>
	<input type="submit" value="search">

	<p>
	<a href="javascript:showAdvancedSearch()">Advanced search</a>
</form>
</div>


<div id="advanced_search" class="searchform" style="display:none">
<form id="advanced_search_form" method="post" action="dosearch.jsp" 
	autocomplete="false" onsubmit="return setOrderby(this)">

	<label for="title">Title</label>
	<input type="text" id="title" name="title" size=50
		value="<%=title%>"/>
	
	<p>
	<label for="ISBN">ISBN</label>
	<input type="text" id="ISBN" name="ISBN" size=50
		value="<%=ISBN%>"/>

	<p>
	<label for="author">Author</label>
	<input type="text" id="author" name="author" size=50
		value="<%=author%>"/>
	
	<p>
	<label for="publisher">Publisher</label>
	<input type="text" id="publisher" name="publisher" size=50
		value="<%=publisher%>"/>
	
	<p>
	<label for="subject">Subject</label>
	<input type="text" id="subject" name="subject" size=50
		value="<%=subject%>"/>

	<p>
	<label for="keywords">Keywords</label>
	<input type="text" id="keywords" name="keywords" size=50
		value="<%=keywords%>"/>

	<input type="hidden" name="searchtype" value="a"/> 
	<input type="hidden" name="orderby" />
	<input type="hidden" name="submitpage" value="<%=submitPage%>"/>

	<p>
	<input type="submit" value="search">

	<p>
	<a href="javascript:showBasicSearch()">Basic search</a>
</form>
</div>

<%
	if (searchtype != null && searchtype.equals("a")) {
		%><script type="text/javascript">showAdvancedSearch()</script><%
	}
%>


