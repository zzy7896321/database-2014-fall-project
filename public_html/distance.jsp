<%@ page import="acmdb.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Distance</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}
	
	String author1 = request.getParameter("author1");
	String author2 = request.getParameter("author2");
	ArrayList<Long> ids1 = new ArrayList<Long>();
	ArrayList<Long> ids2 = new ArrayList<Long>();
	boolean invalid1 = false;
	boolean invalid2 = false;
	String authorID1 = request.getParameter("authorID1");
	String authorID2 = request.getParameter("authorID2");
	Pointer<Integer> dist = null;
	
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		if (authorID1 != null && authorID2 != null) {
			long ID1 = Long.parseLong(authorID1);
			long ID2 = Long.parseLong(authorID2);
			dist = new Pointer<Integer>();
			sql.queryAuthorDistance(ID1, ID2, dist);
		}
		else if (author1 != null && author2 != null) {
			sql.queryAuthorID(author1, ids1);
			invalid1 = (ids1.size() == 0);
			sql.queryAuthorID(author2, ids2);
			invalid2 = (ids2.size() == 0);
		}
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
%>

<style>
#book form {
	width: 1200px;
	margin: 20px 0 0 0;
}

#book label, input {
	display: inline-block;
	margin: 0;
}

#book label {
	width: 120px;
	text-align: right;
}

#book input + label {
	text-align: left;
	margin: 0 0 0 20px;
}

#book input[type="submit"] {
	margin: 0 0 0 60px;
}

#book p {
	margin: 0 0 0 0;
	padding: 8px 0 0 0;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

span.field_info {
	margin-left: 20px;
	

}

</style>
<body>

<h2>Distance</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>

<%
if (dist != null) {
%>
	The distance between <b><%=author1%></b> and <b><%=author2%></b> is <b><%=dist.value==null?"inf":dist.value%></b>
	<br />
	<a href="distance.jsp">Try again</a>
<%
}
else {
%>
<div id="dist">
<form id="dist_form" name="book_form" action="distance.jsp" autocomplete="off" method="post">
	<p id="dist_p1">
	<label for="dist_author1">Author1</label>
	<input type="text" id="dist_author1" name="author1" size=30 value="<%=author1 == null ? "" : author1%>" />
	<span class="error_prompt"><%=invalid1 ? "Author does not exist" : ""%></span>
	
	<p id="dist_p2">
	<label for="dist_author2">Author2</label>
	<input type="text" id="dist_author2" name="author2" size=30 value="<%=author2 == null ? "" : author2%>" />
	<span class="error_prompt"><%=invalid2 ? "Author does not exist" : ""%></span>

	<p>
	<input type="submit" value="submit"/>

</form>
</div>

	<%
	if ( author1 != null && author2 != null && invalid1 == false && invalid2 == false) {
	%>
<script type="text/javascript">
	document.getElementById("dist_author1").readOnly = "readonly";
	var list = document.getElementById("dist_form");
	list.insertBefore(document.createElement("br"), list.lastChild);
	var div = document.createElement("div");
	div.style.marginLeft = "64px";
	
	<%
	sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		long eID = ids1.get(0);
		for (Long id : ids1) {
		%>
			var radio = document.createElement("input");
			radio.type="radio";
			radio.name="authorID1";
			radio.value="<%=id%>";
			<%
			if (eID == id){
			%>
				radio.checked="checked";
			<%
			}
			%>
			div.appendChild(radio);
			div.appendChild(document.createTextNode("<%=author1%>"));
			var ul = document.createElement("ul");
			<%
			ArrayList<Book> books = new ArrayList<Book>();
			sql.queryBookByAuthorID(id, books, 0);
			for (Book book : books) {
			%>
				var li = document.createElement("li");
				li.appendChild(document.createTextNode("<%=book.title%>"));
				ul.appendChild(li); 
			<%
			}
			%>
			div.appendChild(ul);
		<%
		}
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	%>
	list.insertBefore(div, document.getElementById("dist_p2"));
	document.getElementById("dist_author2").readOnly = "readonly";
	var list = document.getElementById("dist_form");
	var div = document.createElement("div");
	div.style.marginLeft = "64px";	
	<%
	sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		long eID = ids2.get(0);
		for (Long id : ids2) {
		%>
			var radio = document.createElement("input");
			radio.type="radio";
			radio.name="authorID2";
			radio.value="<%=id%>";
			<%
			if (eID == id){
			%>
				radio.checked="checked";
			<%
			}
			%>
			div.appendChild(radio);
			div.appendChild(document.createTextNode("<%=author2%>"));
			var ul = document.createElement("ul");
			<%
			ArrayList<Book> books = new ArrayList<Book>();
			sql.queryBookByAuthorID(id, books, 0);
			for (Book book : books) {
			%>
				var li = document.createElement("li");
				li.appendChild(document.createTextNode("<%=book.title%>"));
				ul.appendChild(li); 
			<%
			}
			%>
			div.appendChild(ul);
		<%
		}
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	%>
	
	list.insertBefore(div, list.lastChild);	
</script>
	
<% }} %>		
	

</body>

</html>
