<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - UserInfo</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}
	
	
	String login = user.login;
	int updateResult = -1;
	
	String newpw = request.getParameter("newpw");
	String oldpw = request.getParameter("oldpw");
	String newphone = request.getParameter("phone");
	String newname = request.getParameter("name");
	String newaddr = request.getParameter("addr");
	
	if (newphone != null) {
		SQL sql = WebUtil.getSQL(); 
		if (sql == null) {
			%><jsp:forward page="prompt.jsp"/><%
		} else try {
			if (oldpw == "")
				updateResult = sql.userUpdateAllInfo(login, null, null, newphone, newname, newaddr);
			else
				updateResult = sql.userUpdateAllInfo(login, oldpw, newpw, newphone, newname, newaddr);

			if (updateResult == SQL.SUCCESS) {
					user.phone = newphone;
					user.name = newname;
					user.address = newaddr;
			}
		} /* endof sql try */
		finally {
			sql.close();
			sql = null;
		}
	}

	String phone = user.phone;
	String name = user.name;
	String addr = user.address;
	
%>

<style>
#update form {
	width: 1200px;
	margin: 20px 0 0 0;
}

#update label, input {
	display: inline-block;
	margin: 0;
}

#update label {
	width: 120px;
	text-align: right;
}

#update input + label {
	text-align: left;
	margin: 0 0 0 20px;
}

#update input[type="submit"] {
	margin: 0 0 0 60px;
}

#update p {
	margin: 0 0 0 0;
	padding: 8px 0 0 0;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

span.field_info {
	margin-left: 20px;
}

</style>

<script type="text/javascript" language="javascript">

function checkRegistrationForm() {
	var ret = true;
	
	var passwd_old = document.getElementById("update_passwd_old").value;
	var passwd = document.getElementById("update_passwd_new").value;
	var confirm_passwd = document.getElementById("update_passwd_confirm").value;
	
	if (passwd_old != "" || passwd != "" || confirm_passwd != "") {
	if (passwd_old.length < 1 || passwd_old.length > 30) {
		document.getElementById("invalid_passwd_prompt_old").innerHTML = "Invalid password";
		ret = false;
	}
	
	
	if (passwd.length < 1 || passwd.length > 30) {
		document.getElementById("invalid_passwd_prompt_new").innerHTML = "Invalid password";
		document.getElementById("passwd_mismatch_prompt").innerHTML = "";
		ret = false;
	}
	else {
		document.getElementById("invalid_passwd_prompt_new").innerHTML = "";
	
		
		if (passwd != confirm_passwd) {
			document.getElementById("passwd_mismatch_prompt").innerHTML = "Password mismatch";
			ret = false;
		}
		else {
			document.getElementById("passwd_mismatch_prompt").innerHTML = "";
		}
	}
	}
	
	var phone = document.getElementById("update_phone").value;
	if (phone.length > 16) {
		document.getElementById("too_long_phone").innerHTML = "Too long";
		ret = false;
	}
	else {
		document.getElementById("too_long_phone").innerHTML = "";
	}

	var name = document.getElementById("update_name").value;
	if (name.length > 100) {
		document.getElementById("too_long_name").innerHTML = "Too long";
		ret = false;
	}
	else {
		document.getElementById("too_long_name").innerHTML = "";
	}

	var addr = document.getElementById("update_addr").value;
	if (addr.length > 255) {
		document.getElementById("too_long_addr").innerHTML = "Too long";
		ret = false;
	}
	else {
		document.getElementById("too_long_addr").innerHTML = "";
	}

	return ret;	
}

</script>

<body>

<h2>UserInfo</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>


<div id="update">
<form id="update_form" name="update_form" action="userinfo.jsp" autocomplete="off" 
	onsubmit="return checkRegistrationForm()" method="post">
	
	<label><b>Account</b></label>
	<p>
	
	<label>user</label>
	<%=login%>
	
	<p>
	<label for="update_passwd">old password</label>
	<input type="password" id="update_passwd_old" name="oldpw" size=30 />
	<span id="invalid_passwd_prompt_old" class="error_prompt"></span>
	
	<p>
	<label for="update_passwd">new password</label>
	<input type="password" id="update_passwd_new" name="newpw" size=30 />
	<span class="field_info">1 ~ 30 characters.</span>
	<span id="invalid_passwd_prompt_new" class="error_prompt"></span>	

	<p>
	<label for="update_passwd_confirm">confirm password</label>
	<input type="password" id="update_passwd_confirm" size=30 />
	<span id="passwd_mismatch_prompt" class="error_prompt"></span>
	
	<p>
	Leave the above three boxes empty to keep your password unchanged.
	
	<p>
	<label><b>Information</b></label>
	
	<p>
	<label for="update_phone">phone</label>
	<input type="text" id="update_phone" name="phone" size=30 
		value="<%=(phone == null) ? "" : phone%>" />
	<span class="field_info">0 ~ 16 characters.</span>
	<span id = "too_long_phone" class="error_prompt"></span>

	<p>
	<label for="update_name">real name</label>
	<input type="text" id="update_name" name="name" size=30 
		value="<%=(name == null) ? "" : name%>" />
	<span class="field_info">no more than 100 characters.</span>
	<span id = "too_long_name" class="error_prompt"></span>

	<p>
	<label for="update_addr">address</label>
	<input type="text" id="update_addr" name="addr" size=60 
		value="<%=(addr == null) ? "" : addr%>" />
	<span class="field_info">no more than 255 characters.</span>
	<span id = "too_long_addr" class="error_prompt"></span>

	<p>
	<input type="submit" value="submit"/>
	


</form>
	
</div>

	<span id="result_prompt" class="error_prompt">
	<%= updateResult == SQL.SUCCESS ? "Update successfully" : updateResult == SQL.EINVALIDPASSWD ? "Wrong password!" : ""%>
	</span>

<p>




</body>

</html>
