<%@ page import="acmdb.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Pop authors</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}

	Order order = (Order) session.getAttribute("cart");
	String popn = request.getParameter("popn");
	int n = WebUtil.parseInt(popn, -1);
	if (popn == null || popn.equals(""))
		n = 10;
	boolean invalid = (n < 0);
	
%>

<style>
#authorqueryresults div {
	padding: 0px;
}

#authorqueryresults table, th, td{
	border: 1px solid black;
	border-collapse: collapse;
}

#authorqueryresults th, td {
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.index {
	width: 15%;	
}

th.name {
	width: 55%;	
}

th.id {
	width: 15%;
}

th.sales {
	width: 15%;
}

#authorqueryresults ul {
	list-style-type: none;
	display: block;
	padding: 0px;
	margin: 0px;
}

#authorqueryresults ul, li {
	padding-left: 15px;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

}

</style>
<body>

<h2>Pop authors</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<div id="pop">
<form id="pop_form" name="pop_form" action="popauthors.jsp" autocomplete="off" method="post">
	<p>
	<input type="text" id="pop_text" name="popn" size=50 value="<%=invalid?popn:""%>" placeholder="top n popular authors(0 for unlimited, empty for ten)"/>
	

	<input type="submit" value="submit"/>
	<span id="prompt_id" class="error_prompt"><%=invalid ? "Invalid number" : ""%></span>

</form>
</div>
		
<%
	if (n>=0) {
		%>
	<div id="authorqueryresults">
	<h3>Query results</h3>
	<%		
			SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		ArrayList<Object[]> authors = new ArrayList<Object[]>();
		int result = sql.queryMostPopularAuthors(authors, n);
		if (result != SQL.SUCCESS) { %>
			Query failed;
		<%
		}
		else if (authors == null || authors.isEmpty()) {
		%>No such authors found.<%	
	} else {

		final String[] titles = {
			"Rank",
			"Sales",
			"ID",
			"Name",
		};
		final String[] cssclass = {
			"index",
			"sales",
			"id",
			"names",
		};
		
		%>
			<table id="bqtable" >
				<tr>
				<%
					for (int cno = 0; cno < titles.length; ++cno) {
						%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
					}
				%>
				</tr>
				<%
					for (int rid = 0, tar = authors.size(); rid < tar; ++rid) {
						Object[] author = authors.get(rid);
					%>
						<tr>
							<td><%=rid+1%></td>
							<td><%=author[2]%></td>
							<td><%=author[0]%></td>
							<td><%=author[1]%></td>
						</tr>
					<%
					}
				%>
			</table>
			
			
		<%
	}	
		} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	}
	%>

	</div>

</body>

</html>
