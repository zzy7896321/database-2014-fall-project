<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - management</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}

%>

<body>

<h2>Management</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<div id="man_menu">
	<ul>
		<li><a href="newbooks.jsp">Add new books</a>
		<li><a href="modbookcopies.jsp">Add new copies of existing books</a>
		<li><a href="popbooks.jsp">Show most popular books</a>
		<li><a href="popauthors.jsp">Show most popular authors</a>
		<li><a href="poppublishers.jsp">Show most popular publishers</a>
		<li><a href="mosttrusted.jsp">Show most trusted users</a>
		<li><a href="mostuseful.jsp">Show most useful users</a>
		<li><a href="addadmin.jsp">Create new administrator account</a>
	</ul>
</div>

</body>

</html>
