<%@ page import="acmdb.*" %>


<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}
	
	String sscore = request.getParameter("score");
	int score = WebUtil.parseInt(sscore , -1);
	if (score < 0 || score > 10) {
		%><jsp:forward page="prompt.jsp"/><%
	}
	
	String desc = request.getParameter("desc");
	if (desc == null) {
		desc = "";
	}
	
	String isbn = request.getParameter("isbn");
	if (isbn == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String returnpage = "showdetails.jsp?isbn=" + isbn;

	String back = request.getParameter("back");
	String qn = request.getParameter("qn");
	String pn = request.getParameter("pn");
	
	if (back != null) {
		returnpage += "&back="  + back;
	}
	if (qn != null) {
		returnpage += "&qn=" + qn;
	}
	if (pn != null) {
		returnpage += "&pn=" + pn;
	}
	
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		/* results ignored */
		int result = sql.postComments(isbn, user.login, score, desc);
		returnpage += "&postresult=" + result;
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
	response.sendRedirect(returnpage);
%>

