<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - checkout</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

<style>
#cancelledcontent div {
	padding: 0px;
}

#cancelledtable {
	width: 50%;
	border: 1px solid black;
	border-collapse: collapse;
}

td.cancelled, th.cancelledisbn, th.cancellednumber, th.cancelledcause, th.cancelledaction {
	border: 1px solid black;
	border-collapse: collapse;
	text-align: left;
	vertical-align: top;
	word-wrap: break-word;
}

th.cancelledisbn {
	width: 20%;
}

th.cancellednumber {
	width: 10%;	
}

th.cancelledcause {
	width: 40%;
}

th.cancelledaction {
	width: 30%;
}

</style>
</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	Order order = (Order) session.getAttribute("cart");
	if (order.items.isEmpty()) {
		%>
		<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Your cart is empty. Cannot checkout."/>
			<jsp:param name="redirpage" value="cart.jsp"/>
			<jsp:param name="pagename" value="my cart"/>
		</jsp:forward>
		<%
	}

	if (order.oid != -1) {
		/* This is unexpected. */
		%>
		<jsp:forward page="prompt.jsp"/>
		<%
	
	}
	
	session.setAttribute("cart", new Order(user.login));
	
	PastOrder porder = null;
	SQL sql = WebUtil.getSQL(); 
	int orderResult = 0;
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		orderResult = sql.placeOrder(order);
		if (orderResult != SQL.SUCCESS) {
		%>
		<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Checkout failed."/>
			<jsp:param name="redirpage" value="cart.jsp"/>
			<jsp:param name="pagename" value="my cart"/>
		</jsp:forward>
		<%	
		}
		
		Pointer<PastOrder> o = new Pointer<PastOrder>();
		int result = sql.queryOrderById(order.oid, o);
		if (result != SQL.SUCCESS) {
			/* This is unexpected. */
			%>
			<jsp:forward page="prompt.jsp"/>
			<%
		}

		porder = o.value;
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
%>

<body>

<h2>Checkout</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>

<div id="ordercontent">
	<h3>Order content</h3>
	<% request.setAttribute("pastorder", porder); %>
	<jsp:include page="printpastorder.jsp"/>	
</div>
	
<% if (order.cancelled != null && !order.cancelled.isEmpty()) { 
	final String[] titles = {
		"ISBN",
		"Number",
		"Cause",
		""
	};

	final String[] cssclass = {
		"cancelledisbn",
		"cancellednumber",
		"cancelledcause",
		"cancelledaction"
	};

%>
<div id="cancelledcontent">
	<h3>Failed Items</h3>	
	<table id="cancelledtable">
		<tr>
		<%
			for (int cno = 0; cno < titles.length; ++cno) {
				%><th class="<%=cssclass[cno]%>" ><%=titles[cno]%></th><%	
			}
		%>
		</tr>
		<% {
		int id = -1;
		for (Order.Item item : order.cancelled) {
			++id;
		%>
		<tr>
			<td class="cancelled"><%=item.isbn%></td>
			<td class="cancelled"><%=item.num%></td>
			<td class="cancelled"><%=SQL.strerror(item.errno)%></td>
			<td class="cancelled">
				<a href="<%="showdetails.jsp?isbn=" + item.isbn%>" target="_blank">View book</a>
			</td>
		</tr>
		<%
		}}
		%>
	</table>
</div>
<% } /* some items are cancelled */ %>

</body>

</html>
