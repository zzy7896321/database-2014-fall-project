<%@ page import="acmdb.*" %>


<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}

	Order order = (Order) session.getAttribute("cart");
	
	String isbn = request.getParameter("isbn");
	if (isbn == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String snum = request.getParameter("num");
	int num = WebUtil.parseInt(snum, 1);
	
	String qn = request.getParameter("qn");
	String pn = request.getParameter("pn");

	String returnpage = null;
	if (qn != null) {
		returnpage = "searchbooks.jsp?qn=" + qn;
		if (pn != null) {
			returnpage += "&pn=" + pn;
		}
		returnpage += "&from=additem";
	}
	else {
		returnpage = "cart.jsp";
	}
	
	order.addItem(isbn, "", num, 0);	/* price, title should be checked on checkout */
	
	response.sendRedirect(returnpage);
%>

