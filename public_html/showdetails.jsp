<%@ page import="acmdb.*,java.util.ArrayList" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - book details</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

<style>

#bookinfotable {
	border: 0px;
	border-collapse: collapse;
}

td.caption, td,content, #bookinfotable {
	vertical-align: top;
	word-wrap: break-word;
}

td.caption {
	font-weight: bold;	
	text-align: right;
	width: 20%;
}

td.content {
	padding-left: 5%;
	text-align:left;
	width: 65%;
}

#comments {
	margin-bottom: 20px;
}

div.commentarea {
	float: left;
	display: block;
	margin: 0 auto 0px 10px;
	padding: 0 0 0 0;
	width: 100%;
}

ul.commentheader {
	text-align: left;
	display: inline-block;
	margin: 0;
	padding: 0;
}

li.commentheaderlogin {
	text-align: left;
	display: inline;
	list-style: none;
	padding: 0;
	margin-left: 0px;
	margin-right: 0;
}

li.commentheaderdate {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 20px;
	margin-right: 0;
}

li.commentheaderscore {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 100px;
	margin-right: 0;
}

li.commentheaderveryuseful {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 20px;
	margin-right: 0;
}

li.commentheaderuseful {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 20px;
	margin-right: 0;
}
 
li.commentheaderuseless {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 20px;
	margin-right: 0;
}

li.commentheadertrust {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 0px;
	margin-right: 0;
}

li.commentheaderneutral {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 20px;
	margin-right: 0;
}

li.commentheaderdistrust {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 20px;
	margin-right: 0;
}

li.commentheadertheuser {
	text-align: left;
	display: inline;
	list-style: none;
	margin-left: 10px;
	margin-right: 0;
}


div.description {
	margin-left: 0;
	padding: 0;
	text-align: left;
	float: left;
	word-wrap: break-word;
	width: 50%;
}

pre.description {
	display: inline-block;
	font-family: serif;
	margin: 0 0 20px 0;
}

div.commentheaderhr {
	width: 50%;
	margin: 0;
	padding: 0;
}

hr.commentheader {
	margin: 0;
	size: 50%;
}

#postcomment {
	margin-top: 20px;
}

#desclabel, #scorelabel {
	display: inline-block;
	width: 100px;
	text-align: right;
}

#desc, #score {
	display: inline-block;
	margin: 0;
}

#desclabel, #desc {
	vertical-align: top;
}

#postcommentsubmit {
	vertical-align: bottom;
}

p.postcommentform {
	margin: 0;
	padding: 8px 0 0 0;
}

</style>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}
	
	String isbn = request.getParameter("isbn");
	if (isbn == null) {
		%><jsp:forward page="prompt.jsp"/><%
	}

	String back = request.getParameter("back");
	String qn = request.getParameter("qn");
	String pn = request.getParameter("pn");
	String param = "isbn=" + isbn;
	String backurl = null;
	if (back != null) {
		if (qn == null) {
			back = null;
		}
		else {
			if (pn == null) {
				back = null;
			}
			else {
				param += String.format("&back=%s&qn=%s&pn=%s", back, qn, pn);
				backurl = back + "?qn=" + qn + "&pn=" + pn;
			}
		}
	}
	
	final int maxComments = 0;
	BookQuery query = WebUtil.getISBNBookQuery(isbn);
	Book book = null;
	ArrayList<Comment> comms = null;
	int[] trusts = null;
	int[] ratings = null;
	while (query != null) /* in order to use break */{
		SQL sql = WebUtil.getSQL();
		if (sql != null);
		try {
			ArrayList<Book> books = WebUtil.queryBooks(sql, user.login, query);
			if (books == null || books.isEmpty()) break;
			book = books.get(0);
			
			comms = WebUtil.queryComments(sql, isbn, maxComments, 't');
			trusts = WebUtil.queryTrusts(sql, user.login, comms);
			if (trusts == null) comms = null;
			ratings = WebUtil.queryUsefulness(sql, isbn, user.login, comms);
			if (ratings == null) {
				comms = null;
				trusts = null;
			}
		}
		finally {
			sql.close();
			sql = null;
		}

		break;
	}
%>

<body>

<h2>Book details</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
<% if (back != null) { %>
<li><a href="<%=backurl%>">Back</a>
<% } %>
</ul>
</div>
	
<div id="bookinfo">
	<h3>Book Info</h3>
	<% if (book != null) { %>
	<table id="bookinfotable">
		<tr>
			<td class="caption">Title</td>
			<td class="content"><%=book.title%></td>
		</tr>
		<tr>
			<td class="caption">ISBN</td>
			<td class="content"><%=book.isbn%></td>
		</tr>
		<tr>
			<td class="caption">Publisher</td>
			<td class="content"><%=book.publisher%></td>
		</tr>
		<tr>
			<td class="caption">Year of Publication</td>
			<td class="content"><%=book.year%></td>
		</tr>
		<tr>
			<td class="caption">#copies left</td>
			<td class="content"><%=book.n_copies%></td>
		</tr>
		<tr>
			<td class="caption">Format</td>
			<td class="content"><%=(book.format == 1) ? "hardcover" : "softcover"%></td>
		</tr>
		<tr>
			<td class="caption">Keywords</td>
			<td class="content"><%=book.keywords%></td>
		</tr>
		<tr>
			<td class="caption">Subject</td>
			<td class="content"><%=book.subject%></td>
		</tr>

		<% 
		{ int i = 0;
		for (Author author : book.authors) { %>
		<tr>
			<td class="caption"><%=(i++ == 0) ? "Authors" : ""%></td>
			<td class="content"><%=author.name%></td>
		</tr>

		<% }} %>
		<tr>
			<td class="caption">Avg. Score</td>
			<td class="content"><%=book.additionalFields.get("avg_score")%></td>
		</tr>
		<tr>
			<td class="caption">Avg. Score from Trusted Users</td>
			<td class="content"><%=book.additionalFields.get("avg_trusted_score")%></td>
		</tr>
	</table>
	<% } 
	else {
	%><p>Not found.<%	
	} %>
</div>


<div id="comments">
	<h3>Comments</h3>
	<% if (comms != null) { %>
		<% for (int i = 0; i < comms.size(); ++i) {
			Comment comm = comms.get(i);
		%>
		<div class="commentarea">
			<ul class="commentheader">
				<li class="commentheaderlogin"><%=comm.login%>
				<li class="commentheaderdate"><%=comm.date%>
				<li class="commentheaderscore"><%="score " + comm.score%>
			<% if (!comm.login.equals(user.login)) { %>
				<li class="commentheaderveryuseful"><%=
					(ratings[i] == 2) ? "very useful" : 
					String.format("<a href=\"ratecomment.jsp?rate=2&clogin=%s&%s\">very useful</a>",
						comm.login, param)%>
				<li class="commentheaderuseful"><%=
					(ratings[i] == 1) ? "useful" : 
					String.format("<a href=\"ratecomment.jsp?rate=1&clogin=%s&%s\">useful</a>",
						comm.login, param)%>
				<li class="commentheaderuseless"><%=
					(ratings[i] == 0) ? "useless" : 
					String.format("<a href=\"ratecomment.jsp?rate=0&clogin=%s&%s\">useless</a>",
						comm.login, param)%>
			<% } %>
			</ul>
	
			<% if (!comm.login.equals(user.login)) { %>
			<br>
			<ul class="commentheader">
				<li class="commentheadertrust"><%=
					(trusts[i] == 1) ? "trust" : 
					String.format("<a href=\"trustuser.jsp?trust=1&clogin=%s&%s\">trust</a>",
						comm.login, param) %>
				<li class="commentheaderneutral"><%=
					(trusts[i] == 0) ? "neutral" : 
					String.format("<a href=\"trustuser.jsp?trust=0&clogin=%s&%s\">neutral</a>",
						comm.login, param) %>
				<li class="commentheaderdistrust"><%=
					(trusts[i] == -1) ? "distrust" : 
					String.format("<a href=\"trustuser.jsp?trust=-1&clogin=%s&%s\">distrust</a>",
						comm.login, param) %>
				<li class="commentheadertheuser">the user
			</ul>
			<% } %>
		
			<div class="commentheaderhr"><hr class="commentheader"></div>
			<div class="description">
				<pre class="description"><%=comm.desc%></pre>
			</div>
		</div>
		<%
		} %>
	<% } else {
	%><p>Failed to load the comments.<%	
	}%>
</div>

<script type="text/javascript">
	function updateScore(x) {
		document.getElementById("scoredisp").innerHTML = x.toString();	
	}
</script>

<div id="postcomment">
	<h3>Post Comment</h3>
	<form id="postcommentform" autocomplete="off" method="post" action="postcomment.jsp"/>
		
		<label id="scorelabel" for="score">Score</label>
		<input id="score" type="range" name="score" step="1" min="0" max="10" value="10" 
			oninput="updateScore(this.value);" onchange="updateScore(this.value);"/>
		<span id="scoredisp" style="margin-left: 10px;">10</span>

		<p class="postcommentform">
		<label id="desclabel" for="desc">Description</label>
		<textarea id="desc" name="desc" rows="10" cols="50"></textarea>

		<input type="hidden" name="isbn" value="<%=isbn%>"/>
		<% if (back != null && qn != null && pn != null) { %>
		<input type="hidden" name="back" value="<%=back%>"/>
		<input type="hidden" name="qn" value="<%=qn%>"/>
		<input type="hidden" name="pn" value="<%=pn%>"/>
		<% } %>

		<input id="postcommentsubmit" type="submit" value="Post"/>
	</form>

<% 
	String spostresult = request.getParameter("postresult");
	if (spostresult != null) {
		int postresult = WebUtil.parseInt(spostresult, SQL.EINVAL);
		%><p><span><%
		if (postresult == SQL.SUCCESS) {
			%>Post succeeded.<%
		}
		else if (postresult == SQL.EDUPCOMMENT) {
			%>Post failed: cannot post multiple comments for the same book.<%
		}
		else {
			%>Post failed: server error; retry please. <%
		}
		%></span><%
	}
%>
</div>

</body>

</html>
