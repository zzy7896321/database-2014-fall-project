<%@ page import="acmdb.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - New Books</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login first."/>
			</jsp:forward>
		<%
	}

	if (user.usertype != SQL.UTYPE_ADMIN) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="only administrators can access management pages"/>
			</jsp:forward>
		<%
	}

	Order order = (Order) session.getAttribute("cart");
	int newBookResult = -1;
	
	String isbn = request.getParameter("isbn");
	String title = request.getParameter("title");
	String publisher = request.getParameter("publisher");
	String year = request.getParameter("year");
	String num = request.getParameter("num");
	String price = request.getParameter("price");
	String format = request.getParameter("format");
	int formatIndex = (format != null && format.equals("Hardcover")) ? 1 : 0;
	String keywords = request.getParameter("keywords");
	String subject = request.getParameter("subject");
	LinkedList<Author> authorList = new LinkedList<Author>();
	for (int i = 1;;++i) {
		String aid = request.getParameter("authorID"+i);
		if (aid == null) break;
		Author a = new Author();
		a.id = Long.parseLong(aid);
		a.name = request.getParameter("authorName"+i);
		authorList.push(a);
	}
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		if (isbn != null && (request.getParameter("authorName1") == null || request.getParameter("authorID1") != null) )
			newBookResult = WebUtil.newBook(sql,isbn,title,publisher,year,formatIndex,num,price,keywords,subject, authorList);
		if (newBookResult == 0) {
			isbn = title = publisher = year = num = price = format = keywords = subject = null;
			formatIndex = 0;
		}
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
%>

<style>
#book form {
	width: 1200px;
	margin: 20px 0 0 0;
}

#book label, input {
	display: inline-block;
	margin: 0;
}

#book label {
	width: 120px;
	text-align: right;
}

#book input + label {
	text-align: left;
	margin: 0 0 0 20px;
}

#book input[type="submit"] {
	margin: 0 0 0 60px;
}

#book p {
	margin: 0 0 0 0;
	padding: 8px 0 0 0;
}

span.error_prompt {
	margin-left: 20px;
	color: red;
}

span.field_info {
	margin-left: 20px;
	

}

</style>
<body>

<h2>New Books</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="management.jsp">Management</a>
</ul>
</div>

<script type="text/javascript">
var acounter = 0;
function addauthor() {
	var list = document.getElementById("book_form");
	var p = document.createElement("p");
	var label = document.createElement("label");
	label.style.marginRight="8px";
	acounter = acounter + 1;
	label.innerHTML = "Author"+acounter;
	var input = document.createElement("input");
	
	input.type="text";
	input.size=30;
	input.name="authorName"+acounter;
	p.appendChild(label);
	p.appendChild(input);
	list.insertBefore(p, list.lastChild);
}

</script>
<div id="book">
<form id="book_form" name="book_form" action="newbooks.jsp" autocomplete="off" method="post">
	<label for="book_isbn">ISBN</label>
	<input type="text" id="book_isbn" name="isbn" autofocus size=30 value="<%=isbn == null ? "" : isbn%>" />
	<span id="invalid_isbn_prompt" class="error_prompt"><%=newBookResult == 1 ? "Invalid ISBN" : newBookResult == 7 ? "ISBN already exists" : ""%></span>

	<p>
	<label for="book_title">Title</label>
	<input type="text" id="book_title" name="title" size=30 value="<%=title == null ? "" : title%>" />
	<span id="invalid_title_prompt" class="error_prompt"><%=newBookResult == 2 ? "Invalid Title" : ""%></span>
	
	<p>
	<label for="book_publisher">Publisher</label>
	<input type="text" id="book_publisher" name="publisher" size=30 value="<%=publisher == null ? "" : publisher%>" />
	
	<p>
	<label for="book_year">Year</label>
	<input type="text" id="book_year" name="year" size=30 value="<%=year == null ? "" : year%>" />
	<span id="invalid_year_prompt" class="error_prompt"><%=newBookResult == 3 ? "Invalid year" : ""%></span>	
	
	<p>
	<label for="book_format">Format</label>
	<select id="book_format" name="format">
	<option>Softcover</option>
	<option>Hardcover</option>
	</select>
	
	<p>
	<label for="book_num">Num</label>
	<input type="text" id="book_num" name="num" size=30 value="<%=num == null ? "" : num%>" />
	<span id="invalid_num_prompt" class="error_prompt"><%=newBookResult == 4 ? "Invalid number" : ""%></span>		
	
	<p>
	<label for="book_price">Price</label>
	<input type="text" id="book_price" name="price" size=30 value="<%=price == null ? "" : price%>" />
	<span id="invalid_price_prompt" class="error_prompt"><%=newBookResult == 5 ? "Invalid price" : ""%></span>			
	
	<p>
	<label for="book_kw">Keywords</label>
	<input type="text" id="book_kw" name="keywords" size=30 value="<%=keywords == null ? "" : keywords%>" />
	
	<p>
	<label for="book_subject">Subject</label>
	<input type="text" id="book_subject" name="subject" size=30 value="<%=subject == null ? "" : subject%>" />

	<p>
	<%
	if ( newBookResult == 0 || request.getParameter("authorName1") == null) {
	%>
	<label></label>
	<input type="button" value="Add author" onclick="addauthor()">
	<% } 
	else {
	%>
	<b>Please select the authors</b>
	<% } %>
	<p>
	<input type="submit" value="submit"/>

</form>
</div>

<script type="text/javascript">
	document.getElementById("book_format").selectedIndex = <%=formatIndex%>;
</script>

<script type="text/javascript">
	var list = document.getElementById("book_form");
	<%
	if (newBookResult != 0)
	for (int count = 1;;++count) {
		String author = request.getParameter("authorName"+count);
		if (author == null) break;
	%>
	var p = document.createElement("p");
	p.style.marginTop = "auto";
	var label = document.createElement("label");
	label.style.marginRight="8px";
	label.innerHTML = "Author<%=count%>";
	var input = document.createElement("input");
	input.type="text";
	input.size=30;
	input.name="authorName<%=count%>";
	input.value="<%=author%>"
	input.readOnly = "readonly";
	p.appendChild(label);
	p.appendChild(input);
	list.insertBefore(p, list.lastChild);
	list.insertBefore(document.createElement("br"), list.lastChild);
	var div = document.createElement("div");
	div.style.marginLeft = "128px";
	<%
		sql = WebUtil.getSQL(); 
		ArrayList<Long> ids = new ArrayList<Long>();
		if (sql == null) {
			%><jsp:forward page="prompt.jsp"/><%
		} else try {
			if (author != null) {
				sql.queryAuthorID(author, ids);
			}
		String eID = request.getParameter("authorID"+count);
		if (ids != null)
		for (Long id : ids) {
	%>
			var radio = document.createElement("input");
			radio.type="radio";
			radio.name="authorID<%=count%>";
			radio.value="<%=id%>";
			<%
			if (eID != null && eID.equals(id.toString())){
			%>
			radio.checked="checked";
			<%
			}
			%>
			div.appendChild(radio);
			div.appendChild(document.createTextNode("<%=author%>"));
			var ul = document.createElement("ul");
			<%
			ArrayList<Book> books = new ArrayList<Book>();
			sql.queryBookByAuthorID(id, books, 0);
			for (Book book : books) {
			%>
				var li = document.createElement("li");
				li.appendChild(document.createTextNode("<%=book.title%>"));
				ul.appendChild(li); 
			<%
			}
			%>
			div.appendChild(ul);
		<%
		}
		%>
		<%
		if (author != null) {
		%>
			var newradio = document.createElement("input");
			newradio.type="radio";
			newradio.name="authorID<%=count%>";
			newradio.value="-1";
			<%
			if (eID == null || eID.equals("-1")){
			%>
			newradio.checked="checked";
			<%
			}
			%>
			div.appendChild(newradio);
			div.appendChild(document.createTextNode("<create new>"));
		<%
		}
		} /* endof sql try */
		finally {
			sql.close();
			sql = null;
		}
		%>
	list.insertBefore(div, list.lastChild);
	list.insertBefore(document.createElement("br"), list.lastChild);
	<%
	}
	%>
</script>

<p>

<span id="result_prompt" class="error_prompt"><%=newBookResult == 0 ? "Success!" : newBookResult == 8 ? "Unexpected error" : ""%></span>		

</body>

</html>
