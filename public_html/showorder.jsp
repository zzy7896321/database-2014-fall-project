<%@ page import="acmdb.*" %>

<!DOCTYPE html>
<html>
<head>

<title>Bookstore - Show Order</title>

<meta charset="utf-8"/>
<meta name="description" content="Database Fall 2014 Course Project at SJTU"/>
<meta name="author" content="Peng Yanqing and Zhao Zhuoyue"/>
<link rel="stylesheet" type="text/css" href="style.css"/>

</head>

<%
	User user = (User) session.getAttribute("user");
	if (user == null) {
		response.sendRedirect("login.jsp?from=" + WebUtil.basename(request.getRequestURI()));
		return ;
	}
	
	String soid = request.getParameter("oid");
	if (soid == null) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Invalid parameter. Expecting oid."/>
			</jsp:forward>
		<%
	}
	long oid = WebUtil.parseLong(soid, -1L);
	
	PastOrder order = null;
	SQL sql = WebUtil.getSQL(); 
	if (sql == null) {
		%><jsp:forward page="prompt.jsp"/><%
	} else try {
		Pointer<PastOrder> o = new Pointer<PastOrder>();
		int result = sql.queryOrderById(oid, o);
		if (result != SQL.SUCCESS) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="<%=SQL.strerror(result)%>"/>
			</jsp:forward>
		<%
		}

		order = o.value;
	} /* endof sql try */
	finally {
		sql.close();
		sql = null;
	}
	
	if (user.usertype != SQL.UTYPE_ADMIN && !order.login.equals(user.login)) {
		%>
			<jsp:forward page="prompt.jsp">
			<jsp:param name="msg" value="Please login as the user who placed the order."/>
			</jsp:forward>
		<%
	}
%>

<body>

<h2>Show Order</h2>

<div id="navbar">
<ul>
<li><a href="index.jsp">Home</a>
<li><a href="logout.jsp">Logout</a>
<li><a href="cart.jsp">My cart</a>
</ul>
</div>

<%
	request.setAttribute("pastorder", order);
%>

<div>
	<jsp:include page="printpastorder.jsp"/>
</div>

</body>

</html>
