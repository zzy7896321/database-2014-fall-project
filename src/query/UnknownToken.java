package query;

import java_cup.runtime.ComplexSymbolFactory.Location;

public class UnknownToken extends Exception {

	public UnknownToken(Location left, Location right, String str) {
		this.left = left;
		this.right = right;
		this.str = str;
	}
	
	public Location left() {
		return this.left;
	}
	
	public Location right() {
		return this.right;
	}
	
	public String token() {
		return this.str;
	}
	
	private Location left;
	private Location right;
	private String str;

	@Override
	public String getMessage() {
		return left.toString() + "-" + right.toString() + ": " + str;
	}
	
	@Override
	public String toString() {
		return getMessage();
	}
	
	private static final long serialVersionUID = 1L;

}
