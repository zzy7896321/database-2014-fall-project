package query;

import java.io.InputStreamReader;
import java.io.StringReader;

public class QueryTest {
	public static void main(String args[]) {
		
		String q = " title like ? isbn like ? __require avg feedback score, avg trusted feedback score";
		System.out.println(q);
		
		
		Lex lexer = new Lex(null);
		Parser parser = new Parser(lexer, new java_cup.runtime.ComplexSymbolFactory());
		
		for (int i = 0; i < 1; ++i) {
			lexer.reset(new StringReader(q));

			java_cup.runtime.Symbol result;
			String query = null;
			try {
				result = parser.debug_parse();
				query = (String) result.value;

				System.out.println(query);
			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
		
	}
}
