package query;

import java_cup.runtime.*;
import java_cup.runtime.ComplexSymbolFactory.Location;

%%

%class Lex
%public 

%yylexthrow UnknownToken

%unicode
%ignorecase
%line
%column

%implements java_cup.runtime.Scanner
%function next_token
%type java_cup.runtime.Symbol
%eofval{
	return newSymbol(sym.EOF);
%eofval}
%eofclose
%cupdebug

%{
	ComplexSymbolFactory sf = new ComplexSymbolFactory();
	
	Symbol newSymbol(int type) {
		return newSymbol(type, null);
	}

	Symbol newSymbol(int type, Object o) {
		Location left;
		if (prev_line < yyline) {
			left = new Location(yyline + 1, 1);
		}
		else {
			left = new Location(prev_line + 1, prev_column + 2);
		}

		Location right = new Location(yyline + 1, yycolumn + 1);

		return sf.newSymbol("String", type, left, right, o);
	}

	int prev_line = 0;
	int prev_column = 0;

	public void reset(java.io.Reader reader) {
		prev_line = prev_column = 0;
		yyreset(reader);
	}
%}

WhiteSpace = [ \t\f\n\r\v]

%%

{WhiteSpace}+ 		{ prev_line = yyline; prev_column = yycolumn; }

"AND" | "&&"		{ return newSymbol(sym.AND); }
"OR"  | "||"		{ return newSymbol(sym.OR);	}
"NOT" | "!"			{ return newSymbol(sym.NOT); }
"("					{ return newSymbol(sym.LBRACKET);	}
")"					{ return newSymbol(sym.RBRACKET);	}

"LIKE"				{ return newSymbol(sym.LIKE); }
"="					{ return newSymbol(sym.EQ);	}
"!=" | "<>"			{ return newSymbol(sym.NEQ); }

'(\\.|[^\\'])*'		{ return newSymbol(sym.STRING_LITERAL, yytext().substring(1, yylength() - 1)); }
\"(\\.|[^\\\"])*\"	{ return newSymbol(sym.STRING_LITERAL, yytext().substring(1, yylength() - 1));	}

"ORDER BY"			{ return newSymbol(sym.ORDERBY);	}
"ASC"				{ return newSymbol(sym.ASC);	}
"DESC"				{ return newSymbol(sym.DESC);	}
","					{ return newSymbol(sym.COMMA);	}

"PUBLISHER"		{ return newSymbol(sym.PUBLISHER); }
"AUTHOR"		{ return newSymbol(sym.AUTHOR); }
"TITLE"			{ return newSymbol(sym.TITLE);	}
"SUBJECT"		{ return newSymbol(sym.SUBJECT); }
"KEYWORDS"		{ return newSymbol(sym.KEYWORDS); }
"ISBN"			{ return newSymbol(sym.ISBN);	}

"YEAR"				{ return newSymbol(sym.YEAR); }
"AVG FEEDBACK SCORE"	{ return newSymbol(sym.FEEDBACK_SCORE);	}
"AVG TRUSTED FEEDBACK SCORE"	{ return newSymbol(sym.TRUSTED_FEEDBACK_SCORE); }

"?"			{ return newSymbol(sym.PLACEHOLDER); }
"__REQUIRE"	{ return newSymbol(sym.REQUIRE); }

/*(\\.|[^\\\"' \t\f\n\r\v])+		{ return newSymbol(sym.UNQUOTED_STRING_LITERAL, yytext()); } */

[^]					{ throw new UnknownToken(new Location(yyline + 1, yycolumn + 1)
							, new Location(yyline + 1, yycolumn + 1), yytext()); }
