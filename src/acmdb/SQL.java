package acmdb;

import java.io.IOException;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.sql.Timestamp;

public class SQL {
	private Connector con;
	
	public SQL(Connector connector) {
		con = connector;
	}

	public void close() {
		con.closeConnection();
	}
	
	private void commit() {
		try {
			con.getConnection().commit();
		} catch (SQLException e1) {
			Connector.showException(e1);
			con.closeConnection();
			System.err.println("exception raised when commiting. exit.");
			System.exit(1);
		}
	}

	private void rollback() {
		try {
			con.getConnection().rollback();
		} catch (SQLException e1) {
			Connector.showException(e1);
			con.closeConnection();
			System.err.println("exception raised when rolling back. exit.");
			System.exit(1);
		}
	}
	

	public int userReg(String login, String passwd, String phone, String name, String address) {
		return __userReg(login, passwd, phone, name, address, UTYPE_USER);
	}
	
	public int adminReg(String login, String passwd, String phone, String name, String address) {
		return __userReg(login, passwd, phone, name, address, UTYPE_ADMIN);
	}
	
	public int userReg(User user, String passwd) {
		return __userReg(user.login, passwd, user.phone, user.name, user.address, user.usertype);
	}
	
	private int __userReg(String login, String passwd, String phone, String name, String address, byte utype) {
		if (login == null || login.length() == 0 || login.length() > MAX_LOGIN_LENGTH) {
			return EINVALIDLOGIN;
		}
		if (passwd == null || passwd.length() == 0) {
			return EINVALIDPASSWD;
		}
		if (utype < 0 || utype >= UTYPE_UPPER) {
			return EINVAL;
		}
		
		String enc_passwd = DBUtil.getMD5(passwd);
		
		return executePreparedStatement( (PreparedStatement... stmts) -> {
			PreparedStatement register = stmts[0];
			
			register.setString(1, login);
			register.setString(2, enc_passwd);
			register.setString(3, phone);
			register.setString(4, name);
			register.setString(5, address);
			register.setByte(6, utype);
		
			try {
				if (register.executeUpdate() == 0) {
					return EUNEXPECTED;
				}
			} catch (SQLException e) {
				switch (e.getErrorCode()) {
				case 1062:
					return EDUPLOGIN;
				}
			}
			
			return SUCCESS;
		}
		,  "insert into acmdb02.Users (Login, Passwd, Phone, Name, Address, UserType) values (?, ?, ?, ?, ?, ?);");
	}
	
	public int userLogin(String login, String passwd, User user) {
		if (login == null || login.length() == 0 || login.length() > MAX_LOGIN_LENGTH) {
			return EINVALIDLOGIN;
		}
		if (passwd == null || passwd.length() == 0) {
			return EINVALIDPASSWD;
		}
		
		String enc_passwd = DBUtil.getMD5(passwd);
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement q = stmts[0];
			
			q.setString(1, login);
			ResultSet qres = q.executeQuery();
			
			if (!qres.next()) {
				return EINVALIDLOGIN;
			}
			String q_passwd_str = qres.getString(2);
			if (!q_passwd_str.equals(enc_passwd)) {
				return EINVALIDPASSWD;
			}
			
			if (user != null) {
				user.login = qres.getString(1);
				user.phone = qres.getString(3);
				user.name = qres.getString(4);
				user.address = qres.getString(5);
				user.usertype = qres.getByte(6);
			}
			
			return SUCCESS;
		}
		, "select * from acmdb02.Users U where U.Login = ?");
	}
	
	public int userChangePasswd(String login, String opasswd, String npasswd) {
		if (login == null || opasswd == null || npasswd == null) {
			return EINVAL;
		}
		
		String enc_opasswd = DBUtil.getMD5(opasswd);
		String enc_npasswd = DBUtil.getMD5(npasswd);
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement update_passwd = stmts[0];
			
			update_passwd.setString(1, enc_npasswd);
			update_passwd.setString(2, login);
			update_passwd.setString(3, enc_opasswd);
			
			if (update_passwd.executeUpdate() == 0) {
				return EINVALIDPASSWD;
			}
			
			return SUCCESS;
		}
		, "update acmdb02.Users set Passwd = ? where Login = ? and Passwd = ?;");
	}

	public int userUpdateInfo(String login, int field, String new_info) {
		if (field < UF_PHONE || field > UF_ADDR || login == null || new_info == null) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement update_info = stmts[0];
			
			update_info.setString(1, new_info);
			update_info.setString(2, login);
			
			if (update_info.executeUpdate() == 0) {
				return EINVALIDLOGIN;
			}
			
			return SUCCESS;
		}, __update_user_info_sql[field]);
	}
	
	private static final String[] __update_user_info_sql = {
		"",	/* nothing */
		"",	/* cannot change login */
		"", /* don't use userChangeInfo to change the password */
		"update acmdb02.Users set Phone = ? where Login = ?;",
		"update acmdb02.Users set Name = ? where Login = ?;",
		"update acmdb02.Users set Address = ? where Login = ?;",
		"", /* don't change user type */
	};
	public static final int UF_PHONE = 3;
	public static final int UF_NAME = 4;
	public static final int UF_ADDR = 5;
	
	public int userUpdateInfo(User user) {
		if (user == null || user.login == null) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement update_info = stmts[0];
			
			update_info.setString(1, user.phone);
			update_info.setString(2, user.name);
			update_info.setString(3, user.address);
			
			if (update_info.executeUpdate() == 0) {
				return EINVALIDLOGIN;
			}
			
			return SUCCESS;
		},
		"update acmdb02.Users set Phone = ?, Name = ?, Address = ? where Login = ?;");
	}

	public int userUpdateAllInfo(String login, String oldPasswd, String newPasswd,
			String phone, String name, String address) {
		
		if (login == null || phone == null || name == null || address == null) {
			return EINVAL;
		}

		if ((oldPasswd == null) ^ (newPasswd == null)) {
			return EINVAL;
		}
		
		String sql_stmt = 
		(oldPasswd == null) ? 
			"update acmdb02.Users set Phone = ?, Name = ?, Address = ? where Login = ?;" :
			"update acmdb02.Users set Phone = ?, Name = ?, Address = ?, Passwd = ? where "
			+ " Login = ? and Passwd = ?";

		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement update_info = stmts[0];

			update_info.setString(1, phone);
			update_info.setString(2, name);
			update_info.setString(3, address);
			if (oldPasswd != null) {
				update_info.setString(4, DBUtil.getMD5(newPasswd));
				update_info.setString(5, login);
				update_info.setString(6, DBUtil.getMD5(oldPasswd));
			}
			else {
				update_info.setString(4, login);
			}
			
			if (update_info.executeUpdate() == 0) {
				return (oldPasswd == null) ? EINVALIDLOGIN : EINVALIDPASSWD;	
			}

			return SUCCESS;
		}, sql_stmt);
	}
	
	public int addBook(Book book) {
		if (book == null || book.isbn == null || book.isbn.length() != 13) {
			return EINVAL;
		}
		if (book.title == null) {
			return EINVAL;
		}
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement insertAuthor = stmts[0];
			PreparedStatement insertBook = stmts[1];
			PreparedStatement insertWrittenby = stmts[2];
			
			for (Author author : book.authors) {
				long author_id = author.id;
				String name = author.name;
				
				if (author_id < 0) {
					insertAuthor.setString(1, name);
					if (insertAuthor.executeUpdate() == 0) {
						return EUNEXPECTED;
					}
					ResultSet result = insertAuthor.getGeneratedKeys();
					if (!result.next()) {
						return EUNEXPECTED;
					}
					author_id = result.getLong(1);
					author.id = author_id;
				}
			}
			
			insertBook.setString(1, book.isbn);
			insertBook.setString(2, book.title);
			insertBook.setString(3, book.publisher);
			insertBook.setInt(4, book.year);
			insertBook.setInt(5, book.n_copies);
			insertBook.setInt(6, book.price);
			insertBook.setByte(7, book.format);
			insertBook.setString(8, book.keywords);
			insertBook.setString(9, book.subject);
		
			try {
				if (insertBook.executeUpdate() == 0) {
					return EUNEXPECTED;
				}
			}
			catch (SQLException e) {
				if (e.getErrorCode() == 1062) {
					/* duplicate key */
					return EBOOKDUP;
				}
				
				throw e;
			}
			
			insertWrittenby.setString(1, book.isbn);
			for (Author author : book.authors) {
				insertWrittenby.setLong(2, author.id);
				if (insertWrittenby.executeUpdate() == 0) {
					return EUNEXPECTED;
				}
			}
			
			return SUCCESS;
		}
		, "insert into acmdb02.Authors (Author_name) values (?);"
		, "insert into acmdb02.Books (ISBN, Title, Publisher, Year_of_publication, Num_copies, Price, Format, Keywords, Subject)"
				+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?);"
		, "insert into acmdb02.Written_by (ISBN, Author_id) values (?, ?);");
	}
	
	public int queryAuthorID(String author, ArrayList<Long> ids) {
		if (author == null || ids == null) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement queryAuthor = stmts[0];
			
			queryAuthor.setString(1, author);
			ResultSet result = queryAuthor.executeQuery();
			
			while (result.next()) {
				ids.add(result.getLong(1));
			}
			
			return SUCCESS;
		}
		, "select A.Author_id from acmdb02.Authors A where A.Author_name = ?;");
	}
	
	/**
	 * n == 0 if an unlimited number of books is to be returned.
	 * 
	 * @param id
	 * @param books
	 * @param n
	 * @return
	 */
	public int queryBookByAuthorID(long id, ArrayList<Book> books, int n) {
		if (books == null || n < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement queryBooks = stmts[0];
			PreparedStatement queryAuthors = stmts[1];
			
			int _n = n;
			queryBooks.setLong(1, id);
			ResultSet result = queryBooks.executeQuery();
			
			while ((n == 0 || (_n-- > 0)) && result.next()) {
				Book book = new Book();
				book.isbn = result.getString(1);
				book.title = result.getString(2);
				book.publisher = result.getString(3);
				book.year = result.getInt(4);
				book.n_copies = result.getInt(5);
				book.price = result.getInt(6);
				book.format = result.getByte(7);
				book.keywords = result.getString(8);
				book.subject = result.getString(9);
				
				queryAuthors.setString(1, book.isbn);
				ResultSet aresult = queryAuthors.executeQuery();
				while (aresult.next()) {
					Author a = new Author();
					a.id = aresult.getLong(1);
					a.name = aresult.getString(2);
					book.authors.add(a);
				}
				
				books.add(book);
			}
			
			return SUCCESS;
		}
		, "select DISTINCT B.* from acmdb02.Books B natural join acmdb02.Written_by W where W.Author_id = ?;"
		, "select A.Author_id, A.Author_name from acmdb02.Authors A natural join acmdb02.Written_by W where"
			+ " W.ISBN = ?;");
	}
	
	public int searchForBooks(String condition, String login, ArrayList<Book> books, int n) {
		if (books == null || n < 0 || login == null) {
			return EINVAL;
		}

		Pointer<String> query = new Pointer<String>();
		Pointer<Boolean> need_fill_login = new Pointer<Boolean>();
		{
			int result = compileBookQuery(condition, query, need_fill_login);
			if (result != SUCCESS) {
				return result;
			}
			query.value = "select B.* " + query.value;
		}
		
		return executePreparedStatement( (PreparedStatement... stmts) -> {
			PreparedStatement queryBook = stmts[0];
			PreparedStatement queryAuthor = stmts[1];
			
			if (need_fill_login.value) {
				queryBook.setString(1, login);
			}
			ResultSet result = queryBook.executeQuery();
			
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				Book book = new Book();
				book.isbn = result.getString(1);
				book.title = result.getString(2);
				book.publisher = result.getString(3);
				book.year = result.getInt(4);
				book.n_copies = result.getInt(5);
				book.price = result.getInt(6);
				book.format = result.getByte(7);
				book.keywords = result.getString(8);
				book.subject = result.getString(9);
				
				queryAuthor.setString(1, book.isbn);
				ResultSet aresult = queryAuthor.executeQuery();
				while (aresult.next()) {
					Author a = new Author();
					a.id = aresult.getLong(1);
					a.name = aresult.getString(2);
					book.authors.add(a);
				}
				
				books.add(book);
			}
			
			return SUCCESS;
		}
		, query.value
		, "select A.Author_id, A.Author_name from Written_by W natural join Authors A where W.ISBN = ?;");
	}
	
	public int searchForBooksWithBookQuery(BookQuery query, String login, int startNo, int n, 
			ArrayList<Book> books, Pointer<Integer> nrowsPtr) {
		if (query == null || books == null || startNo < 0 || n < 0 || login == null) {
			return EINVAL;
		}

		int order = query.orderby;
		if (order < 0 || order >= sortingOrders.length) {
			return EINVAL;
		}
			
		StringBuilder condition_builder = new StringBuilder();
		boolean first = true;
		if (query.title != null) {
			if (first) {
				first = false;
			} else {
				condition_builder.append(" and");
			}
			condition_builder.append(" title like ?");
		}
		if (query.isbn != null) {
			if (first) {
				first = false;
			} else {
				condition_builder.append(" and");
			}
			condition_builder.append(" isbn like ?");
		}
		if (query.author != null) {
			if (first) {
				first = false;
			} else {
				condition_builder.append(" and");
			}
			condition_builder.append(" author like ?");
		}
		if (query.publisher != null) {
			if (first) {
				first = false;
			} else {
				condition_builder.append(" and");
			}
			condition_builder.append(" publisher like ?");
		}
		if (query.subject != null) {
			if (first) {
				first = false;
			} else {
				condition_builder.append(" and");
			}
			condition_builder.append(" subject like ?");
		}
		if (query.keywords != null) {
			if (first) {
				first = false;
			} else {
				condition_builder.append(" and");
			}
			condition_builder.append(" keywords like ?");
		}
		condition_builder.append(sortingOrders[order]);
		condition_builder.append(" __require avg feedback score, avg trusted feedback score");
		String condition = condition_builder.toString();

		Pointer<String> queryPtr = new Pointer<String>();
		Pointer<Boolean> need_fill_login = new Pointer<Boolean>();
		{
			int result = compileBookQuery(condition, queryPtr, need_fill_login);
			if (result != SUCCESS) {
				return result;
			}
			queryPtr.value = "select B.*, S.avg_score, TS.avg_score_from_trusted_user " + queryPtr.value;
		}

		return executePreparedStatement( (PreparedStatement... stmts) -> {
			PreparedStatement queryBook = stmts[0];
			PreparedStatement queryAuthor = stmts[1];
			
			int now_field = 0;
			if (need_fill_login.value) {
				queryBook.setString(++now_field, login);
			}

			if (query.title != null) {
				queryBook.setString(++now_field, "%" + query.title + "%");
			}
			if (query.isbn != null) {
				queryBook.setString(++now_field, "%" + query.isbn + "%");
			}
			if (query.author != null) {
				queryBook.setString(++now_field, "%" + query.author + "%");
			}
			if (query.publisher != null) {
				queryBook.setString(++now_field, "%" + query.publisher + "%");
			}
			if (query.subject != null) {
				queryBook.setString(++now_field, "%" + query.subject + "%");
			}
			if (query.keywords != null) {
				queryBook.setString(++now_field, "%" + query.keywords + "%");
			}

			ResultSet result = queryBook.executeQuery();
			
			int nrows = 0;
			int _startNo = startNo;
			while (_startNo-- > 0 && result.next()) ++nrows;
	
			int _n = n;
			while (result.next()) {
				++nrows;

				if (n == 0 || _n-- > 0) {
					Book book = new Book();
					book.isbn = result.getString(1);
					book.title = result.getString(2);
					book.publisher = result.getString(3);
					book.year = result.getInt(4);
					book.n_copies = result.getInt(5);
					book.price = result.getInt(6);
					book.format = result.getByte(7);
					book.keywords = result.getString(8);
					book.subject = result.getString(9);
	
					book.additionalFields.put("avg_score", result.getDouble(10));
					book.additionalFields.put("avg_trusted_score", result.getDouble(11));
					
					queryAuthor.setString(1, book.isbn);
					ResultSet aresult = queryAuthor.executeQuery();
					while (aresult.next()) {
						Author a = new Author();
						a.id = aresult.getLong(1);
						a.name = aresult.getString(2);
						book.authors.add(a);
					}
					books.add(book);
				}
			}

			if (nrowsPtr != null) {
				nrowsPtr.value = nrows;
			}
			
			return SUCCESS;
		}
		, queryPtr.value
		, "select A.Author_id, A.Author_name from Written_by W natural join Authors A where W.ISBN = ?;");
	}

	public static final String sortingOrders[] = {
		"", 
		" order by year",
		" order by year asc",
		" order by avg feedback score",
		" order by avg feedback score asc",
		" order by avg trusted feedback score",
		" order by avg trusted feedback score asc",
	};
	
	public int compileBookQuery(String condition, Pointer<String> query, Pointer<Boolean> need_fill_login) {
		lex.reset(new StringReader(condition));

		String result;
		try {
			result = (String) parser.parse().value;
		}
		catch (Exception e) {
			return EPARSE;
		}
		
		query.value = result;
		need_fill_login.value = parser.need_fill_login();
		
		return SUCCESS;
	}
	
	private query.Lex lex = new query.Lex(null);
	private query.Parser parser = new query.Parser(lex, new java_cup.runtime.ComplexSymbolFactory());
	
	public int placeOrder(Order order) {
		if (order == null) {
			return EINVAL;
		}
		
		String login = order.login;
		if (login == null || login.length() == 0 || login.length() > MAX_LOGIN_LENGTH) {
			return EINVALIDLOGIN;
		}
		if (order.items.isEmpty()) {
			return EEMPTYORDER;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement createOrder = stmts[0];
			PreparedStatement findBook = stmts[1];
			PreparedStatement updateBook = stmts[2];
			PreparedStatement addItem = stmts[3];
			
			createOrder.setString(1, login);
			try{
				if (createOrder.executeUpdate() == 0){
					return EUNEXPECTED;
				}
			}
			catch (SQLException e) {
				if (e.getErrorCode() == 1452) {
					/* foreign key constraint fails */
					return EINVALIDLOGIN;
				}
				
				throw e;
			}
			ResultSet result = createOrder.getGeneratedKeys();
			if (!result.next()) {
				return EUNEXPECTED;
			}
			long oid = result.getLong(1);
			
			order.oid = oid;
			
			for (Order.Item item : order.items.values().toArray(new Order.Item[0])) {
				String isbn = item.isbn;
				int num = item.num;
				
				if (num < 0) {
					order.cancel(isbn, EINVAL);
					continue;
				}
				
				findBook.setString(1, isbn);
				result = findBook.executeQuery();
				if (!result.next()) {
					order.cancel(isbn, EBOOKNOTFOUND);
					continue;
				}
				int price = result.getInt(1);
				item.price = price;
				
				updateBook.setInt(1, num);
				updateBook.setString(2, isbn);
				/* if this test fails, affected row count is 0, indicating there are
				 * insufficient books */
				updateBook.setInt(3, num);	
				if (updateBook.executeUpdate() == 0) {
					order.cancel(isbn, EBOOKINSUFF);
					continue;
				}
				
				addItem.setLong(1, oid);
				addItem.setString(2, isbn);
				addItem.setInt(3, num);
				if (addItem.executeUpdate() == 0) {
					order.cancel(isbn, EUNEXPECTED);
					continue;
				}
			}
			
			if (order.items.isEmpty()) {
				return EEMPTYORDER;
			}
			
			return SUCCESS;
		}
		, "insert into acmdb02.Orders (Login) values (?);"
		, "select B.Price from acmdb02.Books B where ISBN = ?"
		, "update acmdb02.Books set Num_copies = Num_copies - ? where ISBN = ? and Num_copies >= ?;"
		, "insert into acmdb02.Includes (Oid, ISBN, Quantity) values (?, ?, ?);");
	}

	public int queryOrderIds(String login, int n, ArrayList<Long> oids) {
		if (login == null || n < 0 || oids == null) {
			return EINVAL;
		}

		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement q = stmts[0];

			q.setString(1, login);
			
			ResultSet result = q.executeQuery();

			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				oids.add(result.getLong(1));
			}

			return SUCCESS;
		}, 
		"select O.Oid from acmdb02.Orders O where O.Login = ? order by O.Time;");
	}

	public int queryOrderById(long oid, Pointer<PastOrder> o) {
		if (o == null) {
			return EINVAL;
		}

		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement qorder = stmts[0];
			PreparedStatement qitems = stmts[1];
	
			qorder.setLong(1, oid);
			ResultSet result = qorder.executeQuery();
			
			if (!result.next()) {
				return EORDERNOTFOUND;
			}

			Timestamp time = result.getTimestamp(1);
			String login = result.getString(2);

			PastOrder order = new PastOrder(oid, time, login);
			
			qitems.setLong(1, oid);
			result = qitems.executeQuery();
			
			BookQuery query = new BookQuery(null, null, null, null, null, null,  "0");
			while (result.next()) {
				String isbn = result.getString(1);
				int num = result.getInt(2);
				
				order.nums.add(num);
				query.isbn = isbn;
				int qresult = searchForBooksWithBookQuery(query, login, 0, 0, order.books, null);
				if (qresult != SUCCESS) {
					return qresult;
				}
			}

			o.value = order;

			return SUCCESS;
		}
		, "select O.Time, O.Login from acmdb02.Orders O where O.Oid = ?;"
		, "select I.ISBN, I.Quantity from acmdb02.Includes I where I.Oid = ?;");
	}
	
	public int updateNumCopies(String isbn, int new_num_copies) {
		if (isbn == null || new_num_copies < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement update_ncopies = stmts[0];
			
			update_ncopies.setInt(1, new_num_copies);
			update_ncopies.setString(2, isbn);
			if (update_ncopies.executeUpdate() == 0) {
				return EBOOKNOTFOUND;
			}
			
			return SUCCESS;
		}
		, "update acmdb02.Books set Num_copies = ? where ISBN = ?;");
	}

	public int addNumCopies(String isbn, int delta) {
		if (isbn == null) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement update_ncopies = stmts[0];
			
			update_ncopies.setInt(1, -delta);
			update_ncopies.setInt(2, delta);
			update_ncopies.setString(3, isbn);
			if (update_ncopies.executeUpdate() == 0) {
				return EBOOKNOTFOUND;
			}
			
			return SUCCESS;
		}
		, "update acmdb02.Books set Num_copies = if (Num_copies < ?, 0, Num_copies + ?)  where ISBN = ?;");

	}
	
	public int postComments(String isbn, String login, int score, String desc) {
		if (isbn == null || score < 0 || score > 10 || desc == null) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement insert_comment = stmts[0];
			
			insert_comment.setString(1, isbn);
			insert_comment.setString(2, login);
			insert_comment.setInt(3, score);
			insert_comment.setString(4, desc);
			
			try {
				if (insert_comment.executeUpdate() == 0) {
					return EUNEXPECTED;
				}
			} catch (SQLException e) {
				switch (e.getErrorCode()) {
				case 1062:
					return EDUPCOMMENT;
				}
				
				throw e;
			}
			
			return SUCCESS;
		}
		, "insert into acmdb02.Comments (ISBN, Login, Score, Description) values (?, ?, ?, ?);");
	}
	
	public int queryComments(String isbn, ArrayList<Comment> comms, int n, char orderby) {
		if (isbn == null || comms == null || n < 0) {
			return EINVAL;
		}
		
		if (orderby != 'n' && orderby != 't' && orderby != 'u') {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) ->  {
			PreparedStatement query_comment = stmts[0];
			
			query_comment.setString(1, isbn);
			ResultSet result = query_comment.executeQuery();
			
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				Comment com = new Comment();
				com.login = result.getString(1);
				com.score = result.getInt(2);
				com.date = result.getTimestamp(3);
				com.desc = result.getString(4);
				
				comms.add(com);
			}
			
			return SUCCESS;
		}, 
		(orderby == 'n') ? "select C.Login, C.Score, C.Date, C.Description from acmdb02.Comments C where C.ISBN = ?;"
		: (orderby == 't') ? "select C.Login, C.Score, C.Date, C.Description from acmdb02.Comments C where C.ISBN = ? order by C.Date DESC;"
		: /* orderby == 'u' */ "select C.Login, C.Score, C.Date, C.Description from acmdb02.Comments C where C.ISBN = ? order by C.Score DESC;");
	}
	
	public int queryRating(String isbn, ArrayList<Comment> comms, String login, int[] rating) {
		if (isbn == null || comms == null || login == null || rating == null) {
			return EINVAL;
		}

		if (comms.size() != rating.length) {
			return EINVAL;
		}

		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement q = stmts[0];

			for (int i = 0; i < rating.length; ++i) {
				q.setString(1, isbn);
				q.setString(2, comms.get(i).login);
				q.setString(3, login);

				ResultSet result = q.executeQuery();
				if (result.next()) {
					rating[i] = result.getInt(1);
				}
				else {
					rating[i] = -1;
				}
			}
			return SUCCESS;
		}, 
		"select R.Usefulness from Rates R where R.ISBN = ? and R.Commenter_Login = ? and R.Rater_Login = ?;");

	}

	public int postRating(String isbn, String rated_login, String rating_login, int rating) {
		if (isbn == null) {
			return EINVAL;
		}
		
		if (rating_login.equals(rated_login)) {
			return ESELFRATE;
		}
		
		int result = executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement ins = stmts[0];
			
			ins.setString(1, isbn);
			ins.setString(2, rated_login);
			ins.setString(3, rating_login);
			ins.setInt(4, rating);
		
			try {
				if (ins.executeUpdate() == 0) {
					return EUNEXPECTED;
				}
			}
			catch (SQLException e) {
				switch (e.getErrorCode()) {
				case 1062:
					return EDUPRATING;
				}
				
				throw e;
			}
			
			return SUCCESS;
		}
		, "insert into acmdb02.Rates (ISBN, Commenter_Login, Rater_Login, Usefulness) values (?, ?, ?, ?);");
		if (result == EDUPRATING) {
			result = executePreparedStatement((PreparedStatement... stmts) -> {
				PreparedStatement ins = stmts[0];
				
				ins.setInt(1, rating);
				ins.setString(2, isbn);
				ins.setString(3, rated_login);
				ins.setString(4, rating_login);
			
				if (ins.executeUpdate() == 0) {
					return EUNEXPECTED;
				}
				
				return SUCCESS;
			}
			, "update acmdb02.Rates set Usefulness = ? where ISBN = ? and Commenter_Login = ? and Rater_Login = ?;");
		}

		return result;
	}

	public int queryTrustState(String trusting_login, String[] trusted_login, int[] trust) {
		if (trusting_login == null || trusted_login == null || trust == null ||
			trusted_login.length != trust.length) {
			return EINVAL;
		}

		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement q = stmts[0];

			for (int i = 0; i < trusted_login.length; ++i) {
				q.setString(1, trusting_login);
				q.setString(2, trusted_login[i]);

				ResultSet result = q.executeQuery();
				if (result.next()) {
					trust[i] = result.getInt(1);
				}
				else {
					trust[i] = 0;
				}
			}
			return SUCCESS;
		},
		"select T.Trust from acmdb02.Trusts T where T.Commenter_Login = ? and T.Commented_Login = ?;");
	}
	
	public int updateTrustState(String trusting_login, String trusted_login, int trust) {
		if (trusted_login == null || trusting_login == null) {
			return EINVAL;
		}
		
		if (trust == 0) {
			return executePreparedStatement((PreparedStatement... stmts) -> {
				PreparedStatement rt = stmts[0];
				
				rt.setString(1, trusting_login);
				rt.setString(2, trusted_login);
				
				rt.executeUpdate();
				
				return SUCCESS;
			}
			, "delete from acmdb02.Trusts where Commenter_Login = ? and Commented_Login = ?;");
		}
		
		else {
			int r = executePreparedStatement((PreparedStatement... stmts) -> {
				PreparedStatement it = stmts[0];
				
				it.setString(1, trusting_login);
				it.setString(2, trusted_login);
				it.setInt(3, trust);
				
				try {
					if (it.executeUpdate() == 0) {
						return EUNEXPECTED;
					}
				} catch (SQLException e) {
					switch (e.getErrorCode()) {
					case 1062:
						return EDUPTRUST;
					}
					throw e;
				}
				
				return SUCCESS;
			}
			, "insert into acmdb02.Trusts (Commenter_Login, Commented_Login, Trust) values (?, ?, ?);");
			
			if (r == EDUPTRUST) {
				return executePreparedStatement((PreparedStatement... stmts) -> {
					PreparedStatement ut = stmts[0];
					
					ut.setInt(1, trust);
					ut.setString(2, trusting_login);
					ut.setString(3, trusted_login);
					
					if (ut.executeUpdate() == 0) {
						return EUNEXPECTED;
					}
					
					return SUCCESS;
				}
				, "update acmdb02.Trusts set Trust = ? where Commenter_Login = ? and Commented_Login = ?;");
			}
			
			else {
				return r;
			}
		}
	}
	
	public int recommendBooks(String login, ArrayList<Book> books, int n) {
		if (login == null || n < 0 || books == null) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query = stmts[0];
			
			query.setString(1, login);
			query.setString(2, login);
			query.setString(3, login);
			ResultSet result = query.executeQuery();
			
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				Book book = new Book();
				book.isbn = result.getString(1);
				book.title = result.getString(2);
				book.publisher = result.getString(3);
				book.year = result.getInt(4);
				book.n_copies = result.getInt(5);
				book.price = result.getInt(6);
				book.format = result.getByte(7);
				book.keywords = result.getString(8);
				book.subject = result.getString(9);

				book.additionalFields.put("sales", result.getInt(10));

				books.add(book);
			}
			
			return SUCCESS;
		}, 
		  "select B.*, sales_table.sales "
		+ "from Books B natural join "
		+ "(select I.ISBN, sum(I.Quantity) as sales "
		+ "		from Orders O natural join Includes I "
		+ "		where O.Login <> ? and "
		+ "		O.Login in ("
		+ "			select O1.Login from "
		+ "				(Orders O1 natural join Includes I1) inner join "
		+ "				(Orders O2 natural join Includes I2) on I1.ISBN = I2.ISBN "
		+ "			where O2.Login = ?) "
		+ "		group by I.ISBN "
		+ "		having 0 < sum(I.Quantity) "
		+ "	) as sales_table "
		+ "where B.ISBN not in (select I3.ISBN from Orders O3 natural join Includes I3 where O3.Login = ?) "
		+ "order by sales_table.sales desc;"	
		);
	}
	
	public int queryAuthorDistance(long id1, long id2, Pointer<Integer> d) {
		if (id1 < 0 || id2 < 0 || d == null) {
			return EINVAL;
		}
		
		if (id1 == id2) return 0;

		d.value = null;
		HashMap<Long, Integer> dis = new HashMap<Long, Integer>();
		LinkedList<Long> q = new LinkedList<Long>();
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query_edge = stmts[0];
			
			dis.put(id1, 0);
			q.add(id1);
			
			while (!q.isEmpty()) {
				long now = q.pollFirst();
				int dnow = dis.get(now);
				
				query_edge.setLong(1, now);
				ResultSet result = query_edge.executeQuery();
				while (result.next()) {
					long to = result.getLong(1);
					
					if (!dis.containsKey(to)) {
						if (to == id2) {
							d.value = dnow + 1;
							return SUCCESS;
						}
						
						dis.put(to, dnow + 1);
						q.add(to);
					}
				}
			}
			
			return SUCCESS;
		}
		, "select W2.Author_id from Written_by W1 inner join Written_by W2 on W1.ISBN = W2.ISBN where W1.Author_id = ?;");
	}

	/**
	 * sales as Integer type a_fields in book.
	 * 
	 * @param books
	 * @param n
	 * @return
	 */
	public int queryMostPopularBooks(ArrayList<Book> books, int n) {
		if (books == null || n < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query = stmts[0];
		
			ResultSet result = query.executeQuery();
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				Book book = new Book();
				book.isbn = result.getString(1);
				book.title = result.getString(2);
				book.publisher = result.getString(3);
				book.year = result.getInt(4);
				book.n_copies = result.getInt(5);
				book.price = result.getInt(6);
				book.format = result.getByte(7);
				book.keywords = result.getString(8);
				book.subject = result.getString(9);
				
				book.additionalFields.put("sales", result.getInt(10));
				
				books.add(book);
			}
			
			return SUCCESS;
		},
		"select B.*, ifnull(sales_table.sales, 0) as sales "
		+ "from Books B "
		+ "left outer join "
		+ "(select I.ISBN, sum(I.Quantity) as sales from"
		+ " Includes I group by I.ISBN) as sales_table "
		+ "on B.ISBN = sales_table.ISBN "
		+ "order by sales desc");
	}

	/**
	 * Object[3]: { Long author_id, String author_name, Integer sales }
	 * 
	 * @param authors
	 * @param n
	 * @return
	 */
	public int queryMostPopularAuthors(ArrayList<Object[]> authors, int n) {
		if (authors == null || n < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query = stmts[0];
			
			ResultSet result = query.executeQuery();
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				Object[] author = new Object[]{
						new Long(result.getLong(1)),
						result.getString(2),
						new Integer(result.getInt(3))
				};
				
				authors.add(author);
			}
			
			return SUCCESS;
		},
  		    "select A.Author_id, A.author_name, sum(ifnull(sales_table.sales, 0)) as sales  "
		  + "from  "
		  + "	Authors A natural join Written_by W "
		  + "	left outer join "
		  + "	(select I.ISBN, sum(I.Quantity) as sales from Includes I group by I.ISBN) as sales_table "
		  + "	on W.ISBN = sales_table.ISBN "
		  + "group by A.Author_id, A.Author_name "
		  + "order by sales desc; ");
	}

	/**
	 * Pair of publisher and sales 
	 * 
	 * @param pubs
	 * @param n
	 * @return
	 */
	public int queryMostPopularPublishers(ArrayList<Pair<String, Integer>> pubs, int n) {
		if (pubs == null || n < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query = stmts[0];
			
			ResultSet result = query.executeQuery();
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				Pair<String, Integer> pub = new Pair<String, Integer>(
						result.getString(1),
						result.getInt(2));
				
				pubs.add(pub);
			}
			
			return SUCCESS;
		},
		"select B.Publisher, sum(ifnull(sales_table.sales, 0)) as sales from Books B "
		+ "left outer join "
		+ "(select I.ISBN, sum(I.Quantity) as sales from "
		+ "Includes I group by I.ISBN) as sales_table "
		+ "on B.ISBN = sales_table.ISBN "
		+ "group by B.Publisher "
		+ "order by sales desc");
	}

	/**
	 * trust score is in user.a_field as Integer
	 * 
	 * @param users
	 * @param n
	 * @return
	 */
	public int queryMostTrustedUsers(ArrayList<User> users, int n) {
		if (users == null || n < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query = stmts[0];
			
			ResultSet result = query.executeQuery();
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				User user = new User();
				
				user.login = result.getString(1);
				user.phone = result.getString(3);
				user.name = result.getString(4);
				user.address = result.getString(5);
				user.usertype = result.getByte(6);
				
				user.a_field = result.getInt(7);
				users.add(user);
			}
			
			return SUCCESS;
		},
		  "select U.*, ifnull(scores.trust_score, 0) as trust_score "
		+ "from Users U left outer join "
		+ "(select T.Commented_Login as Login, sum(T.Trust) as trust_score "
		+ "from Trusts T "
		+ "group by T.Commented_Login) as scores "
		+ "on U.Login = scores.Login "
		+ "order by trust_score desc;");
	}

	/**
	 * Score as Double stored in user.a_field.
	 * 
	 * @param users
	 * @param n
	 * @return
	 */
	public int queryMostUsefulUsers(ArrayList<User> users, int n) {
		if (users == null || n < 0) {
			return EINVAL;
		}
		
		return executePreparedStatement((PreparedStatement... stmts) -> {
			PreparedStatement query = stmts[0];
			
			ResultSet result = query.executeQuery();
			int _n = n;
			while ((n == 0 || _n-- > 0) && result.next()) {
				User user = new User();
				
				user.login = result.getString(1);
				user.phone = result.getString(3);
				user.name = result.getString(4);
				user.address = result.getString(5);
				user.usertype = result.getByte(6);
				
				user.a_field = result.getDouble(7);
				users.add(user);
			}
			
			return SUCCESS;
		},
		  "select U.*, ifnull(scores.score, 0) as score "
		+ "from Users U left outer join "
		+ "(select comment_scores.Login, avg(comment_scores.score) as score "
		+ "from (select R.ISBN, R.Commenter_Login as Login, avg(R.Usefulness) as score "
		+ "		from Rates R "
		+ "		group by R.ISBN, R.Commenter_Login) as comment_scores "
		+ "group by comment_scores.Login) as scores "
		+ "on U.Login = scores.Login "
		+ "order by score desc;");
	}
	
	public static final int MAX_LOGIN_LENGTH = 30;
	
	public static final byte UTYPE_ADMIN = 0;
	public static final byte UTYPE_USER = 1;
	public static final byte UTYPE_UPPER = UTYPE_USER + 1;
	
	public static final byte FORMAT_SOFTCOVER = 0;
	public static final byte FORMAT_HARDCOVER = 1;
	public static final byte FORMAT_UPPER = FORMAT_HARDCOVER + 1;
	
	public static final int SUCCESS = 0;
	public static final int EINVAL = 1;
	public static final int ESQLERROR = 2;
	public static final int EINVALIDLOGIN = 3;
	public static final int EINVALIDPASSWD = 4;
	public static final int EDUPLOGIN = 5;
	public static final int EEMPTYORDER = 6;
	public static final int EUNEXPECTED = 7;
	public static final int EBOOKNOTFOUND = 8;
	public static final int EBOOKINSUFF = 9;
	public static final int EBOOKDUP = 10;
	public static final int EPARSE = 11;
	public static final int EDUPCOMMENT = 12;
	public static final int ESELFRATE = 13;
	public static final int EDUPRATING = 14;
	public static final int EDUPTRUST = 15;
	public static final int EORDERNOTFOUND = 16;

	public static final String strerror(int errno) {
		return err_msg[errno];
	}
	
	public static final void print_error(int errno) {
		System.out.println(err_msg[errno]);
	}
	
	public static final String[] err_msg = {
		"success",
		"invalid argument",
		"sql error",
		"invalid login",
		"invalid password",
		"login is used by another user",
		"empty order, cancelled",
		"unexpected",
		"no such book",
		"insufficient copies of the book",
		"duplicate book entry",
		"book query parsing error",
		"duplicate feedback on the same book",
		"cannot rate one's own feedback",
		"duplicate rating on the same feedback",
		"duplicate trusting record",
		"order not found"
	};

	@FunctionalInterface
	static private interface Operations<StatementType> {
		public int invoke(@SuppressWarnings("unchecked") StatementType... stmt) throws SQLException;
	}
	
	private int executePreparedStatement(Operations<PreparedStatement> op, String... stmt_strs) {
		if (stmt_strs.length == 0) {
			return EINVAL;
		}
		
		int errno = ESQLERROR;
		PreparedStatement[] stmt = new PreparedStatement[stmt_strs.length];
		try {
			for (int i = 0; i < stmt_strs.length; ++i) {
				stmt[i] = con.getConnection().prepareStatement(stmt_strs[i]);
			}
			errno = op.invoke(stmt);
		}
		catch (SQLException e) {
			Connector.showException(e);
		}
		finally {
			for (PreparedStatement s : stmt) {
				if (s != null) {
					try {
						s.close();
					} catch (SQLException e) {
					}
				}
			}
		}
		
		if (errno == SUCCESS) {
			commit();
		}
		else {
			rollback();
		}
		return errno;
	}
	
	static public void main(String args[]) throws IOException {
		String property_filename;
		if (args.length > 0) {
			property_filename = args[0];
		}
		else {
			property_filename = "default.xml";
		}
		
		Connector con=null;
		con = new Connector(property_filename);
		DBUtil.println ("Database connection established");		
		
		con.connect();
		if (con.getConnection() == null) {
			System.exit(1);
		}
		
		SQL sql = new SQL(con);
//		User user = new User();
		
		sql.executePreparedStatement((PreparedStatement... stmt) -> {
			ResultSet result = stmt[0].executeQuery();
			result.next();
			System.out.println(result.getInt(1));
			
			return SUCCESS;
		},
		"select B.Num_copies from Books B where B.ISBN = '9781118063330';");
		
		System.in.read();
		
		sql.executePreparedStatement((PreparedStatement... stmt) -> {
			ResultSet result = stmt[0].executeQuery();
			result.next();
			System.out.println(result.getInt(1));
			
			return SUCCESS;
		},
		"select B.Num_copies from Books B where B.ISBN = '9781118063330';");
		sql.close();

		DBUtil.println ("Database connection terminated");
	}
	
}
