package acmdb;

import java.util.HashMap;
import java.util.LinkedList;

public class Order {
	public Order(String login) {
		this.login = login;
		oid = -1;
		items = new HashMap<String, Item>();
		cancelled = null;
	}
	
	public void addItem(String isbn, String title, int num, int price) {
		Item item = items.get(isbn);
		
		if (item != null) {
			num += item.num;
		}
		
		if (num <= 0) {
			if (item != null)
				items.remove(isbn);
		}
		else {
			if (item == null) {
				item = new Item(isbn, title, num, price);
				items.put(isbn, item);
			}
			else {
				item.num = num;
			}
		}
	}
	
	public int getNum(String isbn) {
		Item item = items.get(isbn);
		if (item == null) return 0;
		return item.num;
	}

	public String login;
	public long oid;
	public HashMap<String, Item> items;
	
	public void cancel(String isbn, int errno) {
		Item item = items.get(isbn);
		if (item == null) return ;
		
		items.remove(isbn);
		item.errno = errno;
		if (cancelled == null) {
			cancelled = new LinkedList<Item>();
		}
		cancelled.add(item);
	}
	
	public void clearCancelled() {
		cancelled.clear();
	}

	public static class Item {
		private Item(String isbn, String title, int num, int price) {
			this.isbn = isbn;
			this.title = title;
			this.num = num;
			this.price = price;
			this.errno = SQL.SUCCESS;
		}
		
		public String isbn;
		public int num;
		public int price;	/* in cents */
		public String title;
		public int errno;
		
		@Override
		public String toString() {
			if (errno == SQL.SUCCESS) {
				return String.format("%-13s  %40s %9d %9d.%02d", 
						isbn, 
						(title.length() > 40) ? (title.substring(0, 37) + "...") : title, 
						num, 
						price / 100, price % 100);
			}
			else {
				return String.format("%-13s %9d %s", isbn, num, SQL.strerror(errno));
			}
			
		}
	}
	
	public LinkedList<Item> cancelled;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		if (oid < 0) {
			builder.append(String.format("order (not checked out), login = %s\n", login));
		}
		else {
			builder.append(String.format("order id = %d, login = %s\n", oid, login));
		}
		builder.append(String.format("%-13s  %-40s %9s %12s\n", "isbn", "title", "num", "price"));
		int tot_price = 0;
		
		for (Item item : items.values()) {
			tot_price += item.num * item.price;
			builder.append(item).append("\n");
		}
		builder.append(String.format("\nTotal price: %8d.%02d\n", tot_price / 100, tot_price % 100));
	
		if (cancelled != null && !cancelled.isEmpty()) {
			builder.append("\nCancelled items due to submission failure:\n");
			builder.append(String.format("%-13s %9s %s\n", "isbn", "num", "cause"));
			
			for (Item item : cancelled) {
				builder.append(item).append("\n");
			}
		}
		
		return builder.toString();
	}
}
