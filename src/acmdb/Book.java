package acmdb;

import java.util.HashMap;
import java.util.LinkedList;

public class Book implements java.io.Serializable {
	public static final long serialVersionUID = 1L;

	public Book() {
		isbn = null;
		authors = new LinkedList<Author>();
		additionalFields = new HashMap<String, Object>();
	}
	
	public void clear() {
		isbn = null;
		authors.clear();
	}
	
	public void addAuthor(String author, long id) {
		Author a = new Author();
		a.id = id;
		a.name = author;
		authors.add(a);
	}
	
	public String isbn;
	public String title;
	public String publisher;
	public int year;
	public int n_copies;
	public int price;
	public byte format;
	public String keywords;
	public String subject;
	
	public LinkedList<Author> authors;

	/**
	 * Possible fields include:
	 * <ol>
	 * <li> "sales": Integer
	 * </ol>
	 * 
	 */
	public HashMap<String, Object> additionalFields;

	public final static int briefInfoLength() {
		return 41 + 9 + 12;
	}
	public static final String briefInfoHeader = String.format(
			"%-40s %9s %12s", "title", "n_copies", "price");
	
	public String briefInfo() {
		return String.format(
				"%-40s %9d %9d.%02d",
				(title.length() > 40) ? (title.substring(0, 37) + "...") : title,
				n_copies,
				price / 100, price % 100);		
	}
	
	public String fullInfo() {
		StringBuilder b = new StringBuilder();
		
		b.append("title: ").append(title).append("\n")
		 .append("isbn: ").append(isbn).append("\n")
		 .append("publisher: ").append(publisher).append("\n")
		 .append("year: ").append(year).append("\n")
		 .append("n_copies: ").append(n_copies).append("\n")
		 .append("price: ").append(String.format("%d.%02d", price / 100, price % 100)).append("\n")
		 .append("format: ").append((format == 0) ? "softcover" : "hardcover").append("\n")
		 .append("keywords: ").append(keywords).append("\n")
		 .append("subject: ").append(subject).append("\n")
		 .append("author(s): ").append("\n");
		 
		for (Author author : authors) {
			b.append("\t").append(author.name).append('\n');
		}
		
		return b.toString();
	}
	
	@Override
	public String toString() {
		if (isbn == null) return "null";
		
		return String.format("%s\t%s\t%s\t%d\t%d\t%8d.%02d\t%9s\t%s\t%s\t",
				isbn, title, (publisher == null) ? "N/A" : publisher,
				year, n_copies, price / 100, price % 100,
				(format == SQL.FORMAT_SOFTCOVER) ? "softcover" : "hardcover",
				(keywords == null) ? "N/A" : keywords,
				(subject == null) ? "N/A" : subject);
	}
	
	public static final String header = 
			"isbn\ttitle\tpublisher\tyear of publication\tn_copies\tprice\tformat\tkeywords\tsubject";
}
