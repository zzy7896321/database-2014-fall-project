package acmdb;

import java_cup.runtime.*;
import java_cup.runtime.ComplexSymbolFactory.Location;

%%

%class Lex
%public 

%unicode
%ignorecase
%line
%column

%cup

%{
	SymbolFactory sf = new ComplexSymbolFactory();
	
	Symbol newSymbol(int type) {
		return newSymbol(type, null);
	}

	Symbol newSymbol(int type, Object o) {
		Location left;
		if (prev_line < yyline) {
			left = new Location(yyline + 1, 1);
		}
		else {
			left = new Location(prev_line + 1, prev_column + 2);
		}

		Location right = new Location(yyline + 1, yycolumn + 1);

		return sf.newSymbol("String", type, left, right, o);
	}

	int prev_line = 0;
	int prev_column = 0;
%}

WhiteSpace = [ \t\f] | \R

%%

{WhiteSpace}* 		{ prev_line = yyline; prev_column = yycolunm; }

"AND" | "&&"		{ return newSymbol(sym.AND); }
"OR"  | "||"		{ return newSymbol(sym.OR);	}
"NOT" | "!"			{ return newSymbol(sym.NOT); }
"("					{ return newSymbol(sym.LBRACKET);	}
")"					{ return newSymbol(sym.RBRACKET);	}

"LIKE"				{ return newSymbol(sym.LIKE); }

'(\\.|[^\\'])*'		{ return newSymbol(sym.STRING_LITERAL, yytext().substring(1, yytext.length() - 1)); }
\"(\\.|[^\\"])*\"	{ return newSymbol(sym.STRING_LITERAL, yytext().substring(1, yytext.length() - 1));	}

"ORDER BY"			{ return newSymbol(sym.ORDERBY);	}
"ASC"				{ return newSymbol(sym.ASC);	}
"DESC"				{ return newSymbol(sym.DESC);	}
","					{ return newSymbol(sym.COMMA);	}

"PUBLISHED BY"		{ return newSymbol(sym.PUBLISHEDBY); }
"AUTHORED BY"		{ return newSymbol(sym.AUTHOREDBY); }
"WITH TITLE"		{ return newSymbol(sym.WITHTITLE);	}
"WITH SUBJECT"		{ return newSymbol(sym.WITHSUBJECT); }
"WITH KEYWORDS"		{ return newSymbol(sym.WITHKEYWORDS); }

