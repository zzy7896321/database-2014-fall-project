package acmdb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Not a console. Prompts are thrown away.
 * 
 */
public class StdIO implements UserIO {
	
	public StdIO() {
		reader = new BufferedReader(new InputStreamReader(System.in));
	}
	
	@Override
	public String readLine(String fmt, Object... args) {
		System.out.printf(fmt, args);
		String ret = null;
		try {
			ret = reader.readLine();
		} catch (IOException e) {
		}
		return ret;
	}

	@Override
	public String readPassword(String fmt, Object... args) {
		System.out.printf(fmt, args);
		String ret = null;
		try {
			ret = reader.readLine();
		} catch (IOException e) {
		}
		return ret;
	}

	@Override
	public void printf(String fmt, Object... args) {
		System.out.printf(fmt, args);
	}

	private BufferedReader reader;
}
