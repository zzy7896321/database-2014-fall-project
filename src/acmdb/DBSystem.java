package acmdb;


public class DBSystem {
	
	public static void main(String[] args) {
		String property_filename;
		if (args.length > 0) {
			property_filename = args[0];
		}
		else {
			property_filename = "default.xml";
		}
		
		Connector con=null;
		con = new Connector(property_filename);
		
		con.connect();
		if (con.getConnection() == null) {
			DBUtil.println("Cannot connect to the database.");
			System.exit(1);
		}
		DBUtil.println ("Database connection established");		
		
		SQL sql = new SQL(con);
		
		UserInterface UI = new UserInterface(sql);
		UI.start();
		
		sql.close();

		DBUtil.println ("Database connection terminated");
	}

}
