package acmdb;

import java.sql.Timestamp;

public class Comment {
	public String login;
	public int score;
	public Timestamp date;	
	public String desc;
	
	@Override
	public String toString() {
		return String.format(
			"User: %s, score: %d, time: %tc\n%s\n", login, score, date, desc);
	}
}
