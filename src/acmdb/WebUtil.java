package acmdb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;

public class WebUtil {

	/**
	 * Creates a connection to the database.
	 *
	 * @return an SQL object, or <tt>null</tt> if connection failed
	 */
	public static SQL getSQL() {
		Connector con = 
			new Connector("localhost", "acmdbu02", "hhikcg16");

		con.connect();
		if (con.getConnection() == null) {
			return null;
		}
	
		SQL sql = new SQL(con);

		return sql;
	}

	/**
	 * Close the sql object.
	 */
	public static void closeSQL(SQL sql) {
		sql.close();
	}
	
	public static int addWrapAround(int a, int b) {
		int c = a + b;
		if (c < 0) {
			c = b - (Integer.MAX_VALUE - a);
		}
		return c;
	}

	public static boolean inRange(int x, int left, int length) {
		int offset = x - left;
		if (offset < 0) {
			offset = x + (Integer.MAX_VALUE - left);
		}
		return offset >= 0 && offset < length;
	}

	public static int offset(int x, int left) {
		int offset = x - left;
		if (offset < 0) {
			offset = x + (Integer.MAX_VALUE - left);
		}
		return offset;
	}

	public static int extractInteger(Object o, int defaultValue) {
		if (o == null) return defaultValue;
		if (!(o instanceof Integer)) return defaultValue;
		return ((Integer) o).intValue();
	}

	public static int parseInt(String s, int defaultValue) {
		if (s == null) return defaultValue;
		try {
			return Integer.parseInt(s);
		}
		catch (NumberFormatException e) {
			return defaultValue;
		}
	}

	public static int parsePrice(String s, int defaultValue) {
		if (s == null) return defaultValue;
		return DBUtil.checkPrice(s, defaultValue);
	}

	public static String parseISBN(String s) {
		if (s == null) return null;
		return DBUtil.check_ISBN(s);
	}

	public static long parseLong(String s, long defaultValue) {
		if (s == null) return defaultValue;
		try {
			return Long.parseLong(s);
		}
		catch (NumberFormatException e) {
			return defaultValue;
		}
	}
	
	public static BookQuery getBasicBookQuery(String queryString, String orderby) {
		if (queryString != null && queryString.isEmpty()) queryString = null;

		BookQuery query = null;
		String _isbn = null;
		if (queryString != null)
			_isbn = DBUtil.check_ISBN(queryString);

		if (_isbn != null) {
			query = new BookQuery(null, _isbn, null, null, null, null, orderby);
		}
		else {
			query = new BookQuery(queryString, null, null, null, null, null, orderby);
		}
		return query;
	}

	public static BookQuery getISBNBookQuery(String isbn) {
		String _isbn = parseISBN(isbn);
		if (_isbn == null) return null;
		return new BookQuery(null, _isbn, null, null, null, null, "0");
	}

	public static ArrayList<Book> queryBooks(
			SQL sql, 
			String login,
			BookQuery query) {
		if (sql == null) return null;
		if (query == null) return null;

		ArrayList<Book> books = new ArrayList<Book>();
		int result = sql.searchForBooksWithBookQuery(query, login, 0, 0, books, null);
		if (result != SQL.SUCCESS) {
			return null;
		}

		return books;
	}
	
	public static BookQuery getAdvancedBookQuery(
			String title, 
			String isbn, 
			String author, 
			String publisher, 
			String subject,
			String keywords,
			String orderby) {
		
		if (title != null && title.isEmpty()) title = null;
		if (isbn != null && isbn.isEmpty()) isbn = null;
		if (author != null && author.isEmpty()) author = null;
		if (publisher != null && publisher.isEmpty()) publisher = null;
		if (subject != null && subject.isEmpty()) subject = null;
		if (keywords != null && keywords.isEmpty()) keywords = null;

		String _isbn = null;
		if (isbn != null) {
			_isbn = isbn.replaceAll("-", "");
		}
		BookQuery query = new BookQuery(title, _isbn, author, publisher, subject, keywords, orderby);

		return query;
	}

	public static ArrayList<Comment> queryComments(SQL sql, String isbn, int n, char order) {
		if (sql == null || isbn == null) return null;

		ArrayList<Comment> comms = new ArrayList<Comment>();	
		int result = sql.queryComments(isbn, comms, n, order);
		if (result != SQL.SUCCESS) {
			return null;
		}

		return comms;
	}

	public static int[] queryTrusts(SQL sql, String login, ArrayList<Comment> comms) {
		if (comms == null) return null;
		String[] logins = new String[comms.size()];
		for (int i = 0; i < logins.length; ++i) {
			logins[i] = comms.get(i).login;
		}
		return queryTrusts(sql, login, logins);
	}

	public static int[] queryTrusts(SQL sql, String login, String[] logins) {
		if (sql == null || login == null || logins == null) {
			return null;	
		}
		int[] trusts = new int[logins.length];
		if (logins.length > 0) {
			int result = sql.queryTrustState(login, logins, trusts);
			if (result != SQL.SUCCESS) {
				return null;
			}
		}
		return trusts;
	}

	public static int[] queryUsefulness(SQL sql, String isbn, String login, ArrayList<Comment> comms) {
		if (comms == null) return null;
		int[] ratings = new int[comms.size()];
		if (ratings.length > 0) {
			int result = sql.queryRating(isbn, comms, login,  ratings);
			if (result != SQL.SUCCESS) {
				return null;
			}
		}
		return ratings;
	}

	public static HashMap<String, Book> queryBooksInOrder(SQL sql, String login, Order order) {
		if (sql == null || order == null)
			return null;

		HashMap<String, Book> books = new HashMap<String, Book>();
		ArrayList<Book> arr_books = new ArrayList<Book>();
		for (String isbn : order.items.keySet()) {
			BookQuery query = getISBNBookQuery(isbn);
			arr_books.clear();
			int result = sql.searchForBooksWithBookQuery(query, login, 0, 0, arr_books, null);
			if (result == SQL.SUCCESS) {
				books.put(isbn, arr_books.get(0));
			}
		}

		return books;
	}

	public static ArrayList<PastOrder> queryPastOrders(SQL sql, String login, int n) {
		if (sql == null || login == null || n < 0) return null;
			
		ArrayList<Long> ids = new ArrayList<Long>();
		int result = sql.queryOrderIds(login, n, ids);
		if (result != SQL.SUCCESS) {
			return null;
		}

		ArrayList<PastOrder> orders = new ArrayList<PastOrder>();
		Pointer<PastOrder> o = new Pointer<PastOrder>();
		for (int i = 0, tar = Math.min(n, ids.size()); i < tar;  ++i) {
			result = sql.queryOrderById(ids.get(i), o);
			if (result != SQL.SUCCESS) {
				return null;
			}
			orders.add(o.value);
		}

		return orders;
	}

	public static String formatTrust(int trust) {
		if (trust == -1) {
			return "distrusted";
		}
		else if (trust == 0) {
			return "neutral";
		}
		else if (trust == 1) {
			return "trusted";
		}
		else {
			return "N/A";
		}
	}

	public static String formatUsefulness(Integer usefulness) {
		if (usefulness == null) {
			return "N/A";
		}
		int _u = usefulness;
		if (_u == 0) {
			return "useless";
		}
		else if (_u == 1){
			return "useful";
		}
		else if (_u == 2) {
			return "very useful";
		}
		else {
			return "N/A";
		}
	}
	
	public static String getString(String str) {
		if (str == null) return "";
		return str;
	}

	public static String getString(Object str) {
		if (str == null) return "";
		return str.toString();
	}

	public static String basename(String str) {
		if (str == null) return "";
		return str.substring(str.lastIndexOf("/") + 1, str.length());
	}

	public static String formatPrice(int price) {
		return String.format("%d.%02d", price / 100, price % 100);
	}

	public static String formatScore(double score) {
		return String.format("%.1f", score);
	}
	

		public static int newBook(
			SQL sql,
			String isbn,
			String title,
			String publisher,
			String year,
			int format,
			String num,
			String price,
			String keywords,
			String subject,
			LinkedList<Author> authorList) {
		if (sql == null) return 9;
		isbn = parseISBN(isbn);
		if (isbn == null) return 1;
		if (title.length() == 0) return 2;
		int iYear = parseInt(year, -1);
		if (iYear < 0) return 3;
		int iNum = parseInt(num, -1);
		if (iNum < 0) return 4;
		int iPrice = parsePrice(price, -1);
		if (iPrice < 0) return 5;
		if (format != 0 && format != 1) return 6;		
		Book book = new Book();
		book.isbn = isbn;
		book.title = title;
		book.publisher = publisher;
		book.year = iYear;
		book.n_copies = iNum;
		book.price = iPrice;
		book.format = (byte)format;
		book.keywords = keywords;
		book.subject = subject;
		book.authors = authorList;
		int result = sql.addBook(book);
		if (result == SQL.EBOOKDUP) return 7;
		if (result == SQL.EUNEXPECTED) return 8;
		return 0;
	}

	public static final int bookQueryQueueSize() {
		return 5;
	}
}


