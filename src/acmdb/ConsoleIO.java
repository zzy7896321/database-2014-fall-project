package acmdb;

public class ConsoleIO implements UserIO {
	
	@Override
	public String readLine(String fmt, Object... args) {
		return System.console().readLine(fmt, args);
	}

	@Override
	public String readPassword(String fmt, Object... args) {
		return new String(System.console().readPassword(fmt, args));
	}

	@Override
	public void printf(String fmt, Object... args) {
		System.console().printf(fmt, args);
	}
}
