package acmdb;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserInterface {
	SQL sql;
	User user;
	Order order;

    public UserInterface(SQL sql) {
    	this.sql = sql;
    	this.user = new User();
    	this.order = null;
    }

    public int displayMainMenu()
    {
    	int upper;
        DBUtil.println("\n        Book Store");
        DBUtil.println("1. User Register");
        if (user.login == null) 
        	DBUtil.println("2. Login");
        else
        	DBUtil.println("2. Logout");
        DBUtil.println("3. Exit");
        upper = 3;
        
        if (user.login != null) {
        	
        	DBUtil.println("");
        	/* ordinary user */
        	DBUtil.println("4. Show my info");
        	DBUtil.println("5. Show my cart");
        	DBUtil.println("6. Clear my cart");
        	DBUtil.println("7. Search books");
        	DBUtil.println("8. Checkout");
        	DBUtil.println("9. Recommend books");
        	DBUtil.println("10. Author distance");
        	upper = 10;
        	
        	if (user.usertype == SQL.UTYPE_ADMIN) {
        		DBUtil.println("");
        		DBUtil.println("11. Management");
        		upper = 11;
        	}
        }
        DBUtil.println("");
        return upper;
    }
    
    
    public void start() {
        mainMenu();
    }
    
    public void mainMenu() {
		while (true) {
			int upper = displayMainMenu();
			try {
				switch (DBUtil.getInt(1, upper, "bookstore> ")) {
				case 1:	/* register */
					handleRegister(false);
					break;
				case 2:	/* login/logout */
					if (user.login == null)
						handleLogin();
					else
						handleLogout();
					break;
				case 3: 	/* exit */
					handleLogout();
					return ;
					
				case 4:	/* show my info */
					handleShowInfo();
					break;
					
				case 5:	/* show my cart */
					DBUtil.println(order.toString());
					break;
				case 6:	/* clear my cart */
					order = new Order(user.login);
					DBUtil.println("cart cleared");
					break;
					
				case 7:	/* search for books */
					handleSearch();
					break;
					
				case 8:	/* checkout */
					handleCheckout();
					break;
					
				case 9:	/* recommend */
					handleRecommend();
					break;
				
				case 10:	/* author distance */
					handleAuthorDistance();
					break;
					
				case 11:	/* management */
					management();
					break;
				}    		
			}
			catch (IOException e) {
				DBUtil.println("io error");
			}
		}
	}
   
    public void showInfoPrompt() throws IOException {
    	DBUtil.println("");
    	DBUtil.printf(prompt_fmt, "p", "change password");
    	DBUtil.printf(prompt_fmt, "n [name]", "change name");
    	DBUtil.printf(prompt_fmt, "t [phone]", "change phone");
    	DBUtil.printf(prompt_fmt, "a [address]", "change address");
    	DBUtil.printf(prompt_fmt, "s", "re-print your information");
    	DBUtil.printf(prompt_fmt, "h", "show this help");
    	DBUtil.printf(prompt_fmt, "q", "return");
    }
    
    public void updateInfo(int field, String prompt, String new_info) throws IOException {
    	if (new_info == null || new_info.isEmpty()) {
    		new_info = DBUtil.getStr("new " + prompt + ": ");
    	}

    	int result = sql.userUpdateInfo(user.login, field, new_info);
    	if (result != SQL.SUCCESS) {
    		DBUtil.printf(prompt + " update failed: %s\n", SQL.strerror(result));
    		return ;
    	}
    	
    	DBUtil.printf(prompt + " update succeeded\n");
    	switch (field) {
    	case SQL.UF_NAME:
    		user.name = new_info;
    		break;
    	case SQL.UF_ADDR:
    		user.address = new_info;
    		break;
    	case SQL.UF_PHONE:
    		user.phone = new_info;
    		break;
    	}
    }
    
    public void handleShowInfo() throws IOException {
    	showInfoPrompt();
    	while (true) {
    		DBUtil.printf("\n%-16s %-16s %-16s %-16s %s\n", 
    				"login", "name", "phone", "address", "user", "type");
    		DBUtil.println(user.toString());
    	
    		COMMAND:
    		while (true) {
    			DBUtil.printf("\ninfo> ");
    			
    			String command = DBUtil.getStrN();
    			String[] tokens = command.split("[ \t]+");
    			
    			if (tokens.length == 0) {
    				DBUtil.printf("invalid command");
    				continue;
    			}
    			
    			switch (tokens[0].charAt(0)) {
    			case 's':	/* re-print info */
    				break COMMAND;
    				
    			case 'h':	/* print help */
    				showInfoPrompt();
    				continue;
    				
    			case 'q':	/* return */
    				return ;
    				
    			case 'p':	/* change password */
    				{
    					String opasswd = DBUtil.getPassword("old password: ");
    					String npasswd = DBUtil.getPassword("new password: ");
    					String npasswd_confirm = DBUtil.getPassword("confirm new password: ");
    					
    					if (!npasswd.equals(npasswd_confirm)) {
    						DBUtil.println("password mismatch");
    						continue;
    					}
    					
    					int result = sql.userChangePasswd(user.login, opasswd, npasswd);
    					if (result != SQL.SUCCESS) {
    						DBUtil.printf("password update failed: %s\n", SQL.strerror(result));
    						continue;
    					}
    					
    					DBUtil.printf("password update succeeded, please login again\n");
    					user.clear();
    					return ;
    				}
    				
    			case 'n':	/* change name */
    				updateInfo(SQL.UF_NAME, "name", (tokens.length >= 2) ? tokens[1] : null);
    				continue;
    				
    			case 't':
    				updateInfo(SQL.UF_PHONE, "phone", (tokens.length >= 2) ? tokens[1] : null);
    				continue;
    				
    			case 'a':	/* change address */
    				updateInfo(SQL.UF_ADDR, "address", (tokens.length >= 2) ? tokens[1] : null);
    				continue;
    				
    			}	/* switch */
    		}	/* while */
    	}	
    }

	private void handleRegister(boolean is_admin) throws IOException {
        String username;
        String passwd;
        String passwd2;
        String name;
        String phone;
        String address;
        
		username = DBUtil.getStr("username: ");
		passwd = DBUtil.getPassword("password: ");
		passwd2 = DBUtil.getPassword("confirm your password: ");
        if (!passwd.equals(passwd2)) {
        	DBUtil.println("Password mismatch!");
        	return;
        }
        
        name = DBUtil.getStr("Your name: ");
        phone = DBUtil.getStr("Your phone: ");
        address = DBUtil.getStr("Your address: ");
        
        int result = (is_admin) ? sql.adminReg(username, passwd, phone, name, address)
        		: sql.userReg(username, passwd, phone, name, address);
        if (result == SQL.SUCCESS) {
        	DBUtil.printf("Register succeeded. You can now login as %s.\n", username);
        }
        else {
        	DBUtil.printf("Register failed: %s\n", SQL.strerror(result));
        }
    }
    
    private void handleLogin() throws IOException {
        String username = DBUtil.getStr("username: ");
        String password = DBUtil.getPassword("password: ");      
        
        int result = sql.userLogin(username, password, user = new User());
        if (result != SQL.SUCCESS) {
        	user.clear();
        	DBUtil.printf("login failed: %s\n", SQL.strerror(result));
        }
        else {
        	DBUtil.printf("Welcome back, %s!\n", user.login);
        	order = new Order(user.login);
        }
    }
    
    private void handleLogout() throws IOException {
    	if (user.login != null) {
    		DBUtil.printf("Bye, %s.\n", user.login);
    		user.clear();
    		order = null;
    	}
	}
    
    @SuppressWarnings("unused")
	private ArrayList<Book> queryBooks() throws IOException {
    	
    	String query = DBUtil.getMultilineString("query> ", "", 0, "enter your query (empty line to end):\n");
    	
    	ArrayList<Book> books = new ArrayList<Book>();
    	int result = sql.searchForBooks(query, user.login, books, MAX_NUM_BOOK_QUERY_RESULTS);
    	if (result != SQL.SUCCESS) {
    		DBUtil.printf("query failed: %s\n", SQL.strerror(result));
    		return null;
    	}
    	
    	if (books.isEmpty()) {
    		DBUtil.printf("no such books found\n");
    		return null;
    	}
    	
    	if (MAX_NUM_BOOK_QUERY_RESULTS != 0 && books.size() == MAX_NUM_BOOK_QUERY_RESULTS) {
    		DBUtil.printf("too many results; only the first %d results is retrieved.\n", MAX_NUM_BOOK_QUERY_RESULTS);
    	}
    	return books;
    }
   
    private void handleSearch() throws IOException {
    	ArrayList<Book> books = queryBooks();
    	if (books == null) return ;
    	
    	viewBooks(books);
    }
    
    private void handleRecommend() throws IOException {
    	DBUtil.println("best seller among those ordered by users who ever ordered at least one book that you've ordered.");
    	int n = DBUtil.getInt(0, null, "top n (0 for unlimited): ");
    	
    	ArrayList<Book> books = new ArrayList<Book>();
    	int result = sql.recommendBooks(user.login, books, n);
    	
    	if (result != SQL.SUCCESS) {
    		DBUtil.printf("recommendation failed: %s\n", SQL.strerror(result));
    		return ;
    	}
    	
    	if (books.isEmpty()) {
    		DBUtil.println("no such books");
    		return ;
    	}
    	
    	viewBooks(books);    	
    }
  
    private static final String prompt_fmt = "%-26s%s\n";
    private void viewBookPrompt() {
    	DBUtil.println("");
    	DBUtil.printf(prompt_fmt, "i <no.>", "show detailed infomation of no.th book");
    	DBUtil.printf(prompt_fmt, "o <no.> <num>", "order num copies of no.th book");
    	DBUtil.printf(prompt_fmt, "c <no.>",  "provide feedback on the no.th book");
    	DBUtil.printf(prompt_fmt, "v <no.> [num] [t|u|n]", "view the [num](0 for unlimited, default:0)");
    	DBUtil.printf(prompt_fmt, "", "feedbacks on the no.the book, t for time order,");
    	DBUtil.printf(prompt_fmt, "", "u for usefulness order, n for no specific order (default: n)");
    	DBUtil.printf(prompt_fmt, "p", "show the previous page");
    	DBUtil.printf(prompt_fmt, "n", "show the next page");
    	DBUtil.printf(prompt_fmt, "s", "re-print this page");
    	DBUtil.printf(prompt_fmt, "h", "show this help");
    	DBUtil.printf(prompt_fmt, "q", "return");
    }
    
    private void viewBooks(ArrayList<Book> books) throws IOException {
    	
    	int total_n_pages = (books.size() + NUM_BOOKS_PER_PAGE - 1) / NUM_BOOKS_PER_PAGE;
    	int now = 0;
    	
    	viewBookPrompt();
    	
    	while (true) {
    		int tar = Math.min(books.size(), now + NUM_BOOKS_PER_PAGE);
    		int pn = now / NUM_BOOKS_PER_PAGE + 1;
    
    		DBUtil.printf("\npage %d/%d, record %d-%d (%d in total)\n", pn, total_n_pages, now, tar - 1, books.size());
    		DBUtil.println("no.  " + Book.briefInfoHeader);
    		for (int i = now ; i < tar; ++i) {
    			DBUtil.printf("%-5d%s\n", i, books.get(i).briefInfo());
    		}
    		
    		String command;
    		COMMAND:
    		while (true) {
    			DBUtil.printf("\nbook> ");
    			
    			command = DBUtil.getStrN();
    			String[] tokens = command.split(" ");
    			if (tokens.length == 0 || tokens[0].length() == 0) {
    				DBUtil.println("unknown command");
    				continue;
    			}
    			switch (command.charAt(0)) {
    			case 'p':	/* previous page */
    				if (now != 0) {
    					now -= NUM_BOOKS_PER_PAGE;
    				}
    				break COMMAND;

    			case 'n':	/* next page */
    				if (tar != books.size()) {
    					now += NUM_BOOKS_PER_PAGE;
    				}
    				break COMMAND;
    				
    			case 's':
    				break COMMAND;
    				
    			case 'h':
    				viewBookPrompt();
    				continue;

    			case 'q':	/* quit */
    				return ;
    				
    			case 'i':
    				{
    					if (tokens.length < 2) {
    						DBUtil.println("i <no.>, missing");
    						continue;
    					}
    					
	    				int no;
	    				try {
	    					no = Integer.parseInt(tokens[1]);
	    				} catch (NumberFormatException e) {
	    					DBUtil.println("no. is not a number");
	    					continue;
	    				}
	    				
	    				if (no < 0 || no >= books.size()) {
	    					DBUtil.printf("no is out of range [0..%d)\n", books.size());
	    					continue;
	    				}
	    				
	    				DBUtil.printf(books.get(no).fullInfo());
	    				continue;
    				}

    			case 'o':	/* order */
	    			{
	    				if (tokens.length < 3) {
	    					DBUtil.println("o <no.> <num>, something is missing");
	    					continue;
	    				}

	    				int no;
	    				int num;
	    				try {
	    					no = Integer.parseInt(tokens[1]);
	    				} catch (NumberFormatException e) {
	    					DBUtil.println("no. is not a number");
	    					continue;
	    				}
	    				try {
	    					num = Integer.parseInt(tokens[2]);
	    				}
	    				catch (NumberFormatException e) {
	    					DBUtil.println("num is not a number");
	    					continue;
	    				}

	    				if (no < 0 || no >= books.size()) {
	    					DBUtil.printf("no is out of range [0..%d)\n", books.size());
	    					continue;
	    				}

	    				Book book = books.get(no);
	    				int num_in_order = order.getNum(book.isbn);
	    				if (num <= 0 || num + num_in_order > book.n_copies) {
	    					DBUtil.printf("num is out of range (0..%d]\n", book.n_copies - num_in_order);
	    					continue;
	    				}

	    				order.addItem(book.isbn, book.title, num, book.price);
	    				DBUtil.println("items are put to your cart; you can checkout on the main menu.");
	    				continue;
	    			}	/* end order */
	    			
    			case 'c':	/* feedback */
    				{
    					if (tokens.length < 2) {
    						DBUtil.println("c <no.>, something is missing");
    						continue;
    					}
    					
    					int no;
    					try {
    						no = Integer.parseInt(tokens[1]);
    					}
    					catch (NumberFormatException e) {
    						DBUtil.println("no. is not a number");
    						continue;
    					}
    					
    					if (no < 0 || no >= books.size()) {
    						DBUtil.printf("no. is out of range [0..%d)\n", books.size());
    						continue;
    					}
    					
    					int score = DBUtil.getInt(0, 10, "Please input the score(0-10): ");
    					String desc = DBUtil.getMultilineString("dsc> ", "", 255, "Please input the description (empty line to end and <= 255 characters):\n");
    					
    					int result = sql.postComments(books.get(no).isbn, user.login, score, desc);
    					if (result != SQL.SUCCESS) {
    						DBUtil.printf("posting failed: %s\n", SQL.strerror(result));
    					}
    					else {
    						DBUtil.printf("posting succeeded\n");
    					}
    					continue;
    				}
    				
    			case 'v':
    				{
    					if (tokens.length < 2) {
    						DBUtil.println("c <no.> [num] [t|u|n], something is missing");
    						continue;
    					}
    					
    					int no;
    					try {
    						no = Integer.parseInt(tokens[1]);
    					}
    					catch (NumberFormatException e) {
    						DBUtil.println("no. is not a number");
    						continue;
    					}
    					
    					if (no < 0 || no >= books.size()) {
    						DBUtil.printf("no. is out of range [0..%d)\n", books.size());
    						continue;
    					}
    					
    					int n = MAX_NUM_COMMENT_QUERY_RESULTS;
    					if (tokens.length >= 3) {
    						try {
    							n = Integer.parseInt(tokens[2]);
    						}
    						catch (NumberFormatException e) {
    							DBUtil.println("num is not a number");
    							continue;
    						}
    					}
    					
    					char orderby = 'n';
    					if (tokens.length >= 4) {
    						if (!tokens[3].isEmpty()) {
    							orderby = tokens[3].charAt(0);
    							if (orderby != 't' && orderby != 'u' && orderby != 'n') {
    								DBUtil.println("invalid orderby character");
    								continue;
    							}
    						}
    					}
    					
    					handleRating(books.get(no).isbn, n, orderby);
    					continue;
    				}
    				
    			default:
    				DBUtil.println("unknown command");
	    		
    			}	/* switch token[0] */
    		}	/* command loop */
    	}	/* book display loop */
    }
    
    private final int MAX_NUM_BOOK_QUERY_RESULTS = 0;
    private final int NUM_BOOKS_PER_PAGE = 16;
   
    private void ratingPrompt() {
    	DBUtil.println("");
    	DBUtil.printf(prompt_fmt, "r <no.> <rating>", "rate the no.th feedback, (0 for useless, 1 for useful, 2 for very usefule)");
    	DBUtil.printf(prompt_fmt, "t <no.> <1|0|-1>", "mark the user of nth as trusted(1) or untrusted(-1) or neutral (0)"); 
    	DBUtil.printf(prompt_fmt, "p", "show the previous page");
    	DBUtil.printf(prompt_fmt, "n", "show the next page");
    	DBUtil.printf(prompt_fmt, "s", "re-print this page");
    	DBUtil.printf(prompt_fmt, "h", "show this help");
    	DBUtil.printf(prompt_fmt, "q", "return");
    }
    
    private void handleRating(String isbn, int limit, char orderby) throws IOException {
    	ArrayList<Comment> comms = new ArrayList<Comment>();
    	
    	int result = sql.queryComments(isbn, comms, limit, orderby);
    	if (result != SQL.SUCCESS) {
    		DBUtil.printf("failed to find feedbacks: %s\n", SQL.strerror(result));
    		return ;
    	}
    	
    	if (comms.isEmpty()) {
    		DBUtil.printf("no feedbacks found\n");
    		return ;
    	}
    
    	ratingPrompt();
    	
    	int total_n_pages = (comms.size() + NUM_COMMENTS_PER_PAGE - 1) / NUM_COMMENTS_PER_PAGE;
    	int now = 0;
    	while (true) {
    		int tar = Math.min(now + NUM_COMMENTS_PER_PAGE, comms.size());
    		int pn = now / NUM_COMMENTS_PER_PAGE + 1;
    		
    		DBUtil.printf("\npage %d/%d, record %d-%d (%d in total)\n", pn, total_n_pages, now, tar - 1, comms.size());
    		for (int i = now; i < tar; ++i) {
    			DBUtil.println("--------------------------------------------------------------------");
    			DBUtil.printf("no. %d, ", i);
    			DBUtil.println(comms.get(i).toString());
    		}
   			DBUtil.println("--------------------------------------------------------------------");
    		
    		COMMAND:
    		while (true) {
    			DBUtil.printf("\nfeedback> ");
    			String command = DBUtil.getStrN();
    			String[] tokens = command.split(" ");
    			
    			if (tokens.length == 0 || tokens[0].length() == 0) {
    				DBUtil.println("unknown command");
    				continue;
    			}
    			switch (command.charAt(0)) {
    			case 'p':	/* previous page */
    				if (now != 0) {
    					now -= NUM_COMMENTS_PER_PAGE;
    				}
    				break COMMAND;

    			case 'n':	/* next page */
    				if (tar != comms.size()) {
    					now += NUM_COMMENTS_PER_PAGE;
    				}
    				break COMMAND;
    				
    			case 's':	/* re-print this page */
    				break COMMAND;
    				
    			case 'h':
    				ratingPrompt();
    				continue;
    				
    			case 'q':	/* quit */
    				return ;
    				
    			case 'r':	/* rate */
    				{
    					if (tokens.length < 3) {
    						DBUtil.println("r <no.> <rating>, something is missing");
    						continue;
    					}
    					
    					int no;
    					try {
    						no = Integer.parseInt(tokens[1]);
    					}
    					catch (NumberFormatException e) {
    						DBUtil.println("no. is not a number");
    						continue;
    					}
    					
    					if (no < 0 || no >= comms.size()) {
    						DBUtil.printf("no. is out of range [0..%d)\n", comms.size());
    						continue;
    					}
    						
    					int rating;
    					try {
    						rating = Integer.parseInt(tokens[2]);
    					}
    					catch (NumberFormatException e) {
    						DBUtil.println("rating is not a number");
    						continue;
    					}
    					
    					if (rating < 0 || rating > 2) {
    						DBUtil.println("rating is out of range {0, 1, 2}");
    						continue;
    					}
    					
    					/* TODO post rating */
    					Comment comm = comms.get(no);
    					
    					result = sql.postRating(isbn, comm.login, user.login, rating);
    					if (result != SQL.SUCCESS) {
    						DBUtil.printf("rating failed: %s\n", SQL.strerror(result));
    					}
    					else {
    						DBUtil.println("rating done");
    					}
    					continue;
    				}
    				
    			case 't':	/* trust */
    				{
    				
    					if (tokens.length < 3) {
    						DBUtil.println("t <no.> <1|0|-1>, something is missing");
    						continue;
    					}
    					
    					int no;
    					try {
    						no = Integer.parseInt(tokens[1]);
    					}
    					catch (NumberFormatException e) {
    						DBUtil.println("no. is not a number");
    						continue;
    					}
    					
    					if (no < 0 || no >= comms.size()) {
    						DBUtil.printf("no. is out of range [0..%d)\n", comms.size());
    						continue;
    					}
    					
    					int trusted;
    					try {
    						trusted = Integer.parseInt(tokens[2]);
    					}
    					catch (NumberFormatException e) {
    						DBUtil.println(tokens[2] + " is not a number");
    						continue;
    					}
    					
    					if (trusted < -1 || trusted > 1) {
    						DBUtil.println("t-val is out of range {-1, 0, 1}");
    						continue;
    					}
    					
    					result = sql.updateTrustState(user.login, comms.get(no).login, trusted);
    					if (result != SQL.SUCCESS) {
    						DBUtil.printf("trust failed: %s\n", SQL.strerror(result));
    					}
    					else {
    						DBUtil.printf("trust done\n");
    					}
    					
    					continue;
    				}

    			default:
    				DBUtil.println("unknown command");
	    		
    			}	/* switch token[0] */
    			
    		}	/* command loop */
    		
    	}	/* feedback display loop */
    }
    
    private final int MAX_NUM_COMMENT_QUERY_RESULTS = 0;	/* unlimited */
    private final int NUM_COMMENTS_PER_PAGE = 4;
    
    private void handleCheckout() throws IOException {
    	if (order.items.isEmpty()) {
    		DBUtil.println("your cart is empty.");
    		return ;
    	}
    	
    	DBUtil.println(order.toString());
    	char response;
    	while (true) {
    		response = DBUtil.getChar("sure to checkout? y/n: ");
    		if (response == 'y') {
    			break;
    		}
    		else if (response == 'n') {
    			return ;
    		}
    	}
    	
    	int result = sql.placeOrder(order);
    	if (result != SQL.SUCCESS) {
    		if (result == SQL.EEMPTYORDER && !order.cancelled.isEmpty()) {
    			DBUtil.printf(order.toString());
    			order = new Order(user.login);
    			return ;
    		}
    		
    		DBUtil.printf("checkout failed: %s\n", SQL.strerror(result));
    		return ;
    	}
    	
    	DBUtil.printf("checkout succeeded.\n");
    	DBUtil.println(order.toString());
    	order = new Order(user.login);
    }
    
    private void handleAuthorDistance() throws IOException {
    	String author1 = DBUtil.getStr("author name 1: ");
    	long id1 = queryAuthorId(author1, false);
    	if (id1 < 0) return ;
    	
    	String author2 = DBUtil.getStr("author name 2: ");
    	long id2 = queryAuthorId(author2, false);
    	if (id2 < 0) return ;
    	
    	Pointer<Integer> d = new Pointer<Integer>();
    	int result = sql.queryAuthorDistance(id1, id2, d);
    	if (result != SQL.SUCCESS) {
    		DBUtil.printf("query author distance failed: %s\n", SQL.strerror(result));
    		return ;
    	}
    	
    	if (d.value == null) {
    		DBUtil.printf("distance(%s, %s) = INF\n", author1, author2);
    	}
    	else {
    		DBUtil.printf("distance(%s, %s) = %d\n", author1, author2, d.value);
    	}
    }

    public void displayManageMenu()
    {
        DBUtil.println("        Book Store Management");
        DBUtil.println("1. New book");
        DBUtil.println("2. Arrival of more copies");
        DBUtil.println("3. Show most popular books");
        DBUtil.println("4. Show most popular authors");
        DBUtil.println("5. Show most popular publishers");
        DBUtil.println("6. Show most trusted users");
        DBUtil.println("7. Show most useful users");
        DBUtil.println("8. Create new administrator account");
        DBUtil.println("9. Back to main menu");
        DBUtil.println("");
    }         
    
	public void management() throws IOException {
    	while (true) {
        	displayManageMenu();
        	
        	switch (DBUtil.getInt(1, 9, "manage> ")) {
        	case 1:	/* new book */
        		handleNewBook();
        		break;
        		
        	case 2:	/* more copies */
        		handleArrivalMoreCopies();
        		break;
        		
        	case 3:	/* most popular books */
        		handleMostPopularBooks();
        		break;
        	
        	case 4: /* most popular authors */
        		handleMostPopularAuthors();
        		break;
        		
        	case 5: /* most popular publishers */
        		handleMostPopularPublishers();
        		break;
        		
        	case 6: /* most trusted users */
        		handleMostTrustedUsers();
        		break;
        		
        	case 7:	/* most useful users */	
        		handleMostUsefulUsers();
        		break;
        		
        	case 8:	/* new admin */
        		handleRegister(true);
        		break;
        		
        	case 9: 	/* back to main menu */
        		return;
        	}    		
    	}
    }
	
	private void handleNewBook() throws IOException {
		String ISBN = DBUtil.getISBN("ISBN: ");
		String title = DBUtil.getStr("Title: ");
		String publisher = DBUtil.getStrN("Publisher: ");
		int year = DBUtil.getInt(1600, null, "Year of publication(1600-): ");
		int num = DBUtil.getInt(0, null, "Number of copies: ");
		int price = DBUtil.getPrice("Price: ");
		int format = DBUtil.getInt(0, 1, "format(0=softcover, 1=hardcover): ");
		String keywords = DBUtil.getStrN("Keywords: ");
		String subject = DBUtil.getStrN("Subject: ");
		
		Book book = new Book();
		book.isbn = ISBN;
		book.title = title;
		book.publisher = publisher;
		book.year = year;
		book.n_copies = num;
		book.price = price;
		book.format = (byte) format;
		book.keywords = keywords;
		book.subject = subject;
	
		String author;
		while (true) {
			author = DBUtil.getStrN("Authors (enter empty line to end): ");
			if (author.isEmpty()) break;
			
			long id = queryAuthorId(author, true);
			if (id == -2) {
				/* error */
				return ;
			}
			
			book.addAuthor(author, id);
		}
		
		int result = sql.addBook(book);
		if (result == SQL.SUCCESS) {
			DBUtil.printf("Done.\n");
		}
		else {
			DBUtil.printf("insert failed: %s\n", SQL.strerror(result));
		}
	}
	
	public long queryAuthorId(String author, boolean create) throws IOException {
		ArrayList<Long> ids = new ArrayList<Long>();
		int result = sql.queryAuthorID(author, ids);
		if (result != SQL.SUCCESS) {
			DBUtil.printf("author query failed: %s\n", SQL.strerror(result));
			return -2;
		}
		
		if (ids.isEmpty()) {
			if (create) {
				return -1;
			}
			else {
				DBUtil.printf("author not found.\n");
				return -2;
			}
		}
	
		if (!create && ids.size() == 1) {
			return ids.get(0);
		}
		
		DBUtil.printf("%d authors found:\n", ids.size());
		
		ArrayList<Book> books = new ArrayList<Book>();
		for (long id : ids) {
			books.clear();
			result = sql.queryBookByAuthorID(id, books, 3);
			if (result != SQL.SUCCESS) {
				DBUtil.printf("query book by author id failed: %s\n", SQL.strerror(result));
				return -2;
			}

			if (books.isEmpty()) {
				DBUtil.printf("author id = %d, no books found\n", id);
			}
			else {
				DBUtil.printf("author id = %d, the first %d books:\n", id, books.size());
				DBUtil.println(Book.header);
				for (Book abook: books) {
					DBUtil.println(abook.toString());
				}
			}
			DBUtil.println("");
		}

		long id;
		while (true) {
			if (create) {
				id = DBUtil.getLong(null, null, "which one (-1 for a new author): ");
				if (id == -1) break;
			}
			else {
				id = DBUtil.getLong((long) 0, null, "which one: ");
			}

			if (ids.contains(id)) {
				break;
			}
			else {
				DBUtil.printf("invalid id. ");
				continue;
			}
		}
		
		return id;
	}

	public void handleArrivalMoreCopies() throws IOException {
		ArrayList<Book> books = queryBooks();
		if (books == null) return ;
    	
    	int now = 0;
    	while (true) {
    		int tar = Math.min(books.size(), now + NUM_BOOKS_PER_PAGE);
    		int pn = now / NUM_BOOKS_PER_PAGE;
    	
    		DBUtil.printf("page %d\n", pn);
    		DBUtil.println("no.  " + Book.header);
    		for (int i = now ; i < tar; ++i) {
    			DBUtil.printf("%-5d%s\n", i, books.get(i).toString());
    		}
    		
    		DBUtil.println("enter: ");
    		DBUtil.println("a <no.> <num> to add some copies");
    		if (now != 0) {
    			DBUtil.println("p: show the previous page");
    		}
    		if (tar != books.size()) {
    			DBUtil.println("n: show the next page");
    		}
    		DBUtil.println("q: return");
    		
    		String command;
    		COMMAND:
    		while (true) {
    			command = DBUtil.getStrN();
    			if (command.length() == 1) {
    				char c = command.charAt(0);
    				switch (c) {
    				case 'p':
    					if (now != 0) {
    						now -= NUM_BOOKS_PER_PAGE;
    					}
   						break COMMAND;
    				case 'n':
    					if (tar != books.size()) {
    						now += NUM_BOOKS_PER_PAGE;
    					}
   						break COMMAND;
    				case 'q':
    					return ;
    				}
    			}
    			
    			if (command.isEmpty() || command.charAt(0) != 'a') {
    				DBUtil.println("unknown command");
    				continue;
    			}
    			String[] tokens = command.split(" ");
    			if (tokens.length < 3) {
    				DBUtil.println("a <no.> <num>, something is missing");
    				continue;
    			}
    			
    			int no;
    			int num;
    			try {
    				no = Integer.parseInt(tokens[1]);
    			} catch (NumberFormatException e) {
    				DBUtil.println("no. is not a number");
    				continue;
    			}
    			try {
    				num = Integer.parseInt(tokens[2]);
    			}
    			catch (NumberFormatException e) {
    				DBUtil.println("num is not a number");
    				continue;
    			}
    			
    			if (no < 0 || no >= books.size()) {
    				DBUtil.printf("no is out of range [0..%d)\n", books.size());
    				continue;
    			}
    			
    			Book book = books.get(no);
    			if (num + book.n_copies < 0) {
    				DBUtil.printf("num is out of range (0..maxint)\n");
    			}
    			
    			int result = sql.updateNumCopies(book.isbn, num + book.n_copies);
    			if (result != SQL.SUCCESS) {
    				DBUtil.printf("update num copies failed: %s\n", SQL.strerror(result));
    			}
    			else {
    				book.n_copies += num;
    			}
    			
    			break;
    		}
    			
    	}
	} 
	
	public void handleMostPopularBooks() throws IOException {
		int n = DBUtil.getInt(0, null, "top n popular books(0 for unlimited): ");
		
		ArrayList<Book> books = new ArrayList<Book>();
		int result = sql.queryMostPopularBooks(books, n);
		if (result != SQL.SUCCESS) {
			DBUtil.printf("query failed: %s\n", SQL.strerror(result));
			return ;
		}
		
		if (books.isEmpty()) {
			DBUtil.printf("no such books\n");
			return ;
		}
		
		DBUtil.printf("%d most popular books:\n", books.size());
		DBUtil.printf("%-11s%13s\t%10s\t%s\n", "no.", "isbn", "sales", "title");
		for (int i = 0; i < books.size(); ++i) {
			Book book = books.get(i);
			
			DBUtil.printf("%-11d%11s\t%10d\t%s\n", 
					i + 1,
					book.isbn,
					book.additionalFields.get("sales"),
					book.title);
		}
		DBUtil.println("");
		
	}
	
	public void handleMostPopularAuthors() throws IOException {
		int n = DBUtil.getInt(0, null, "top n popular authors(0 for unlimited): ");
		
		ArrayList<Object[]> authors = new ArrayList<Object[]>();
		int result = sql.queryMostPopularAuthors(authors, n);
		if (result != SQL.SUCCESS) {
			DBUtil.printf("query failed: %s\n", SQL.strerror(result));
			return ;
		}
		
		if (authors.isEmpty()) {
			DBUtil.printf("no such authors\n");
			return ;
		}
		
		DBUtil.printf("%d most popular authors\n", authors.size());
		DBUtil.printf("%-11s%10s\t%10s\t%s\n", "no.", "author id", "sales", "author name");
		for (int i = 0; i < authors.size(); ++i) {
			Object[] author = authors.get(i);
			
			DBUtil.printf("%-11d%10d\t%10d\t%s\n", 
					i + 1,
					author[0],
					author[2],
					author[1]);
		}
		DBUtil.println("");
	}
    
	public void handleMostPopularPublishers() throws IOException {
		int n = DBUtil.getInt(0, null, "top n popular publishers(0 for unlimited): ");
		
		ArrayList<Pair<String, Integer>> pubs = new ArrayList<Pair<String, Integer>>();
		int result = sql.queryMostPopularPublishers(pubs, n);
		if (result != SQL.SUCCESS) {
			DBUtil.printf("query failed: %s\n", SQL.strerror(result));
			return ;
		}
		
		if (pubs.isEmpty()) {
			DBUtil.printf("no such publishers\n");
			return ;
		}
		
		DBUtil.printf("%d most popular publishers\n", pubs.size());
		DBUtil.printf("%-11s%10s\t%s\n", "no.", "sales", "publisher");
		for (int i = 0; i < pubs.size(); ++i) {
			Pair<String, Integer> pub = pubs.get(i);
			
			DBUtil.printf("%-11d%10d\t%s\n", 
					i + 1,
					pub.second,
					pub.first);
		}
		DBUtil.println("");
	}
	
	public void handleMostTrustedUsers() throws IOException {
		int n = DBUtil.getInt(0, null, "top n trusted users(0 for unlimited): ");
		
		ArrayList<User> users = new ArrayList<User>();
		int result = sql.queryMostTrustedUsers(users, n);
		if (result != SQL.SUCCESS) {
			DBUtil.printf("query failed: %s\n", SQL.strerror(result));
			return ;
		}
		
		if (users.isEmpty()) {
			DBUtil.printf("no such users\n");
			return ;
		}
		
		DBUtil.printf("%d most trusted users\n", users.size());
		DBUtil.printf("no.\tscore\tlogin\tname\tphone\taddress\tuser type\n");
		for (int i = 0; i < users.size(); ++i) {
			User user = users.get(i);
			
			DBUtil.printf("%d\t%d\t%s\n", i + 1, user.a_field, user.toString());
		}
	}
	
	public void handleMostUsefulUsers() throws IOException {
		int n = DBUtil.getInt(0, null, "top n useful users(0 for unlimited): ");
		
		ArrayList<User> users = new ArrayList<User>();
		int result = sql.queryMostUsefulUsers(users, n);
		if (result != SQL.SUCCESS) {
			DBUtil.printf("query failed: %s\n", SQL.strerror(result));
			return ;
		}
		
		if (users.isEmpty()) {
			DBUtil.printf("no such users\n");
			return ;
		}
		
		DBUtil.printf("%d most useful users\n", users.size());
		DBUtil.printf("no.\tscore\tlogin\tname\tphone\taddress\tuser type\n");
		for (int i = 0; i < users.size(); ++i) {
			User user = users.get(i);
			
			DBUtil.printf("%d\t%.1f\t%s\n", i + 1, user.a_field, user.toString());
		}
	}
	
}
