package acmdb;

public class User {
	
	public String login = null;;
	public String phone;
	public String name;
	public String address;
	public byte usertype = SQL.UTYPE_USER;
	
	public Object a_field;
	
	public void clear() {
		login = null;
		phone = null;
		name = null;
		address = null;
	}

	@Override
	public String toString() {
		if (login == null) return "null";
		return String.format("%-16s %-16s %-16s %-16s %s", 
				login, 
				(name == null) ? "N/A": name, 
				(phone == null) ? "N/A" : phone, 
				(address == null) ? "N/A" : address,
				(usertype == SQL.UTYPE_ADMIN) ? "admin" : "user");
	}
}
