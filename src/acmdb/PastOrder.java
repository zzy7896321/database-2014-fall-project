package acmdb;

import java.sql.Timestamp;
import java.util.ArrayList;

public class PastOrder {
	public PastOrder(long oid, Timestamp time, String login) {
		this.oid = oid;
		this.time = time;
		this.login = login;

		this.books = new ArrayList<Book>();
		this.nums = new ArrayList<Integer>();
	}

	public int getNum() {
		return books.size();
	}

	public int getTotalPrice() {
		int total = 0;
		for (int i = 0; i < books.size(); ++i) {
			total += books.get(i).price * nums.get(i);	
		}
		return total;
	}

	public long oid;
	public String login;
	public ArrayList<Book> books;
	public ArrayList<Integer> nums;
	public Timestamp time;
}

