package acmdb;

import java.io.IOException;

public class DBUtil {
	
	private static UserIO io = (System.console() == null) ? new StdIO() : new ConsoleIO();
	
	public static void println(String str) {
		io.printf("%s\n", str);
	}
	
	public static void printf(String fmt, Object... args) {
		io.printf(fmt, args);
	}
	
	public static String getMD5(byte[] source) {
		String s = null;
		char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',  'e', 'f'}; 
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance( "MD5" );
			md.update( source );
			byte tmp[] = md.digest();
			char str[] = new char[16 * 2];
			int k = 0;
			for (int i = 0; i < 16; i++) {
				byte byte0 = tmp[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			s = new String(str);
		} catch( Exception e ) {
			e.printStackTrace();
		}
		return s;
	}

	public static String getMD5(String source) {
		return getMD5(source.getBytes());
	}

	public static int getPrice() throws IOException {
		return getPrice("");
	}
	
	public static int getPrice(String fmt, Object... args) throws IOException {
		String line;
		boolean second = false;
		while (true) {
			if (second) {
				io.printf("error: invalid value\n");
			}
			else {
				second = true;
			}
			
			line = io.readLine(fmt, args);
			if (line == null) throw new IOException();
			
			int price = checkPrice(line, -1);
			if (price >= 0) {
				return price;
			}
		}
	}

	public static int checkPrice(String line, int defaultValue) {
		int dot = line.indexOf('.');
		if (dot == -1) dot = line.length();
		if (dot > 8) {
			return defaultValue;
		}
		
		int main_part = -1;
		try {
			main_part = Integer.parseInt(line.substring(0, dot));
		}
		catch (NumberFormatException e) {
		}
		
		if (main_part < 0) return defaultValue;
	
		int secondary_part = -1;
		if (dot + 1 < line.length()) {
			if (dot + 3 < line.length()) {
				return defaultValue;
			}
			try{
				secondary_part = Integer.parseInt(line.substring(dot + 1, line.length()));
			}
			catch (NumberFormatException e) {
				return defaultValue;
			}
			if (secondary_part < 0) return defaultValue;
		}
		
		return main_part * 100 + secondary_part;
	}

	public static long getLong(Long lower, Long upper) throws IOException {
		return getLong(lower, upper, "");
	}

	public static long getLong(Long lower, Long upper, String fmt, Object... args) throws IOException {
		String line;
		while (true) {
			line = io.readLine(fmt, args);
			if (line == null) throw new IOException();
			
			long value;
			try {
				value = Long.parseLong(line);
			}
			catch (NumberFormatException e) {
				continue;
			}

			if ( (lower == null || value >= lower) && (upper == null || value <= upper) ) {
				return value;
			}
			io.printf("error: invalid value\n");
		}
	}
	
	public static int getInt(Integer lower, Integer upper) throws IOException {
		return getInt(lower, upper, "");
	}

	public static int getInt(Integer lower, Integer upper, String fmt, Object... args) throws IOException {
		String line;
		while (true) {
			line = io.readLine(fmt, args);
			if (line == null) throw new IOException();
			
			int value;
			try {
				value = Integer.parseInt(line);
			}
			catch (NumberFormatException e) {
				continue;
			}

			if ( (lower == null || value >= lower) && (upper == null || value <= upper) ) {
				return value;
			}
			
			io.printf("error: invalid value\n");
		}
	}
	
	public static char getChar() throws IOException {
		return getChar();
	}

	public static char getChar(String fmt, Object... args) throws IOException {
		String line;
		while (true) {
			line = io.readLine(fmt, args);
			if (line == null) throw new IOException();
			
			if (!line.isEmpty()) {
				return line.charAt(0);
			}
			io.printf("error: invalid value\n");
		}
	}
	
	public static double getDouble(Double lower, Double upper) throws IOException {
		return getDouble(lower, upper, "");
	}
	
	public static double getDouble(Double lower, Double upper, String fmt, Object... args) throws IOException {
		String line;
		while (true) {
			line = io.readLine(fmt, args);
			if (line == null) throw new IOException();
			
			double value;
			try {
				value = Double.parseDouble(line);
			}
			catch (NumberFormatException e) {
				continue;
			}

			if ( (lower == null || value >= lower) && (upper == null || value <= upper) ) {
				return value;
			}
			io.printf("error: invalid value\n");
		}
	}	 
	
	public static String getMultilineString(String header, String end_marker, int limit) throws IOException {
		return getMultilineString(header, end_marker, limit, "");
	}
	
	public static String getMultilineString(String header, String end_marker, int limit, String fmt, Object... args) throws IOException {
		io.printf(fmt, args);
		
		StringBuilder builder = new StringBuilder();
		String line;
		int n = 0;
		while (true) {
			line = getStrN(header);
			if (line.equals(end_marker)) {
				return builder.toString();
			}
			
			if (n > 0) builder.append("\n");
			builder.append(line);
			n += line.length() + 1;
			
			if (limit != 0 && n >= limit) {
				builder.delete(limit, builder.length());
				return builder.toString();
			}
		}
	}

	public static String getStr() throws IOException {
		return getStr("");
	}
	
	public static String getStr(String fmt, Object... args) throws IOException {
		String line;
		while (true) {
			line = io.readLine(fmt, args);
			if (line == null) throw new IOException();
			
			if (!line.isEmpty()) return line;
			io.printf("error: invalid value\n");
		}
	}

	public static String getStrN() throws IOException {
		return getStrN("");
	}
	
	public static String getStrN(String fmt, Object... args) throws IOException {
		String line;
		line = io.readLine(fmt, args);
		if (line == null) throw new IOException();
		
		return line;
	}

	public static String getPassword() throws IOException {
		return getPassword("");
	}
	
	public static String getPassword(String fmt, Object... args) throws IOException {
		String passwd;
		while (true) {
			passwd = io.readPassword(fmt, args);
			if (passwd == null) throw new IOException();

			if (!passwd.isEmpty()) return passwd;
			io.printf("error: invalid value\n");
		}
	}

	public static String getISBN() throws IOException {
		return getISBN("");
	}
	
	public static String getISBN(String fmt, Object... args) throws IOException {
		String isbn = null;
		
		while (isbn == null) {
			String line = getStr(fmt, args);
			isbn = check_ISBN(line);
			if (isbn == null) {
				io.printf("error: invalid ISBN\n");
			}
		}
		
		return isbn;
	}
	
	public static String check_ISBN(String line) {
		int[] isbn = new int[13];
		int cur_num = 0;
		int p_line = 0;
		boolean valid = true;

		while (p_line < line.length()) {
			char c = line.charAt(p_line++);

			if (c >= '0' && c <= '9') {
				if (cur_num == 13) {
					valid = false;
					break;
				}
				isbn[cur_num++] = c - '0';
			}
			else if (c == 'x' || c == 'X'){
				if (cur_num != 9) {
					valid = false;
					break;
				}
				isbn[cur_num++] = 10;
			}
			else if (c != '-'){
				valid = false;
				break;
			}
		}

		if (!valid) return null;

		if (cur_num == 10) {
			/* convert it to the 13-digit version */
			int checksum = get_10digit_ISBN_checksum(isbn);
			if (checksum != isbn[9]) return null;

			System.arraycopy(isbn, 0, isbn, 3, 9);

			isbn[0] = 9;
			isbn[1] = 7;
			isbn[2] = 8;
			isbn[12] = get_13digit_ISBN_checksum(isbn);
		}

		else if (cur_num == 13) {
			if (isbn[9] == 10) return null;

			int checksum = get_13digit_ISBN_checksum(isbn);
			if (checksum != isbn[12]) return null;
		}

		else {
			/* invalid isbn */
			return null;
		}

		char[] buffer = new char[13];
		for (int i = 0; i < 13; ++i) {
			buffer[i] = (char) (isbn[i] + '0');
		}
		return new String(buffer);
	}
	
	private static int get_10digit_ISBN_checksum(int[] isbn) {
		int checksum = 0;
		for (int i = 0; i < 9; ++i) {
			checksum += (1 + i) * isbn[i];
		}
		checksum %= 11;
		return checksum;
	}
	
	private static int get_13digit_ISBN_checksum(int[] isbn) {
		int checksum = 0;
		for (int i = 0; i < 6; ++i) {
			checksum += 9 * isbn[(i << 1)] + 7 * isbn[(i << 1) + 1];
		}
		checksum %= 10;
		return checksum;
	}
}
