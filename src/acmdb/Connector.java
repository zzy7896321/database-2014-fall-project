package acmdb;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class Connector {
	private Connection con = null;

	public Connector(String host, String dbuser, String dbpasswd) {
		properties = new Properties();
		properties.put("host", host);
		properties.put("username", dbuser);
		properties.put("password", dbpasswd);
	}
	
	public Connector(String propertyFileName) {
		properties = new Properties();
		try {
			if (propertyFileName != null) {
				FileInputStream in = null;
				try {
					in = new FileInputStream(propertyFileName);
					properties.loadFromXML(in);
					
					if (!properties.containsKey("host") || !properties.containsKey("username") ||
						!properties.containsKey("password")) {
						properties.clear();
					}
				}
				catch (FileNotFoundException e) {
				}
				catch (InvalidPropertiesFormatException e) {
				}
				finally {
					if (in != null) in.close();
				}
			}
			
			if (properties.isEmpty()) {
				String host = DBUtil.getStr("host: ");
				String username = DBUtil.getStr("username: ");
				String password = DBUtil.getPassword("password: ");
				
				properties.put("host", host);
				properties.put("username", username);
				properties.put("password", password);
				
				if (propertyFileName != null) {
					try (FileOutputStream out = new FileOutputStream(propertyFileName)) {
						properties.storeToXML(out, null);
					}
				}
			}
		}
		catch (IOException e) {
			System.err.println("io error");
			System.exit(1);
		}
	}
	
	private Properties properties;
	
	public void connect() {
		String host = properties.getProperty("host");
		String username = properties.getProperty("username");
		String password = properties.getProperty("password");
		String url = "jdbc:mysql://" + host + "/acmdb02";
		
		try {
			Class.forName ("com.mysql.jdbc.Driver").newInstance();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	        
		try {
			con = DriverManager.getConnection(url, username, password);
			con.setAutoCommit(false);
		} catch (SQLException e) {
			showException(e);			
			if (con != null)  {
				try {
					con.close();
				} catch (SQLException e1) {
					showException(e);
					System.exit(1);
				}
				con = null;
			}
		}
	}
	
	public Connection getConnection() {
		return con;
	}
	
	public void closeConnection() {
		if (con != null)
			try {
				con.close();
				con = null;
			} catch (SQLException e) {
				showException(e);
			}
	}
	
	public static void showException(SQLException sql_exceptions) {
		for (Throwable e: sql_exceptions) {
			System.err.println(e.getMessage());
			if (e instanceof SQLException) {
				SQLException sql_exception = (SQLException) e;
				System.err.println("SQLState = " + sql_exception.getSQLState());
				System.err.println("Error code = " + sql_exception.getErrorCode());
				
				Throwable cause = sql_exception.getCause();
				while (cause != null) {
					System.err.println("cause: " + cause);
					cause = cause.getCause();
				}
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		String filename;
		if (args.length > 0) {
			filename = args[0];
		}
		else {
			filename = "default.xml";
		}
		
		Connector connector = new Connector(filename);
		connector.connect();
		
		Connection con = connector.getConnection();
		
		try {
			DatabaseMetaData dbMetaData = con.getMetaData();
		    System.out.println("ResultSet.HOLD_CURSORS_OVER_COMMIT = " +
		        ResultSet.HOLD_CURSORS_OVER_COMMIT);

		    System.out.println("ResultSet.CLOSE_CURSORS_AT_COMMIT = " +
		        ResultSet.CLOSE_CURSORS_AT_COMMIT);

		    System.out.println("Default cursor holdability: " +
		        dbMetaData.getResultSetHoldability());

		    System.out.println("Supports HOLD_CURSORS_OVER_COMMIT? " +
		        dbMetaData.supportsResultSetHoldability(
		            ResultSet.HOLD_CURSORS_OVER_COMMIT));

		    System.out.println("Supports CLOSE_CURSORS_AT_COMMIT? " +
		        dbMetaData.supportsResultSetHoldability(
		            ResultSet.CLOSE_CURSORS_AT_COMMIT));
			
			
		} catch (SQLException e) {
			showException(e);
		}
	}
}
