package acmdb;

public interface UserIO {
	
	public String readLine(String fmt, Object... args);
	
	public String readPassword(String fmt, Object... args);
	
	public void printf(String fmt, Object... args);
}
