package acmdb;

public class BookQuery implements java.io.Serializable {
	public static final long serialVersionUID = 1L;

	public BookQuery(String title, String isbn, String author, 
			String publisher, String subject, String keywords,
			String orderby) {
		this.title = title;
		this.isbn = isbn;
		this.author = author;
		this.publisher = publisher;
		this.subject = subject;
		this.keywords = keywords;

		int _orderby = WebUtil.parseInt(orderby, 0);
		if (_orderby < 0 || _orderby >= SQL.sortingOrders.length) 
			_orderby = 0;
		this.orderby = _orderby;
	}
	
	public String title;
	public String isbn;
	public String author;
	public String publisher;
	public String subject;
	public String keywords;
	public int orderby;

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof BookQuery)) {
			return false;
		}

		BookQuery q2 = (BookQuery) other;
		return 
			this.orderby == q2.orderby &&
			strequal(this.title, q2.title) &&
			strequal(this.isbn, q2.isbn) &&
			strequal(this.author, q2.author) &&
			strequal(this.publisher, q2.publisher) &&
			strequal(this.subject, q2.subject) &&
			strequal(this.keywords, q2.keywords);
	}

	@Override
	public int hashCode() {
		final int m = 31;
		int hcode = 0;
		hcode = hcode * m + ((title == null) ? 0 : title.hashCode());
		hcode = hcode * m + ((isbn == null) ? 0 : isbn.hashCode());
		hcode = hcode * m + ((author == null) ? 0 : author.hashCode());
		hcode = hcode * m + ((publisher == null) ? 0 : publisher.hashCode());
		hcode = hcode * m + ((subject == null) ? 0 : subject.hashCode());
		hcode = hcode * m + ((keywords == null) ? 0 : keywords.hashCode());
		hcode = hcode * m + (orderby);
		return hcode;
	}

	private static boolean strequal(String s1, String s2) {
		if (s1 == null) {
			return s2 == null;
		}
		return s1.equals(s2);
	}

	@Override
	public String toString() {
		return new StringBuilder()
			.append("title: ").append(title).append("\n")
			.append("isbn: ").append(isbn).append("\n")
			.append("author: ").append(author).append("\n")
			.append("publisher: ").append(publisher).append("\n")
			.append("subject: ").append(subject).append("\n")
			.append("keywords: ").append(keywords).append("\n")
			.append(SQL.sortingOrders[orderby]).append("(").append(orderby).append(")\n")
			.toString();
	}
}
