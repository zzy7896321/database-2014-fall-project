package acmdb;

public class Pair<T, V> {
	Pair(T first, V second) {
		this.first = first;
		this.second = second;
	}
	
	Pair() {
		this(null, null);
	}
	
	public T first;
	public V second;
}
