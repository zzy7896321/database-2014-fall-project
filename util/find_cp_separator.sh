#!/bin/sh

back=`pwd`
if [ `basename ${back}` != 'util' ]; then
	cd util
fi

${JAVAC} PathSeparator.java

${JAVA} PathSeparator

cd ${back}

