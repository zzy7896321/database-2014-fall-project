import java.io.*;

public class PathSeparator {
	public static void main(String[] args) throws IOException {
		String sep = System.getProperty("path.separator");
		System.out.printf("%s", sep);
	}
}
